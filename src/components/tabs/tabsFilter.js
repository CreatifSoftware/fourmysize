import React from "react";
import {View, StyleSheet, Text, TouchableOpacity, Image} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";
import {HorizontalSeperator} from "../seperators/horizontalSeperator";


export const GroupTabsFilter = ({values, callBackIndex}) => {

    const [selectedItem, setSelectedItem] = React.useState(0);


    const _onPress = (idx) => {
        setSelectedItem(idx);
        callBackIndex(idx)
    };

    const renderGroupTabs = () => {
        return (values || []).map((item, idx) => {
            let isChecked = selectedItem === idx ? true : false;
            var RandomNumber = Math.floor(Math.random() * 1000) + 1;

            return (
                <TouchableOpacity key={"btn" + RandomNumber + "_" +  idx} onPress={() => _onPress(idx)}
                                  style={[styles.container]}>
                    <Text
                        key={"text" + RandomNumber + "_" +  idx}
                        style={[styles.text, {
                            color: isChecked ? Colors.Primary : Colors.title_active,

                        }]}
                    >{item.text}</Text>

                    {isChecked ? <View
                        key={"view0_" + RandomNumber + "_" +  idx}
                        style={{
                            backgroundColor: Colors.Primary,
                            height: 2,
                            width: "100%"
                        }}/> : <View
                        key={"view1_" + RandomNumber + "_" +  idx}
                        style={{
                            height: 2,
                            width: "100%"
                        }}/>}


                </TouchableOpacity>
            );
        });
    };

    return <View style={{flexDirection: "row"}}>{renderGroupTabs()}</View>;
}


const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius:  hp(5),

        flexDirection: "column",
        marginLeft: wp(20),
        paddingLeft: wp(10),
        paddingRight: wp(10),
    },
    text: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.white,
        textAlign: "center",
        marginBottom: hp(5),
    },


})
