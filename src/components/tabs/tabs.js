import React from "react";
import {View, StyleSheet, Text, TouchableOpacity, Image} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";


export const GroupTabs = ({values, callBackIndex, color, passiveColor, fontSize = 12, selectTab = -1}) => {

    const [selectedItem, setSelectedItem] = React.useState(selectTab);

    const _onPress = (idx) => {
        setSelectedItem(idx);
        callBackIndex(idx)
    };

    const renderGroupTabs = () => {
        return (values || []).map((item, idx) => {
            let isChecked = selectedItem === idx;
            return (<TouchableOpacity key={"touch" + "_" + idx} onPress={() => _onPress(idx)}
                                      style={[styles.container, {backgroundColor: isChecked ? color : passiveColor}]}>

                    {item.iconName && <Image style={styles.icon} source={item.iconName}/>}

                    <Text
                        key={"text" + "_" + idx}
                        style={[styles.text, {
                            fontSize: wp(fontSize),
                            color: isChecked ? Colors.white : Colors.title_active
                        }]}
                    >{item.text}</Text>
                </TouchableOpacity>
            );
        });
    };

    return <View style={{flexDirection: "row"}}>{renderGroupTabs()}</View>;
}


const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        borderRadius:  hp(5),
        backgroundColor: Colors.Primary,
        flexDirection: "row",
        marginRight: wp(20),
        paddingLeft: wp(10),
        paddingRight: wp(10),
        height: hp(40),
    },
    text: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.white,
        textAlign: "center",
    },
    icon: {
        height: hp(24),
        width: hp(24),
        marginRight: wp(8)
    }
})
