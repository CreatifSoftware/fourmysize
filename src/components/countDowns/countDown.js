import React from "react";
import {StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";

const CountDown = (props) => {
    const [index, setindex] = React.useState(props.count)

    const setIndex = (index) => {
        if (index >= 1 && index <= 99) {
            setindex(index)
            props.onPress(index);
        }
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => { setIndex(index - 1)}}>
                <Text style={styles.text}>-</Text>
            </TouchableOpacity>

            <Text style={styles.text}>{index}</Text>

            <TouchableOpacity onPress={() => setIndex(index + 1)}>
                < Text style={[styles.text, {marginRight: 0}]}>+</Text>
            </TouchableOpacity>
        </View>
    )
}

export default CountDown

const styles = StyleSheet.create({
    container: {
        height: hp(32),
        width: wp(78),
        backgroundColor: Colors.line_tab,
        borderRadius:  hp(10),
        marginTop: hp(11),
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    text: {
        fontSize: wp(16),
        fontFamily: "Gilroy-bold",
        fontWeight: "800",
        color: Colors.content_text,
        marginRight: wp(15),

    }
})
