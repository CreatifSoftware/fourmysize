import React from "react"
import {hp, wp} from "../../styles/Dimen";
import {SliderHuePicker} from "react-native-slider-color-picker";
import {Dimensions, StyleSheet, TouchableOpacity, View} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import {Colors} from "../../styles/Colors";
import tinycolor from "tinycolor2";

const ColorPicker = (props) => {

    const [oldColor, setoldColor] = React.useState("#FF7700")
    const {width} = Dimensions.get('window');

    const changeColor = (colorHsvOrRgb, resType) => {
        setoldColor(tinycolor(colorHsvOrRgb).toHexString())
        console.log(oldColor)
    }

    return (<>
            {props.active &&
            <View style={[styles.container, {width: wp(width - wp(140))}]}>
                <SliderHuePicker
                    oldColor={oldColor}
                    trackStyle={[{height: hp(12), width: wp(width - wp(140))}]}
                    thumbStyle={styles.thumb}
                    useNativeDriver={true}
                    onColorChange={changeColor}/>

                <TouchableOpacity onPress={() => {
                    props.onPress(oldColor, false)
                }} style={[styles.item, {backgroundColor: oldColor}]}>
                    <AntDesign name={"check"} size={hp(15)} color={Colors.white}/>
                </TouchableOpacity>
            </View>
            }
        </>
    )
}

export default ColorPicker

const styles = StyleSheet.create({
    container: {
        marginLeft: wp(10),
        marginBottom: hp(20),
    },
    thumb: {
        width: hp(20),
        height: hp(20),
        borderColor: 'white',
        borderWidth: 1,
        borderRadius:  hp(10),
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: hp(2)
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    item: {
        borderRadius:  hp(20),
        height: hp(20),
        width: hp(20),
        marginRight: wp(20),
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        right: wp(-50),
        top: hp(-10),
    },
})



