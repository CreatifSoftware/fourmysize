import React from "react";
import {StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import RNPickerSelect from "react-native-picker-select";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

const CardPicker = (props) => {

    const placeholder = {
        label: props.placeHolder,
        value: null,
        color: '#9EA0A4',
    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>{props.title} : </Text>
            <RNPickerSelect
                style={pickerSelectStyles}
                onValueChange={props.onValueChange}
                placeholder={placeholder}
                items={props.data}
                value={props.value}
                useNativeAndroidPickerStyle={false}

            />
            <SimpleLineIcons style={{position: "absolute", right: wp(10)}} color={Colors.login_title}
                             name={"arrow-down"}/>
        </View>
    )
}

export default CardPicker

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(340),
        height: hp(46),
        borderRadius:  hp(10),
        flexDirection: "row",
        alignItems: "center",
        marginBottom: hp(17),
        marginLeft: wp(10),
        marginRight: wp(10),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    title: {
        marginLeft: wp(15),
        marginRight: wp(5),

        fontSize: wp(12),
        color: Colors.content_text,
    }
})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        width: wp(280),
        marginTop: hp(12),
        fontSize: wp(14),
        color: Colors.content_text,
        fontFamily: "Gilroy-Regular",
        lineHeight: hp(16),
    },
    inputAndroid: {
        width: wp(260),
        marginTop: hp(5),
        fontSize: wp(14),
        color: Colors.content_text,
        lineHeight: hp(16),
    },
});
