import React from "react";
import {StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import PropTypes from "prop-types";

const TextButton = (props) => {
    return <TouchableOpacity onPress={props.onPress}>
        <View style={styles.container}>
            <Text style={[styles.text, {color: props.color}]}>{props.text}</Text>
            <MaterialIcons name={"navigate-next"} size={hp(20)} color={props.color}/>
        </View>
    </TouchableOpacity>
}

export default TextButton

TextButton.propTypes = {
    color: PropTypes.string,
    text: PropTypes.string,
    onPress: PropTypes.func,
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        marginTop: hp(10),
        alignItems: "center"
    },
    text: {
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "600",
        color: Colors.Secondary,
    }
})



