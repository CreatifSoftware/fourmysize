import React from "react";
import {View, StyleSheet, Text, TouchableOpacity} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";

export const ButtonSelect = (props) => {

    return <TouchableOpacity onPress={

        props.onPress} style={[styles.viewtitle,
        props.style, {backgroundColor: props.buttonColor}]}>

            <Text style={[styles.viewtitletext, {
                fontSize: wp(props.fontSize),
                color: props.textColor
            }]}>{props.text}</Text>

    </TouchableOpacity>
}

ButtonSelect.defaultProps = {
    text: "text",
    fontSize: 12,
    color: Colors.white,
    buttonColor: Colors.Primary
};

const styles = StyleSheet.create({
    viewtitle: {
        height: hp(32),
        width: wp(93),
        justifyContent: "center",
        alignItems: "center",
        borderRadius: hp(5),
        backgroundColor: Colors.Primary,

    },
    viewtitletext: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.white,
        textAlign: "center",
    },
})
