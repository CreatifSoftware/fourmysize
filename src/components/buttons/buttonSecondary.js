import React from "react";
import {StyleSheet, Text, TouchableOpacity} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import AntDesign from "react-native-vector-icons/AntDesign";
import {Colors} from "../../styles/Colors";
import PropTypes from "prop-types";


export const ButtonSecondary = (props) => {

    return <TouchableOpacity onPress={props.onPress}
                             style={[styles.container, props.style, {borderColor: props.buttonColor}]}>

        <Text style={[styles.text, {fontSize: wp(props.fontSize), color: props.buttonColor}]}>{props.text}</Text>

        {props.iconVisible ?
            <AntDesign style={styles.icon} size={hp(props.iconSize)} name={"arrowright"}
                       color={props.buttonColor}/>
            : null
        }

    </TouchableOpacity>
}

ButtonSecondary.defaultProps = {
    text: "text",
    iconColor: "white",
    iconVisible: false,
    iconSize: 20,
    fontSize: 17,
    buttonColor: Colors.Primary,
    style: {
        height: hp(44),
        width: wp(276),
        marginBottom: hp(20)
    }
};

ButtonSecondary.propTypes = {
    onPress: PropTypes.func,
    text: PropTypes.string,
    iconColor: PropTypes.string,
    iconVisible: PropTypes.bool,
    iconSize: PropTypes.number,
    fontSize: PropTypes.number,
    buttonColor: PropTypes.string,
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: hp(20),
        borderWidth: 1,
    },
    text: {
        fontSize: wp(17),
        fontWeight: "400",
        color: "white",
        textAlign: "center",
    },
    icon: {
        marginRight: wp(5)
    }
})
