import React from "react";
import {StyleSheet, Text, TouchableOpacity} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import AntDesign from "react-native-vector-icons/AntDesign";
import {Colors} from "../../styles/Colors";
import * as PropTypes from "prop-types";

export const ButtonPrimary = (props) => {

    return <TouchableOpacity onPress={props.onPress}
                             style={[styles.container, props.style, {backgroundColor: props.buttonColor}]}>

        {props.btnLeft &&
        <AntDesign style={styles.icon} size={hp(props.iconSize)} name={"arrowleft"}
                   color={props.iconColor}/>
        }

        <Text style={[styles.text, {
            fontSize: props.fontSize,
            color: props.textColor ? props.textColor : Colors.white
        }]}>{props.text}</Text>

        {(props.iconVisible && !props.btnLeft) ?
            <AntDesign style={styles.icon} size={hp(props.iconSize)} name={"arrowright"}
                       color={props.iconColor}/> : null
        }

    </TouchableOpacity>
}

ButtonPrimary.defaultProps = {
    text: "text",
    iconColor: "white",
    iconVisible: false,
    iconSize: 20,
    fontSize: wp(17),
    buttonColor: Colors.Primary,
    style:{
        height:hp(44),
        width:wp(276),
        marginBottom: hp(20),
    }
};

ButtonPrimary.propTypes = {
    onPress: PropTypes.func,
    text: PropTypes.string,
    iconColor: PropTypes.string,
    iconVisible: PropTypes.bool,
    iconSize: PropTypes.number,
    fontSize: PropTypes.number,
    buttonColor: PropTypes.string,
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderRadius:  hp(20),
    },
    text: {
        fontSize: wp(17),
        fontWeight: "400",
        color: "white",
        textAlign: "center",
    },
    icon: {
        marginRight: wp(5)
    }
})
