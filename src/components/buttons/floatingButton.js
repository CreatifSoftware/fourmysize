import React from "react";
import {StyleSheet, View, Text, TouchableOpacity, Modal, Platform} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {BlurView} from "@react-native-community/blur";
import Styled from 'styled-components';
import * as PropTypes from "prop-types";
import Feather from "react-native-vector-icons/Feather";
import AntDesign from "react-native-vector-icons/AntDesign";
import Draggable from "react-native-draggable";
import {useDispatch, useSelector} from "react-redux";
import {threeDModelTshirt} from "../../assets/Images";
import allActions from "../../redux/action";
import uuid from "react-native-uuid";

const FloatingButton = (props) => {

    const [isShow, setisShow] = React.useState(false)
    const dispatch = useDispatch()
    const {
        id,overline, underline,
        italic, bold,
        textColor, isColor,
        text, textZone,
        title, type, image,myTemplates
    } = useSelector(state => state.design)

    const params = {
        id: id,
        overline: overline,
        underline: underline,
        italic: italic,
        bold: bold,
        textColor: textColor,
        isColor: isColor,
        text: text,
        textZone: textZone,
        title:  myTemplates !== undefined &&  "Kayıtlı Şablon " + (myTemplates.length + 1),
        type: "T-Shirt",
        image: threeDModelTshirt,
    }

    const handleFloatingAction = (name) => {
        if (name === "btn_save") {
            dispatch(allActions.addTemplateAction(params))
        }
        if (name === "btn_finish") {
            dispatch(allActions.resetTemplateAction())
        }
    }

    return <>
        {isShow ?
            <Modal transparent animationType={"slide"}>
                <View style={styles.container}>
                    <BlurWrapper
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={props.blurAmount}
                        reducedTransparencyFallbackColor="white"/>

                    <View style={styles.content}>
                        {props.actions.map((item, index) =>

                            <View key={item.name + "_view"} style={[styles.itemview,
                                {marginTop: hp(index * props.itemTopMargin)}]}>

                                <Text key={item.name + "_text"} style={styles.text}>{item.text}</Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        setisShow(!isShow)
                                        handleFloatingAction(item.name)
                                        props.onPressItem(item.name)
                                    }}
                                    style={styles.button}
                                    key={item.name + "_touch"}>
                                    <AntDesign color={Colors.Primary} size={hp(17)} key={item.name + "_image"}
                                               name={item.icon}/>
                                </TouchableOpacity>
                            </View>
                        )}

                        <TouchableOpacity onPress={() => setisShow(!isShow)}
                                          style={styles.floatingButton}>
                            <AntDesign size={hp(30)} name={"close"} color={Colors.white}/>
                        </TouchableOpacity>
                    </View>


                </View>

            </Modal>
            :
            Platform.OS === "ios" ?
                <Draggable
                    disabled={props.isDraggerEnabled}
                    x={wp(props.x)}
                    y={hp(props.y)}

                    minX={wp(90)}
                    minY={hp(90)}
                    maxX={wp(370)}
                    maxY={hp(600)}>
                    <View style={styles.content}>
                        <TouchableOpacity onPress={() => setisShow(!isShow)}
                                          style={styles.floatingButton}>
                            <Feather size={hp(30)} name={"more-horizontal"} color={Colors.white}/>
                        </TouchableOpacity>
                    </View>
                </Draggable> :
                <View style={[styles.content, {top: 0, left: 0}]}
                >
                    <TouchableOpacity onPress={() => setisShow(!isShow)}
                                      style={styles.floatingButton}>
                        <Feather size={hp(30)} name={"more-horizontal"} color={Colors.white}/>
                    </TouchableOpacity>
                </View>

        }

    </>
}

export default FloatingButton

FloatingButton.defaultProps = {
    actions: [],
    blurAmount: 13,
    itemTopMargin: 26,
    isDraggerEnabled: false,
    x: 90,
    y: 90,
}

FloatingButton.propTypes = {
    blurAmount: PropTypes.number,
    itemTopMargin: PropTypes.number,
    actions: PropTypes.array,
    onPressItem: PropTypes.func,
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    content: {
        position: "absolute",
        right: 25,
        bottom: hp(25),
        alignItems: "flex-end",
    },
    button: {
        height: hp(34),
        width: hp(34),
        backgroundColor: Colors.PrimaryLight,
        borderRadius: hp(30),

        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },
    floatingButton: {
        marginTop: hp(23),
        height: hp(54),
        width: hp(54),
        backgroundColor: Colors.Primary,
        borderRadius: hp(30),
        right: wp(-10),
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },
    itemview: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    text: {
        marginRight: wp(8),
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.Primary,
    }


})

const BlurWrapper = Styled(BlurView)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
