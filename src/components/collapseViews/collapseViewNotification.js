import React from "react";
import {StyleSheet, View, Text, LayoutAnimation, TouchableOpacity, Image} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import {Select} from "../select/select";
import {ButtonSelect} from "../buttons/buttonSelect";

const CollapseViewNotification = (props) => {

    const [boxPosition, setBoxPosition] = React.useState("left");

    const openBox = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        setBoxPosition(boxPosition === "left" ? "right" : "left");
    };

    return (
        <View style={[styles.container, boxPosition === "left" ? null : styles.containerMove]}>
            <View style={styles.titleView}>
                <Image style={styles.icon} source={props.icon}/>
                <Text style={styles.title}>{props.title}</Text>
            </View>
            {boxPosition === "right" &&
            <>
                <Text style={styles.textcontent}>{props.content}</Text>
                <ButtonSelect
                    style={styles.btnCheck}
                    buttonColor={Colors.PrimaryLight}
                    textColor={Colors.Primary}
                    text={"Okundu Olarak İşaretle"} />
            </>
            }

            <TouchableOpacity style={{position: "absolute", marginTop: hp(20), right: wp(23)}}
                              onPress={() => openBox()}>
                {boxPosition === "right" ?
                    <Ionicons name={"close"} color={Colors.Primary} size={wp(14)} style={styles.exit}/> :
                    <SimpleLineIcons color={Colors.content_text} size={hp(14)} name={"arrow-down"}/>}
            </TouchableOpacity>
        </View>
    )
}

export default CollapseViewNotification

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: wp(332),
        height: hp(58),
        borderRadius: hp(10),
        alignItems: "flex-start",
        flexWrap: "wrap",
        backgroundColor: Colors.white,
    },
    containerMove: {
        flex: 1,
        width: wp(332),
        height: "auto",
        borderRadius: hp(10),
        flexWrap: "wrap",
        backgroundColor: Colors.white,
    },
    title: {
        height: hp(35),
        paddingTop:hp(5),
        width: wp(196),
        marginLeft: wp(10),
        fontSize: wp(14),
        lineHeight: hp(13),
        fontWeight: "700",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,

    },
    textcontent: {
        width: wp(286),
        marginBottom: hp(13),
        marginLeft: wp(23),
        fontSize: wp(12),
        lineHeight: hp(20),
        fontWeight: "700",
        color: Colors.content_text,
    },
    exit: {
        borderWidth: 1,
        height: hp(18),
        width: hp(18),
        borderRadius: hp(9),
        textAlign: "center",

        borderColor: Colors.Primary,
    },
    titleView: {
        flexDirection: "row",
        alignItems: "center",
        height: hp(58),
        width: wp(280),
    },
    icon: {
        height: hp(24),
        width: hp(24),
        marginLeft: wp(23),
    },
    btnCheck:{
        width: wp(147),
        marginBottom: hp(13),
        marginLeft: wp(23),
    }
})
