import React from "react";
import {StyleSheet, View, Text, LayoutAnimation, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";

const CollapseView = (props) => {

    const [boxPosition, setBoxPosition] = React.useState("left");

    const openBox = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        setBoxPosition(boxPosition === "left" ? "right" : "left");
    };

    return (
        <View style={[styles.container, boxPosition === "left" ? null : styles.containerMove]}>
            <Text style={styles.title}>{props.title}</Text>
            {boxPosition === "right" && <Text style={styles.textcontent}>{props.content}</Text>}

            <TouchableOpacity style={{position: "absolute", marginTop: hp(20), right: wp(23)}}
                              onPress={() => openBox()}>
                {boxPosition === "right" ?
                    <Ionicons name={"close"} color={Colors.content_text} size={hp(14)} style={styles.exit}/> :
                    <SimpleLineIcons color={Colors.content_text} size={hp(14)} name={"arrow-down"}/>}
            </TouchableOpacity>
        </View>
    )
}

export default CollapseView

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: wp(332),
        height: hp(54),
        borderRadius:  hp(10),
        alignItems: "flex-start",
        flexWrap: "wrap",
        backgroundColor: Colors.white,
    },
    containerMove: {
        flex: 1,
        width: wp(332),
        height: "auto",
        borderRadius: hp(10),
        flexWrap: "wrap",
        backgroundColor: Colors.white,
    },
    title: {
        height: hp(28),
        marginTop: hp(20),
        marginLeft: wp(23),
        textAlign: "center",
        fontSize: wp(12),
        lineHeight: hp(13),
        fontWeight: "700",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,
    },
    textcontent: {
        width: wp(286),
        marginBottom: hp(13),
        marginLeft: wp(23),
        fontSize: wp(12),
        lineHeight: hp(20),
        fontWeight: "700",
        color: Colors.content_text,
    },
    exit: {
        borderWidth: 1,
        height: hp(18),
        width: hp(18),
        borderRadius:  hp(9),
        textAlign: "center",
        justifyContent: "center",
        borderColor: Colors.content_text,
        alignItems: "center",
    },
})
