import React from "react";
import {StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import Slider from "@react-native-community/slider";

const SeekBar = (props) => {

    const [currentvalue, setValue] = React.useState(172)

    return (
        <View style={styles.container}>
            <Text style={styles.seektitle}>{props.title}</Text>

            <View style={{flexDirection: "column"}}>
                <Slider
                    style={{width: 191, height: 40,}}
                    minimumValue={10}
                    maximumValue={250}
                    value={172}
                    minimumTrackTintColor={Colors.Primary}
                    maximumTrackTintColor={Colors.line_tab}
                    onValueChange={value => {
                        props.onChange(value)
                        setValue(value )
                    }}/>

                <Text style={{
                    color: Colors.Primary,
                    fontSize: wp(10),
                    fontWeight: "400",
                    marginLeft: wp(currentvalue/1.65)
                }}>

                    {Math.round(currentvalue)}
                    <Text style={{
                        color: Colors.content_text,
                        fontSize: wp(8),
                        fontWeight: "400"
                    }}> cm</Text>
                </Text>
            </View>
        </View>
    )
}

export default SeekBar

const styles = StyleSheet.create({
    container: {

        backgroundColor: Colors.white,
        height: hp(38),
        width: wp(240),
        marginTop: hp(20),
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    }, seektitle: {
        fontSize: wp(11),
        fontWeight: "400",
        color: Colors.title_active,
        marginRight: wp(12),
        textAlign: "center",
    },
})
