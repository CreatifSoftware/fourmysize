import React from "react";
import {View, StyleSheet} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";

export const LineBottom = (props) => {
    return <View style={styles.container}>
        <View style={[{backgroundColor: Colors.bottomLineColor_1}, styles.line]}/>
        <View style={[{backgroundColor: Colors.bottomLineColor_2}, styles.line]}/>
        <View style={[{backgroundColor: Colors.bottomLineColor_3}, styles.line]}/>
        <View style={[{backgroundColor: Colors.bottomLineColor_4}, styles.line]}/>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",
        position: "absolute",
        bottom: 0,
        width: "100%",
    },
    line: {
        flex: 1,
        height: hp(5),
        width: "100%",
    }
})
