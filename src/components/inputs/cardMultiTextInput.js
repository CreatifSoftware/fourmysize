import React from "react";
import {StyleSheet, View, Text, TextInput} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";

const CardMultiTextInput = (props) => {
    return (
        <View style={styles.container}>

            <Text style={styles.title}>{props.title} : </Text>
            <TextInput
                onChangeText={props.onChangeText}
                value={props.value}
                style={styles.input}
                multiline={true}
                placeholder={props.placeHolder}
            />
        </View>
    )
}

export default CardMultiTextInput

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(340),
        height: hp(89),
        borderRadius:  hp(10),
        flexDirection: "row",
        alignItems: "flex-start",
        marginBottom: hp(17),
        marginLeft: wp(10),
        marginRight: wp(10),

        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 0.9,
    },
    title: {
        marginTop:  hp(12),
        marginLeft: wp(15),
        marginRight: wp(5),

        fontSize: wp(12),
        color: Colors.content_text,
    },
    input: {
        marginTop:Platform.OS === 'ios' ? hp(8): hp(5),
        fontSize: wp(14),
        paddingTop:hp(2),
        color: Colors.content_text,
        lineHeight: hp(16),
        width: wp(220),

    }
})
