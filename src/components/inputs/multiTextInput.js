import React from "react";
import {StyleSheet, View, TextInput} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";

const MultiTextInput = (props) => {
    return (
        <View style={[styles.container,props.style]}>
            <TextInput
                onChangeText={props.onChangeText}
                value={props.value}
                multiline={true}
                style={styles.input}
                placeholder={props.placeHolder}
            />
        </View>
    )
}

export default MultiTextInput

const styles = StyleSheet.create({
    container: {
        marginTop: hp(20),
        backgroundColor: Colors.input_background,
        height: hp(108),
        width: wp(276),
        borderRadius:  hp(10),
        padding: hp(10),
        marginBottom:hp(20),
    },
    input: {
        textAlignVertical: 'top',
        fontSize: wp(12),
        color: Colors.content_text,
        lineHeight: hp(16),
    }
})
