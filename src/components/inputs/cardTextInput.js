import React from "react";
import {StyleSheet, View, Text, TextInput} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";

const CardTextInput = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{props.title} : </Text>
            <TextInput
                onChangeText={props.onChangeText}
                value={props.value}
                style={styles.input}
                placeholder={props.placeHolder}
            />
        </View>
    )
}

export default CardTextInput

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(340),
        height: hp(46),
        borderRadius: hp(10),
        flexDirection: "row",
        alignItems: "center",
        marginBottom: hp(17),
        marginLeft: wp(10),
        marginRight: wp(10),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    title: {
        marginLeft: wp(15),
        marginRight: wp(5),

        fontSize: wp(12),
        color: Colors.content_text,
    },
    input: {
        width:wp(250),
        fontSize: wp(14),
        color: Colors.content_text,
        lineHeight: hp(16),
    }
})



