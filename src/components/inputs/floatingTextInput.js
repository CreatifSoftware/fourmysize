import React, {Fragment, useState} from 'react'
import {StyleSheet, View, TouchableOpacity, Image, Text, TextInput} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import {Colors} from '../../styles/Colors';
import {hp, wp} from '../../styles/Dimen';
import {HorizontalSeperator} from "../seperators/horizontalSeperator";

export const FloatingTextInput = (props) => {

    const [focused, setFocused] = useState(false)

    const onFocus = () => {
        setFocused(!focused)
        props.onFocus
    }

    const getMaskByType = () => {
        if (props.type === 'only-chars') {
        }
    }

    return (
        <View style={styles.container}>
            {props.value
                ?
                <Text style={focused ? styles.activeFloatingLabel : styles.floatingLabel}>{props.label}</Text>
                :
                null
            }

            <View>
                {props.type ?

                    <TextInputMask
                        {...props}
                        editable={!props.disabled}
                        placeholderTextColor={Colors.title_active}
                        style={[props.disabled ? styles.disabledTextInput : styles.textInput, props.style]}
                        autoCorrect={false}
                        onFocus={onFocus}
                        onBlur={onFocus}
                        returnKeyType={'next'}
                        onChangeText={props.onChangeText}
                        value={props.value}
                        type={props.type}
                        maxLength={props.maxLength}
                        options={props.options}/>
                    :
                    <TextInput
                        {...props}
                        editable={!props.disabled}
                        placeholderTextColor={props.disabled ? Colors.PlaceHolder : Colors.title_active}
                        style={[props.disabled ? styles.disabledTextInput : styles.textInput, props.style]}
                        autoCorrect={false}
                        onFocus={onFocus}
                        onBlur={onFocus}
                        returnKeyType={'next'}
                        onChangeText={props.onChangeText}
                        value={props.value}
                    />

                }

                {props.iconSource && <Image style={styles.iconButtonLeft} source={props.iconSource}/>}

                <View style={{flexDirection: "row"}}>
                    {props.iconPassHide &&
                    <TouchableOpacity
                        onPress={props.iconPassHideOnPress}

                        style={props.iconQuestion ? [styles.iconButtonRight, {marginRight: wp(30)}] :
                            [styles.iconButtonRight, {marginRight: wp(0)}]}>
                        <Image
                            style={{
                                width: hp(18),
                                height: hp(18)
                            }}
                            source={props.iconPassHide}
                        />
                    </TouchableOpacity>}
                    {props.iconQuestion ?
                        <TouchableOpacity
                            onPress={props.iconForgotOnPress}
                            style={[styles.iconButtonRight, {}]}>
                            <Image
                                style={[styles.icon, {
                                    width: hp(15),
                                    height: hp(15)
                                }]}
                                source={props.iconQuestion}
                            />
                        </TouchableOpacity> : null
                    }
                </View>

            </View>

            <HorizontalSeperator
                style={{backgroundColor: props.disabled ? Colors.PlaceHolder : focused ? Colors.Error : Colors.title_active}}/>

            {props.error === "" ? null : <Text style={styles.errorText}>{props.error}</Text>}

        </View>

    )
}

FloatingTextInput.defaultProps = {
    options: {
        maskType: 'BRL',
        withDDD: true,
        dddMask: '0 999 999 99 99'
    },
    maxLength: 16,
}

const styles = StyleSheet.create({
    container: {
        marginTop: hp(20),
        backgroundColor: Colors.white,
        marginHorizontal: wp(20),
    },
    textInput: {
        height: hp(40),
        fontSize: wp(16),
        marginLeft: wp(45),
        backgroundColor: 'transparent',
        color: Colors.title_active
    },
    disabledTextInput: {
        height: hp(40),
        fontSize: wp(16),
        marginLeft: wp(45),
        backgroundColor: 'transparent',
        color: Colors.title_active
    },
    icon: {},
    iconButtonLeft: {
        width: hp(24),
        height: hp(24),
        position: 'absolute',
        start: wp(12),
        bottom: hp(8),
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconButtonRight: {
        width: hp(24),
        height: hp(24),
        position: 'absolute',
        end: wp(12),
        bottom: hp(8),
        justifyContent: 'center',
        alignItems: 'center'
    },
    floatingLabel: {
        fontSize: wp(12),
        color: Colors.title_active,
        fontFamily: 'Gilroy-Bold'
    },
    activeFloatingLabel: {
        fontSize: wp(12),
        color: Colors.title_active,
        fontFamily: 'Gilroy-Bold'
    },
    errorText: {
        marginTop: hp(4),
        fontSize: wp(12),
        color: Colors.Error,
        fontFamily: 'Gilroy-Bold',

    }
})
