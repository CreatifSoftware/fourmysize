import React from "react";
import {StyleSheet, View} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ProgressSteps, ProgressStep} from 'react-native-progress-steps';

const CustomProgressSteps = (props) => {
    return <View style={styles.container}>
        <ProgressSteps progressBarColor={Colors.StepperLine}
                       disabledStepIconColor={Colors.StepperLine}
                       activeStep={props.activeStep}
                       activeStepIconColor={Colors.Primary}
                       activeStepIconBorderColor={Colors.Primary}
                       labelFontSize={hp(10)}
                       activeLabelColor={Colors.Primary}
                       labelColor={Colors.StepperLine}
                       completedStepIconColor={Colors.Primary}
                       completedProgressBarColor={Colors.Primary}
                       completedLabelColor={Colors.Primary}
                       isComplete={props.isComplete}
                       activeStepNumColor={Colors.white}
                       labelFontFamily={"Gilroy-Regular"}>

            <ProgressStep removeBtnRow={true} label="Siparişiniz Alındı"/>
            <ProgressStep removeBtnRow={true} label="Siparişiniz Hazırlanıyor"/>
            <ProgressStep removeBtnRow={true} label="Siparişiniz Kargoya Verildi"/>
            <ProgressStep removeBtnRow={true} label="Siparişiniz Teslim Edildi"/>
        </ProgressSteps>
    </View>
}

export default CustomProgressSteps

const styles = StyleSheet.create({
    container: {
        height: hp(123),
        backgroundColor: Colors.white
    }
})





