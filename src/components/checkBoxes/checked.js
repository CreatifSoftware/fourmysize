import React from 'react'
import {StyleSheet, View, Image} from 'react-native';
import {hp, wp} from '../../styles/Dimen';
import {iconChecked, iconUnChecked} from "../../assets/Icon";
import PropTypes from "prop-types";

export const Checked = (props) => {

    return <View>
        {props.isChecked ? <Image style={styles.container} source={iconChecked}/> :
            <Image style={styles.container} source={iconUnChecked}/>
        }
    </View>
}

Checked.propTypes = {
    isChecked: PropTypes.bool,
}

const styles = StyleSheet.create({
    container: {
        width: hp(18),
        height: hp(18),
        marginRight: wp(13),
    }
})
