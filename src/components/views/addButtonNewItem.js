import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import * as RootNavigation from "../../navigation/RootNavigation";
import {iconAddtoCard} from "../../assets/Icon";

const AddButtonNewItem = (props) => {
    return <View style={[styles.container,props.style]}>
        <TouchableOpacity onPress={() => RootNavigation.navigate(props.redirect, {})}>
            <View style={{justifyContent: "center", alignItems: "center"}}>
                <View style={styles.footerContainer}>

                    <Image source={iconAddtoCard}/>
                    <Text style={styles.addtext}>{props.title}</Text>
                </View>
            </View>
        </TouchableOpacity>
    </View>
}

export default AddButtonNewItem

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    footerContainer: {
        height: hp(94),
        width: wp(334),
        marginBottom: hp(50),
        alignItems: "center",
        borderRadius: hp(20),
        borderStyle: 'dashed',
        borderColor: Colors.Secondary,
        borderWidth: 1,
        marginTop: hp(37)
    },
    addtext: {
        fontSize: wp(14),
        fontWeight: "400",
        color: Colors.Secondary,
    }

})
