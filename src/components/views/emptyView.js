import React from "react";
import {Image, StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import PropTypes from "prop-types";
import * as Img from "../../assets/Images"


const type = {
    address: 'address',
    card: 'card',
    order: 'order',
    coupon: 'coupon',
    notification: 'notification',
}

const EmptyView = (props) => {
    return (
        <View style={styles.container}>
            {
                props.type === type.address ?
                    <Image style={styles.image} source={Img.EmptyAddress}/> :
                    props.type === type.card ?
                        <Image style={styles.image} source={Img.EmptyCard}/> :
                        props.type === type.order ?
                            <Image style={styles.image} source={Img.EmptyMyOrders}/> :
                            props.type === type.coupon ?
                                <Image style={styles.image} source={Img.EmptyCoupon}/> :
                                props.type === type.notification ?
                                    <Image style={styles.image} source={Img.EmptyNotification}/> : null
            }
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.subtitle}>{props.subtitle}</Text>

        </View>
    )
}

EmptyView.propTypes = {
    type: PropTypes.oneOf(['address', 'card', 'order', 'coupon', 'notification']).isRequired,
    title: PropTypes.string,
    subtitle: PropTypes.string,
}

export default EmptyView

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        width: wp(237),
        height: hp(226),
        resizeMode: "contain",
        marginTop: hp(22),
    },
    title: {
        width: wp(307),
        fontSize: wp(30),
        fontWeight: "800",
        lineHeight: hp(37),
        color: Colors.title_active,
        fontFamily: "Gilroy-Bold",
        textAlign: "center",
        marginTop: hp(33),
    },
    subtitle: {
        width: wp(307),
        fontSize: wp(14),
        fontWeight: "400",
        lineHeight: hp(20),
        color: Colors.content_text,
        textAlign: "center",
        marginTop: hp(15),
    }
})
