import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import AntDesign from "react-native-vector-icons/AntDesign";

const CouponItem = ({item}) => {
    return <View style={styles.container}>
        <Text style={styles.title}>{item.title}{" "}
            <Text style={[styles.coupon, {color: item.colorDark}]}>{item.coupon}</Text>
        </Text>

        <Text style={[styles.subtitle, {color: item.colorDark}]}>{item.subtitle}</Text>

        <View style={[styles.date, {backgroundColor: item.colorLight}]}>
            <Text style={[styles.dateTitle, {color: item.colorDark}]}>{item.dateTitle}</Text>
        </View>
    </View>
}

export default CouponItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(341),
        height: hp(83),
        borderRadius: hp(20),
        marginBottom: hp(15),

        alignItems: "center",
        marginTop: hp(37),
        marginLeft: wp(17),
        marginRight: wp(17),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    title: {
        fontSize: wp(16),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        marginBottom: hp(5),
        marginTop: hp(15),
    },
    subtitle: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.Heading,
        marginBottom: hp(10)
    },
    coupon: {
        fontSize: wp(16),
        fontWeight: "900",
        color: Colors.Heading,
        fontFamily: "Gilroy-bold",
        marginBottom: hp(10)
    },
    date: {
        width: wp(137),
        height: hp(32),
        borderRadius: hp(90),
        bottom: hp(-15),
        justifyContent: "center",
        alignItems: "center",
        position:"absolute",
    },
    dateTitle: {
        fontSize: wp(10),
        fontWeight: "400",
        color: Colors.Heading,
    },
})
