import React from "react";
import {StyleSheet, View,TouchableWithoutFeedback} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import CollapseView from "../collapseViews/collapseView";

const SSSDetailItem = ({item}) => {
    return <TouchableWithoutFeedback>
        <View style={styles.container}>
            <CollapseView
                title={item.title}
                content={item.content}/>
        </View>
    </TouchableWithoutFeedback>
}

export default SSSDetailItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        borderRadius:  hp(10),
        marginTop: hp(16),
        marginLeft: wp(10),
        marginRight: wp(10),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
})
