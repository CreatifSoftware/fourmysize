import React from "react";
import {StyleSheet, View, Text, Image, TouchableWithoutFeedback} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import * as RootNavigation from "../../navigation/RootNavigation";

const SSSItem = ({item}) => {
    return <TouchableWithoutFeedback onPress={() => RootNavigation.navigate("SSSPageDetails",
        {id: item.id, title: item.title})}>
        <View style={styles.container}>
            <Image style={styles.image} source={item.icon}/>
            <Text style={styles.title}>{item.title}</Text>
        </View>
    </TouchableWithoutFeedback>
}

export default SSSItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(95),
        height: hp(101),
        borderRadius:  hp(20),
        alignItems: "center",
        justifyContent: "center",
        marginTop: hp(32),
        marginLeft: wp(10),
        marginRight: wp(10),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    image: {
        width: wp(50),
        height: hp(50),
        resizeMode:"contain",
        marginBottom: hp(12),
    },
    title: {
        textAlign: "center",
        width: wp(60),
        fontSize: wp(12),
        lineHeight: hp(13),
        fontWeight: "700",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,
    },

})
