import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt} from "../../assets/Images";
import {iconPrepare, iconInCargo, iconComplated} from "../../assets/Icon";
import * as RootNavigation from "../../navigation/RootNavigation"

const HistoryOrderItem = ({item}) => {
    return (<TouchableOpacity onPress={() => RootNavigation.navigate("OrderDetailsStatus", {
        id: item.id
    })}>
        <View style={styles.container}>

            <Image style={styles.image} source={threeDModelTshirt}/>

            <View style={{marginTop: hp(30)}}>
                <View style={styles.content}>
                    <Text style={styles.title}>Sipariş No</Text>
                    <Text style={styles.subtitle}>{item.orderNumber}</Text>
                </View>

                <View style={styles.content}>
                    <Text style={styles.title}>Sipariş Tarihi</Text>
                    <Text style={styles.subtitle}>{item.orderDate}</Text>
                </View>

                <View style={styles.content}>
                    <Text style={styles.title}>Sipariş Durumu</Text>

                    {item.orderStatus === 0 &&
                    <View style={styles.status}>
                        <Image source={iconPrepare}/>
                        <Text style={styles.status1text}>Hazırlanıyor</Text>
                    </View>}

                    {item.orderStatus === 1 &&
                    <View style={styles.status}>
                        <Image source={iconPrepare}/>
                        <Text style={styles.status1text}>Hazırlanıyor</Text>
                    </View>}

                    {item.orderStatus === 2 &&
                    <View style={styles.status}>
                        <Image source={iconInCargo}/>
                        <Text style={[styles.status1text, {color: Colors.supplement_1}]}>Kargoda</Text>
                    </View>}

                    {item.orderStatus === 3 &&
                    <View style={styles.status}>
                        <Image source={iconComplated}/>
                        <Text style={[styles.status1text, {color: Colors.Success}]}>Teslim Edildi</Text>
                    </View>}


                </View>
            </View>

        </View>
    </TouchableOpacity>)
}

export default HistoryOrderItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(340),
        height: hp(120),
        borderRadius: hp(20),
        flexDirection: "row",
        alignItems: "center",
        marginTop: hp(32),
        shadowColor: "#ffffff",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    image: {
        width: wp(70),
        height: hp(94),
        resizeMode: "contain",
        marginLeft: wp(21)
    },
    content: {
        marginLeft: wp(17),
        flexDirection: "row",


    },
    title: {
        width: wp(100),
        fontSize: wp(12),
        fontWeight: "600",
        fontFamily: "Gilroy-Bold",
        color: Colors.Heading,
        marginBottom: hp(15)
    },
    subtitle: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.content_text,
    },
    status1text: {
        fontSize: wp(12),
        fontWeight: "600",
        color: Colors.supplement_2,
        marginLeft: wp(5),
    },
    status: {
        flexDirection: "row",
        alignItems: "center",
        top: -7
    }

})
