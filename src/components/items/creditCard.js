import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import AntDesign from "react-native-vector-icons/AntDesign";

const CreditCard = (props) => {
    return <View style={styles.container}>
        {props.item.id === props.select.id ?
            <View style={[styles.item]}>
                <AntDesign name={"check"} size={hp(10)} color={Colors.white}/>
            </View> : null}

        <TouchableOpacity onPress={() => props.onPress(props.item)}>
            <View style={styles.content}>
                <Image source={props.item.cardtype}/>
                <View>
                    <Text style={styles.title}>{props.item.title}</Text>
                    <Text style={styles.title}>{props.item.cardnumber}</Text>
                </View>

            </View>
        </TouchableOpacity>
    </View>
}

export default CreditCard

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(320),
        height: hp(94),
        borderRadius:  hp(20),
        flexDirection: "row",
        alignItems: "center",
        marginTop: hp(32),
        marginLeft: wp(10),
        marginRight: wp(10),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    content: {
        justifyContent: "center",
        alignItems: "center",

        flexDirection: "row",
    },
    title: {
        fontSize: wp(14),
        fontWeight: "400",
        color: Colors.Heading,
        marginBottom: hp(10)
    },
    cardnumber: {
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.PagerNormal,
        marginBottom: hp(10)
    },
    username: {
        fontSize: wp(14),
        fontWeight: "600",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,
        marginBottom: hp(3)
    },
    adress: {
        fontSize: wp(14),
        fontWeight: "400",
        color: Colors.PlaceHolder,
        width: wp(290),
        marginBottom: hp(3)
    },
    item: {
        position: "absolute",
        right: 0,
        top: 0,
        backgroundColor: Colors.Primary,
        borderRadius:  hp(20),
        height: hp(17),
        width: hp(17),
        marginRight: wp(32),
        marginTop: hp(26),
        justifyContent: "center",
        alignItems: "center",
    },


})
