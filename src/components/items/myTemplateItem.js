import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt} from "../../assets/Images";
import CountDown from "../countDowns/countDown";
import {
    Menu,
    MenuOptions,
    MenuContext,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';
import {useDispatch} from "react-redux";
import {updateOrderAction} from "../../redux/action/orderAction";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";

const MyTemplateItem = (props) => {


    return <>
        <View style={styles.container}>

            <Image style={styles.image} source={threeDModelTshirt}/>
            <View style={styles.content}>

                <Text
                    style={styles.stokon}>{props.item.type}</Text>
                <Text style={styles.title}>{props.item.title}</Text>

                <TouchableOpacity style={styles.goButton}>
                    <AntDesign name={"arrowright"} size={hp(17)} color={Colors.white}/>
                </TouchableOpacity>
            </View>
        </View>
    </>

}

export default MyTemplateItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(340),
        height: hp(120),
        borderRadius: hp(20),
        flexDirection: "row",
        alignItems: "center",
        marginTop: hp(15),
        shadowColor: "#ffffff",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.5,
    },
    image: {
        width: wp(70),
        height: hp(94),
        resizeMode: "contain",
        marginLeft: wp(21)
    },
    content: {
        marginLeft: wp(17)
    },
    stokon: {
        fontSize: wp(8),
        fontWeight: "400",
        color: Colors.Success,
        marginBottom: hp(4)
    },
    title: {
        width: wp(200),
        fontSize: wp(18),
        fontWeight: "600",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,
        marginBottom: hp(3)
    },
    colorsizeview: {
        flexDirection: "row",
    },
    color: {
        width: wp(101),
        fontSize: wp(10),
        fontWeight: "400",
        color: Colors.PlaceHolder
    },
    icon: {
        position: "absolute",
        width: wp(101),
    },
    price: {
        position: "absolute",
        right: wp(15),
        bottom: hp(15),
        fontSize: wp(22),
        fontFamily: "Gilroy-Bold",
        fontWeight: "600",
        color: Colors.Primary
    },
    goButton: {
        width: wp(30),
        height: hp(25),
        borderRadius: hp(8),
        backgroundColor: Colors.Secondary,
        position: "absolute",
        right: wp(-10),
        justifyContent: "center",
        alignItems: "center",
    },

})
