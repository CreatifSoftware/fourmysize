import React from "react";
import {StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataSize} from "../../api/data";

const [indexSize, setIndexSize] = React.useState(dataSize);

const updateItemsSize = (index) => {
    const list = indexSize.map((item) => {
        item.selected = false;
        return item;
    });
    list[index].selected = true;
    setIndexSize(list);
};

const ItemSize = (item, index) => {
    return (
        <TouchableOpacity onPress={() => updateItemsSize(index)}><View
            style={item.selected ? styles.itemSizeContainerActive : styles.itemSizeContainer}>
            <Text style={item.selected ? styles.itemtitleActive : styles.itemtitle}>{item.size}</Text>
        </View></TouchableOpacity>)
}

export default ItemSize

const styles = StyleSheet.create({
    itemSizeContainerActive: {
        height: hp(40),
        marginLeft: wp(18),
        justifyContent: "center",
        backgroundColor: Colors.Primary,
        borderRadius:  hp(15),
    },
    itemSizeContainer: {
        height: hp(40),
        marginLeft: wp(18),
        justifyContent: "center",
        backgroundColor: Colors.line_tab,
        borderRadius:  hp(15),
    },
    itemtitle: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "400",
    },
    itemtitleActive: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        color: Colors.white,
        fontSize: wp(12),
        fontWeight: "400",
    }
})
