import React from "react";
import {Image, StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt} from "../../assets/Images";
import CountDown from "../countDowns/countDown";
import {
    Menu,
    MenuOptions,
    MenuContext,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';
import {useDispatch} from "react-redux";
import {updateOrderAction} from "../../redux/action/orderAction";

const OrderItem = (props) => {

    const [count, setCount] = React.useState(props.item.count)
    const [price, setPrice] = React.useState(props.item.price)

    let params = {
        'id': props.item.id,
        'stockStatus': props.item.stockStatus,
        'title': props.item.title,
        'color': props.item.color,
        'size': props.item.size,
        'count': count,
        'unitPrice': props.item.unitPrice,
        'price': price,
    }

    React.useEffect(() => {
        setPrice(count * parseInt(props.item.unitPrice));
        props.onPress(params, props.index)
    }, [count, price])

    return (<>
            <View style={styles.container}>
                <Text style={styles.price}>{price} TL</Text>
                <Image style={styles.image} source={threeDModelTshirt}/>
                <View style={styles.content}>

                    <Text
                        style={props.item.stockStatus ? styles.stokon : styles.stokoff}>{props.item.stockStatus ? " Stokta Mevcut" : "Stokta Mevcut Değil"}</Text>
                    <Text style={styles.title}>{props.item.title}</Text>

                    <View style={styles.colorsizeview}>
                        <Text style={styles.color}>Renk: {props.item.color}</Text>
                        <Text style={styles.color}>Beden: {props.item.size}</Text>
                    </View>

                    <CountDown count={count} onPress={(index) => {
                        setCount(index);

                    }}/>
                </View>
            </View>
        </>
    )
}

export default OrderItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(340),
        height: hp(120),
        borderRadius: hp(20),
        flexDirection: "row",
        alignItems: "center",
        marginTop: hp(15),
        shadowColor: "#c1c1c1",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.5,
    },
    image: {
        width: wp(70),
        height: hp(94),
        resizeMode: "contain",
        marginLeft: wp(21)
    },
    content: {
        marginLeft: wp(17)
    },
    stokon: {
        fontSize: wp(8),
        fontWeight: "400",
        color: Colors.Success,
        marginBottom: hp(4)
    },
    stokoff: {
        fontSize: wp(8),
        fontWeight: "400",
        color: Colors.Error,
        marginBottom: hp(4)
    },
    title: {
        width: wp(200),
        fontSize: wp(18),
        fontWeight: "600",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,
        marginBottom: hp(3)
    },
    colorsizeview: {
        flexDirection: "row",
    },
    color: {
        width: wp(101),
        fontSize: wp(10),
        fontWeight: "400",
        color: Colors.PlaceHolder
    },
    icon: {
        position: "absolute",
        width: wp(101),
    },
    price: {
        position: "absolute",
        right: wp(15),
        bottom: hp(15),
        fontSize: wp(22),
        fontFamily: "Gilroy-Bold",
        fontWeight: "600",
        color: Colors.Primary
    },

})
