import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt} from "../../assets/Images";
import CountDown from "../countDowns/countDown";
import {
    Menu,
    MenuOptions,
    MenuContext,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';
import {useDispatch} from "react-redux";
import {updateOrderAction} from "../../redux/action/orderAction";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import Entypo from "react-native-vector-icons/Entypo";
import {iconToolTip} from "../../assets/Icon";

const MyFavoriteItem = (props) => {


    return <>
        <View style={styles.container}>

            <Image style={styles.image} source={threeDModelTshirt}/>
            <View style={styles.content}>

                <Text
                    style={props.item.stockStatus ? styles.stokon : styles.stokoff}>{props.item.stockStatus ? " Stokta Mevcut" : "Stokta Mevcut Değil"}</Text>

                <Text style={styles.title}>{props.item.title}</Text>


                <View style={{flexDirection: "row", marginTop: hp(15)}}>
                    <Text style={[styles.price,
                        {color: props.item.stockStatus ? Colors.Primary:Colors.StepperLine}]}>
                        65 TL</Text>

                    {!props.item.stockStatus &&
                    <View style={styles.noti}>
                        <Text style={styles.notiText}>{props.item.title}</Text>
                        <Image style={{position: "absolute", right: wp(-6)}} source={iconToolTip}/>
                    </View>
                    }
                </View>

            </View>

            <View style={styles.right}>

                <TouchableOpacity style={styles.favoriteButton}>
                    <AntDesign name={"heart"} size={wp(14)} color={Colors.white}/>
                </TouchableOpacity>

                {props.item.stockStatus ?
                    <TouchableOpacity style={styles.addButton}>
                        <Entypo name={"plus"} size={wp(16)} color={Colors.white}/>
                    </TouchableOpacity> :


                    <TouchableOpacity style={styles.alarmButton}>
                        <Feather name={"bell"} size={wp(16)} color={Colors.Secondary}/>
                    </TouchableOpacity>

                }

            </View>
        </View>
    </>

}

export default MyFavoriteItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(340),
        height: hp(120),
        borderRadius: hp(20),
        flexDirection: "row",

        marginTop: hp(15),
        shadowColor: "#ffffff",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.5,
    },
    image: {
        width: wp(70),
        height: hp(94),
        resizeMode: "contain",
        marginTop: hp(10),
        marginLeft: wp(21)
    },
    content: {
        marginLeft: wp(17)
    },
    stokon: {
        marginTop: hp(23),
        fontSize: wp(8),
        fontWeight: "400",
        color: Colors.Success,
        marginBottom: hp(7)
    },
    stokoff: {
        marginTop: hp(23),
        fontSize: wp(8),
        fontWeight: "400",
        color: Colors.Error,
        marginBottom: hp(7)
    },
    title: {
        width: wp(200),
        fontSize: wp(18),
        fontWeight: "600",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,
        marginBottom: hp(3)
    },
    colorsizeview: {
        flexDirection: "row",
    },
    color: {
        width: wp(101),
        fontSize: wp(10),
        fontWeight: "400",
        color: Colors.PlaceHolder
    },
    icon: {
        position: "absolute",
        width: wp(101),
    },
    price: {
        marginTop: hp(5),
        fontSize: wp(22),
        fontFamily: "Gilroy-Bold",
        fontWeight: "600",
        marginRight: wp(15),
        color: Colors.StepperLine
    },
    right: {
        height: hp(118),
        position: "absolute",
        top: 0,
        right: wp(20),
        justifyContent: "center",
        alignItems: "center",

    },
    favoriteButton: {
        width: hp(25),
        height: hp(25),
        borderRadius: hp(15),
        marginTop: hp(15),
        backgroundColor: Colors.Primary,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    addButton: {
        width: wp(30),
        height: hp(25),
        borderRadius: hp(8),
        marginTop: hp(15),
        backgroundColor: Colors.Primary,
        justifyContent: "center",
        alignItems: "center",
    },
    alarmButton: {
        width: wp(30),
        height: hp(25),
        borderRadius: hp(8),
        marginTop: hp(15),
        backgroundColor: Colors.SecondaryLight,
        justifyContent: "center",
        alignItems: "center",
    },
    noti: {
        height: hp(28),
        backgroundColor: Colors.SecondaryLight,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: hp(10),
        minWidth: wp(99),
    },
    notiText: {
        fontSize: wp(10),
        fontWeight: "600",
        fontFamily: "Gilroy-Bold",
        color: Colors.Secondary,
        textAlign: "center",
        marginLeft: wp(15),
        marginRight: wp(15),
    },

})
