import React from "react";
import {StyleSheet, View, Text, TouchableWithoutFeedback} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import TextButton from "../buttons/textButton";
import AntDesign from "react-native-vector-icons/AntDesign";
import * as RootNavigation from "../../navigation/RootNavigation";

const AddressItem = (props) => {
    return <TouchableWithoutFeedback onPress={() => props.onPress(props.item)}>
        <View style={styles.container}>
            {props.item.id === props.select.id ?
                <View style={[styles.item]}>
                    <AntDesign name={"check"} size={hp(10)} color={Colors.white}/>
                </View> : null}

            <View style={styles.content}>
                <Text style={styles.title}>{props.item.title}</Text>
                <Text style={styles.username}>{props.item.username}</Text>
                <Text style={styles.adress}>{props.item.adress}</Text>
                <TextButton
                    onPress={() => RootNavigation.navigate("AddAddress", {
                        id:props.item.id
                    })}
                    color={Colors.Success}
                    text={"Düzenle"}/>
            </View>
        </View>
    </TouchableWithoutFeedback>
}

export default AddressItem

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        width: wp(334),
        height: hp(132),
        borderRadius:  hp(20),
        flexDirection: "row",
        alignItems: "center",
        marginTop: hp(32),
        marginLeft: wp(10),
        marginRight: wp(10),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    content: {
        marginLeft: wp(17)
    },
    title: {
        fontSize: wp(18),
        fontWeight: "700",
        fontFamily: "Gilroy-Bold",
        color: Colors.Secondary,
        marginBottom: hp(10)
    },
    username: {
        fontSize: wp(14),
        fontWeight: "600",
        fontFamily: "Gilroy-Bold",
        color: Colors.title_active,
        marginBottom: hp(3)
    },
    adress: {
        fontSize: wp(14),
        fontWeight: "400",
        color: Colors.PlaceHolder,
        width: wp(290),
        marginBottom: hp(3)
    },
    item: {
        position: "absolute",
        right: 0,
        top: 0,
        backgroundColor: Colors.Primary,
        borderRadius:  hp(20),
        height: hp(17),
        width: hp(17),
        marginRight: wp(32),
        marginTop: hp(26),
        justifyContent: "center",
        alignItems: "center",
    },
})
