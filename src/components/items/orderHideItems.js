import React from "react";
import {StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import {useDispatch} from "react-redux";
import {removeOrderAction} from "../../redux/action/orderAction";

const OrderHideItems = (props) => {

    const dispatch = useDispatch()

    return <View style={styles.rowBack}>
        {
            props.leftText ? <TouchableOpacity>
                <View style={styles.left}>
                    <MaterialIcons
                        style={{marginLeft: wp(36)}}
                        name={"favorite-outline"}
                        size={hp(17)}
                        color={Colors.white}/>
                    <Text style={styles.lefttext}>{props.leftText}</Text>
                </View>
            </TouchableOpacity>: <View style={styles.leftnull}/>
        }
        {
            props.rightText &&
            <TouchableOpacity onPress={() => props.onChange()}>
                <View style={styles.right}>
                    <AntDesign
                        style={{marginRight: wp(40)}}
                        name={"delete"}
                        size={hp(17)}
                        color={Colors.white}/>
                    <Text style={styles.righttext}>{props.rightText}</Text>

                </View>
            </TouchableOpacity>
        }
    </View>
}

export default OrderHideItems

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    }, rowBack: {
        width: wp(340),
        height: hp(115),
        marginTop: hp(17),
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.content_text,
        borderRadius: hp(20),
    }, left: {
        borderBottomStartRadius: hp(20),
        borderTopStartRadius: hp(20),
        backgroundColor: Colors.Primary,
        height: hp(115),
        width: wp(170),
        justifyContent: "center",
    },
    leftnull: {
        borderBottomStartRadius: hp(20),
        borderTopStartRadius: hp(20),
    },
    lefttext: {
        width: wp(62),
        fontSize: wp(11),
        color: Colors.white,
        textAlign: "center",
        lineHeight: hp(13),
        marginLeft: wp(16),
    },
    right: {
        borderBottomEndRadius: hp(20),
        borderTopEndRadius: hp(20),
        backgroundColor: Colors.content_text,
        height: hp(115),
        width: wp(170),
        justifyContent: "center",
        alignItems: "flex-end"
    },
    righttext: {
        width: wp(62),
        fontSize: wp(11),
        color: Colors.white,
        textAlign: "center",
        lineHeight: hp(13),
        marginRight: wp(16),
    },
})
