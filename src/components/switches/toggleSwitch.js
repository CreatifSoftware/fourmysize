import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, LayoutAnimation} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {toogleClose, toogleOpen} from "../../assets/Images";

const ToggleSwitch = (props) => {

    const [boxPosition, setBoxPosition] = React.useState("left");

    const openBox = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        setBoxPosition(boxPosition === "left" ? "right" : "left");
    };

    return (<View style={styles.container}>
            <TouchableOpacity onPress={() => {
                props.onPress();
                openBox()
            }}>
                {props.isOpen ?
                    <Image style={styles.image} source={toogleOpen}/>
                    : <Image style={styles.image} source={toogleClose}/>
                }
            </TouchableOpacity>
            <Text style={styles.title}>{props.label}</Text>
        </View>
    )
}

export default ToggleSwitch

const styles = StyleSheet.create({
    container: {
        marginLeft: wp(16),
        flexDirection: "row",
    },
    image: {
        height: hp(47),
        width: wp(54),
        resizeMode:"contain",
        marginRight: wp(16),
    },
    title: {
        marginTop: hp(5),
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.title_active,
        width: wp(245),
    },
})
