import React from "react";
import {StyleSheet, Modal, ActivityIndicator} from "react-native";
import {hp, wp} from "../styles/Dimen";
import {Colors} from "../styles/Colors";
import {BlurView} from "@react-native-community/blur";
import Styled from "styled-components";

const Loading = (props) => {
    return (
        <Modal transparent={true} animated={"slide"} style={styles.container}>
            <BlurWrapper
                style={styles.absolute}
                blurType="dark"
                blurAmount={9}
                blurAmount={props.blurAmount}
                reducedTransparencyFallbackColor="white"/>

            <ActivityIndicator style={{alignItems: "center", marginTop: hp(300)}}/>

        </Modal>
    )
}

export default Loading

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.black,
        justifyContent: "center",
        alignItems: "center",
    }
})

const BlurWrapper = Styled(BlurView)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
