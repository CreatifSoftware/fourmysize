export const Colors = {

    background: "#F6F9FB",

    Primary: '#FE4550',
    Secondary: '#6B68FE',
    Error: '#ED2E7E',
    Success: '#00B3A6',
    supplement_1: '#FC6B4D',
    supplement_2: '#5DCEEE',
    supplement_3: '#9663FF',

    Secondary_1: "#FE4550",

    PrimaryDark: "#D61420",
    PrimaryLight: "#FFEFF4",
    SecondaryLight: "#EDEDFF",
    PlaceHolder: "#959DB3",
    PagerNormal: "#959DB3",
    Heading: "#261548",
    StepperLine: "#C5D0EB",

    RatingO: "#66769C",

    statusThree: "#FFF7EE",
    statusFour: "#F0F2F4",
    statusThreeText: "#4A423A",
    statusTwo: "#01CFDC",

    brown: "#47060B",
    title_active: '#261548',
    content_text: "#66769C",
    line_tab: "#E8EFFF",
    Input: '#000000',
    white: '#FFFFFF',
    black: '#000000',

    input_background: "#F4F8FF",

    login_title: "#959DB3",

    bottomLineColor_1: "#C1BCF1",
    bottomLineColor_2: "#DF9194",
    bottomLineColor_3: "#F6DA98",
    bottomLineColor_4: "#ADE5F4",
}
