import React from "react";

export const Logo = require("./images/LogoDikey.png")
export const LogoW = require("./images/LogoDikeyW.png")
export const Line = require("./images/Line.png")
export const LineDizayn = require("./images/LineDizayn.png")
export const LogoHorizontal = require("./images/LogoYatay.png")
export const LogoHorizontalW = require("./images/LogoYatayW.png")

export const welcomeImage = require("./images/img.png")

export const picOne = require("./images/picOne.png")
export const picTwo = require("./images/picTwo.png")
export const picThree = require("./images/picThree.png")
export const picFour = require("./images/picFour.png")
export const picFive = require("./images/picFive.png")

export const threeDModelTshirt = require("./images/GhostProduct.png")
export const threeDModelFabric = require("./images/Kumas.png")

export const profileImage = require("./images/M.png")

export const tourOne = require("./images/tour1.png")
export const tourTwo = require("./images/tour2.png")
export const tourThree = require("./images/tour3.png")

export const patternImage = require("./images/patternImage.png")
export const zone = require("./images/zone.png")
export const modelMan = require("./images/modelErkek.png")

export const track = require("./images/track.png")
export const card = require("./images/card.png")

export const star = require("./images/star.png")

export const statusOne = require("./images/statusOne.png")
export const statusTwo = require("./images/statusTwo.png")
export const statusThree = require("./images/statusThree.png")
export const statusFour = require("./images/statusFour.png")

export const accountProfile = require("./images/accountProfile.png")
export const addressCargo = require("./images/addressCargo.png")
export const change = require("./images/change.png")
export const notification = require("./images/notification.png")
export const orderPay = require("./images/OrderPay.png")
export const productAndDesign = require("./images/prodectAndDesign.png")
export const sales = require("./images/sales.png")
export const security = require("./images/security.png")
export const settings = require("./images/settings.png")

export const notificationImage = require("./images/bildirim.png")

export const toogleClose = require("./images/tooglekapali.png")
export const toogleOpen = require("./images/toogleOpen.png")

export const colorSlider = require("./images/Slider.png")
export const colorPicker = require("./images/Picker.png")

export const bg_bottomRight = require("./images/bgline_bottomRight.png")
export const bg_bottomLeft = require("./images/bgline_bottomLeft.png")
export const bg_topRight = require("./images/bgline_topRight.png")
export const bg_topLeft = require("./images/bgline_topLeft.png")

export const HDImageOne = require("./images/HDImageOne.png")
export const HDImageTwo = require("./images/HDImageTwo.png")
export const HDImageThree = require("./images/HDImageThree.png")
export const HDImageFour = require("./images/HDImageFour.png")

export const homePageImage = require("./images/homePageImage.png")

export const EmptyAddress = require("./images/EmptyAddress.png")
export const EmptyCard = require("./images/EmptyCard.png")
export const EmptyMyOrders = require("./images/EmptyMyOrders.png")
export const EmptyNotification = require("./images/EmptyNotification.png")
export const EmptyCoupon = require("./images/EmptyCoupon.png")
