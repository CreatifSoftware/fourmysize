import {getLocalizedString} from "../utils/localization";
import {
    LOCALIZED_DESIGN_ITEMS_COLOR, LOCALIZED_DESIGN_ITEMS_EMBROIDERY, LOCALIZED_DESIGN_ITEMS_NICK,
    LOCALIZED_DESIGN_ITEMS_PATTERN, LOCALIZED_DESIGN_ITEMS_PRINTING,
    LOCALIZED_DESIGN_ITEMS_SIZE, LOCALIZED_DESIGN_ITEMS_TEXT
} from "../utils/constants";
import {
    iconBaski,
    iconBeden,
    iconCargo,
    iconDesen,
    iconInvoice,
    iconNakis,
    iconRenk,
    iconRumuz, iconStock,
    iconText, iconWasDelivered
} from "../assets/Icon";
import Color from "../views/design/Color";
import {Colors} from "../styles/Colors";
import {
    accountProfile,
    addressCargo,
    card,
    change,
    notification,
    orderPay,
    patternImage, productAndDesign,
    security, settings, sales,
    zone, threeDModelTshirt
} from "../assets/Images";
import uuid from "react-native-uuid";

export const language = [
    {
        key: 'welcome_title',
        valueTR: 'Hayallerini Tasarladığın Dijital Atölyen \'a Hoş Geldin!',
    },
    {
        key: 'welcome_title_name',
        valueTR: '4MySize',
    },
    {
        key: 'welcome_btn_individual',
        valueTR: 'Bireysel',
    },
    {
        key: 'welcome_btn_corporate',
        valueTR: 'Kurumsal',
    },
    {
        key: 'welcome_btn_go',
        valueTR: 'Üye Olmadan Devam Et',
    },
    ///LOGİN
    {
        key: 'login_title',
        valueTR: 'Tarzına Kendini Katmak İçin',
    },
    {
        key: 'login_sub_title',
        valueTR: 'Giriş Yap!',
    },
    {
        key: 'login_email',
        valueTR: 'E-Posta',
    },
    {
        key: 'login_password',
        valueTR: 'Parolanız',
    },
    {
        key: 'login_password_repeate',
        valueTR: 'Parola Tekrar',
    },
    {
        key: 'login_button_go',
        valueTR: 'Hadi Başlayalım',
    },
    {
        key: 'login_text_register',
        valueTR: 'Bir hesabın yok mu?',
    },
    {
        key: 'login_button_register',
        valueTR: 'Hemen üye ol!',
    },
    {
        key: 'login_text_social',
        valueTR: 'Ya da sosyal medya hesaplarınla bağlan',
    },
    {
        key: 'register_title',
        valueTR: 'Tarzına Kendini Katmak İçin',
    },
    {
        key: 'register_sub_title',
        valueTR: 'Üye Ol!',
    },
    {
        key: 'register_username',
        valueTR: 'İsim-Soyisim',
    },
    {
        key: 'register_email',
        valueTR: 'E-Posta',
    },
    {
        key: 'register_phone',
        valueTR: 'Telefon Numarası',
    },
    {
        key: 'register_password',
        valueTR: 'Parolanız',
    },
    {
        key: 'register_contract',
        valueTR: 'Sağladığım kişisel verilerin 4MySize tarafından Gizlilik ve Kişisel Verilerin Korunması Politikası\'nda belirtilen amaçlar doğrultusunda işlenmesine onay verir, Üyelik Sözleşmesi\'ni kabul ederim.',
    },
    {
        key: 'register_button_go',
        valueTR: '4MySize Dünyasına Katıl',
    },
    {
        key: 'register_text_register',
        valueTR: 'Bir hesabın var mı?',
    },
    {
        key: 'register_button_register',
        valueTR: 'Giriş Yap!',
    },
    {
        key: 'forgot_title',
        valueTR: 'Parolanı mı Unuttun?',
    },
    {
        key: 'forgot_button_go',
        valueTR: 'Doğrulama Kodu Gönder',
    },
    {
        key: 'myownpageone_title',
        valueTR: 'Kimin Bu Tarz?',
    },
    {
        key: 'myownpageone_sub_title',
        valueTR: 'Vücuduna en uygun kalıbı bulmak için cinsiyetini seçebilir, çocuklara yönelik kalıplar için çocuk seçeneğiyle ilerleyebilirsin.',
    },
    {
        key: 'myownpageone_button_go',
        valueTR: 'Kimin Bu Tarz',
    },
    {
        key: 'myownpagetwo_title',
        valueTR: 'Nasıl Bi’ Tarz?',
    },
    {
        key: 'myownpagetwo_sub_title',
        valueTR: 'Tarzını uygulayacağın kıyafet tipini buradan seçebilirsin.',
    },
    {
        key: 'myownpagetwo_button_go',
        valueTR: 'Nasıl Bi’ Kumaş',
    },
    {
        key: 'myownpagetwo_button_back',
        valueTR: 'Kimin Bu Tarz',
    },
    {
        key: 'myownpagethree_title',
        valueTR: 'Organik Pamuk',
    },
    {
        key: 'myownpagethree_sub_title',
        valueTR: 'Organik pamuk kumaş, herhangi bir böcek ilacı ve kimyasal madde kullanılmadan üretilen pamuktan elde edilen bir kumaş türüdür.',
    },
    {
        key: 'myownpagethree_button_go',
        valueTR: 'Tasarlamaya Başla',
    },
    {
        key: 'myownpagethree_button_back',
        valueTR: 'Nasıl Bi’Tarz',
    },

    {
        key: 'homepage_pic_1_text',
        valueTR: 'Önce Modelin',
    },
    {
        key: 'homepage_pic_2_text',
        valueTR: 'Hayalindeki Tasarımın',
    },
    {
        key: 'homepage_pic_3_text',
        valueTR: 'Sonra Kumaşın',
    },
    {
        key: 'homepage_pic_4_text',
        valueTR: 'Evine Gelsin 4/4\'lük Tarzın!',
    },
    {
        key: 'homepage_title',
        valueTR: '4/4’lük Tarzına',
    },
    {
        key: 'homepage_sub_title',
        valueTR: 'Kendini Kat!',
    },
    {
        key: 'homepage_button_1',
        valueTR: 'Kendi Tarzımı Yaratmak İstiyorum',
    },
    {
        key: 'homepage_button_2',
        valueTR: 'Özel Tasarımlara Göz Atmak İstiyorum',
    },

    {
        key: 'design_items_color',
        valueTR: 'Renk',
    },
    {
        key: 'design_items_size',
        valueTR: 'Beden',
    },
    {
        key: 'design_items_embroidery',
        valueTR: 'Nakış',
    },
    {
        key: 'design_items_pattern',
        valueTR: 'Desen Ekle',
    },
    {
        key: 'design_items_text',
        valueTR: 'Yazı Ekle',
    },
    {
        key: 'design_items_nick',
        valueTR: 'Rumuz & Logo',
    },
    {
        key: 'design_items_printing',
        valueTR: 'Baskı Türü',
    },
    {
        key: 'orderstatus_product_name',
        valueTR: 'Ürün Adı',
    },
    {
        key: 'orderstatus_colorsize_name',
        valueTR: 'Renk | Beden',
    },
    {
        key: 'orderstatus_paytype_name',
        valueTR: 'Ödeme Türü',
    },
    {
        key: 'orderstatus_totalprice_name',
        valueTR: 'Toplam Tutar',
    },
    {
        key: 'orderstatus_cargo_name',
        valueTR: 'Kargo Adı',
    },
    {
        key: 'orderstatus_cargodate_name',
        valueTR: 'Kargo Tarihi',
    },
    {
        key: 'orderstatus_cargodelivery_name',
        valueTR: 'Tahmini Teslim',
    },
    {
        key: 'orderstatus_cargotraking_name',
        valueTR: 'Kargo Takip No',
    },
    {
        key: 'status_one_title',
        valueTR: 'Siparişini Aldık!',
    },
    {
        key: 'status_one_subtitle',
        valueTR: '4/4\'lük tarzına kendini kattığın tasarımı hazırlamak için sabırsızlanıyoruz!\n' +
            'Tasarımın kargoya verildiğinde seni bilgilendireceğiz.',
    },
    {
        key: 'status_one_btn1_text',
        valueTR: 'Siparişlerim',
    },
    {
        key: 'status_one_btn2_text',
        valueTR: 'Tasarlamaya Devam Et',
    },
    {
        key: 'status_two_title',
        valueTR: 'Bir Şeyler Ters Gitti!',
    },
    {
        key: 'status_two_subtitle',
        valueTR: 'Ama endişelenme! ' +
            'Bilgilerini yanlış ya da eksik girmiş olabilirsin. Sipariş vermeyi tekrar deneyebilir ya da 4MySize hesabına dönerek eksik bilgilerini kontrol edebilirsin.',
    },
    {
        key: 'status_two_btn1_text',
        valueTR: 'Tekrar Dene',
    },
    {
        key: 'status_two_btn2_text',
        valueTR: '4MySize Hesabım',
    },
    {
        key: 'status_three_title',
        valueTR: 'İnternet ' +
            'Bağlantın Çok Yavaş!',
    },
    {
        key: 'status_three_subtitle',
        valueTR: 'Biz de senin gibi tasarımının bir sonraki adımına geçmeyi çok istiyoruz ama maalesef şartlar izin vermiyor ☹\n' +
            'İnternet bağlantını kontrol ederek ' +
            'yeniden denemeye ne dersin?',
    },
    {
        key: 'status_three_btn1_text',
        valueTR: 'Tekrar Dene',
    },
    {
        key: 'status_four_title',
        valueTR: 'Bir Hata Oluştu!',
    },
    {
        key: 'status_four_subtitle',
        valueTR: 'Tam da en heyecanlı yerinde! Sen gerçekleştirdiğin son adımı yeniden denerken biz de sorunu çözmek için elimizden geleni yapacağız.\n',
    },
    {
        key: 'status_four_btn1_text',
        valueTR: 'Tekrar Dene',
    },
    {
        key: 'howdesign_one_title',
        valueTR: 'Önce Modelin',
    },
    {
        key: 'howdesign_one_subtitle',
        valueTR: '4/4’lük tarzına kendini katmaya, kıyafeti seçerek başla!' +
            'Cinsiyetini ve tasarlamak istediğin kıyafet türünü seçerek bir sonraki adıma geçebilirsin.',
    },
    {
        key: 'howdesign_two_title',
        valueTR: 'Sonra Kumaşın',
    },
    {
        key: 'howdesign_two_subtitle',
        valueTR: 'Sıra geldi vücuduna' +
            'en uygun kumaşı bulmaya!' +
            'Farklı kumaş türleri arasından tarzına ve kullanımına en uygun olanını seçerek kıyafetini tasarlamaya başlayabilirsin.',
    },
    {
        key: 'howdesign_three_title',
        valueTR: 'Hayalindeki Tasarımın',
    },
    {
        key: 'howdesign_three_subtitle',
        valueTR: 'Bedeninden favori rengine, en sevdiğin yaka tipinden baskı içeriğine kadar kıyafetindeki her öğeyi sen tasarla!' +
            'Pratik bir arayüzle sahip' +
            'tasarım menümüzden seçimlerini' +
            'yapmaya başlayabilirsin!',
    },
    {
        key: 'howdesign_four_title',
        valueTR: 'Evine Gelsin 4 /4’lük Tarzın!',
    },
    {
        key: 'howdesign_four_subtitle',
        valueTR: 'Sen seçimini yaptın, sıra bizde! Sipariş ettiğin anda hazırlamaya başladığımız tasarımını en kısa sürede tamamlar' +
            've sana kargolarız.' +
            'Böylece günler içerisinde 4/4’lük tarzının tadını çıkarmaya başlayabilirsin!',
    },

]

export const dataOrder = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        stockStatus: true,
        title: 'First Item',
        color: "Beyaz",
        size: "M",
        count: 1,
        unitPrice: 65,
        price: "65"
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        stockStatus: true,
        title: 'First Item',
        color: "Beyaz",
        size: "M",
        count: 1,
        unitPrice: 65,
        price: "65 TL"
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        stockStatus: true,
        title: 'First Item',
        color: "Beyaz",
        size: "M",
        count: 1,
        unitPrice: 65,
        price: "65 TL"
    },
];

export const dataSize = [
    {
        id: "1",
        size: "S"
    },
    {
        id: "2",
        size: "M"
    },
    {
        id: "3",
        size: "L"
    },
    {
        id: "4",
        size: "XL"
    },
    {
        id: "5",
        size: "2XL"
    },
    {
        id: "6",
        size: "3XL"
    },
]


export const dataStyle = [
    {
        id: "1",
        size: "Regular"
    },
    {
        id: "2",
        size: "Fit"
    },
    {
        id: "3",
        size: "Oversize"
    },
    {
        id: "4",
        size: "Normal"
    },
    {
        id: "5",
        size: "Bold"
    },
]

export const dataEmbroidery = [
    {
        id: "1",
        title: "Nakış Türü - 1"
    },
    {
        id: "2",
        title: "Nakış Türü - 2"
    },
    {
        id: "3",
        title: "Nakış Türü - 3"
    },
    {
        id: "4",
        title: "Nakış Türü - 4"
    },
    {
        id: "5",
        title: "Nakış Türü - 5"
    },
]

export const dataPrinting = [
    {
        id: "1",
        title: "Emprime Baskı"
    },
    {
        id: "2",
        title: "Transfer Baskı"
    },
    {
        id: "3",
        title: "Sublime Baskı"
    },

]


export const dataColor = [
    {
        id: "1",
        color: Colors.Secondary_1
    },
    {
        id: "2",
        color: Colors.supplement_1
    },
    {
        id: "3",
        color: Colors.supplement_2
    },
    {
        id: "4",
        color: Colors.supplement_3
    },
    {
        id: "5",
        color: Colors.white
    },
    {
        id: "6",
        color: Colors.Success
    },
    {
        id: "7",
        color: Colors.line_tab
    },
    {
        id: "8",
        color: Colors.PlaceHolder
    },
    {
        id: "9",
        color: Colors.content_text
    },
    {
        id: "10",
        color: Colors.bottomLineColor_4
    },
    {
        id: "11",
        color: Colors.background
    },
    {
        id: "12",
        color: Colors.Secondary_1
    },
    {
        id: "13",
        color: Colors.content_text
    },
    {
        id: "14",
        color: Colors.line_tab
    },
    {
        id: "15",
        color: Colors.title_active
    },
    {
        id: "16",
        color: Colors.login_title
    },
    {
        id: "17",
        color: Colors.Secondary_1
    },
    {
        id: "18",
        color: Colors.Secondary_1
    },
    {
        id: "19",
        color: Colors.Secondary_1
    },
    {
        id: "20",
        color: Colors.Secondary_1
    },
    {
        id: "21",
        color: Colors.Secondary_1
    },
    {
        id: "22",
        color: Colors.Secondary_1
    },
    {
        id: "23",
        color: Colors.Secondary_1
    },
    {
        id: "24",
        color: Colors.Secondary_1
    },
    {
        id: "25",
        color: Colors.Secondary_1
    },
]


export const dataPattern = [
    [
        {
            id: "1",
            title: "Desen No:1",
            image: patternImage
        },
        {
            id: "2",
            title: "Desen No:2",
            image: patternImage
        },
        {
            id: "3",
            title: "Desen No:3",
            image: patternImage
        },
        {
            id: "4",
            title: "Desen No:4",
            image: patternImage
        },
        {
            id: "5",
            title: "Desen No:5",
            image: patternImage
        }],
    [
        {
            id: "6",
            title: "Desen No:6",
            image: patternImage
        },
        {
            id: "7",
            title: "Desen No:7",
            image: patternImage
        },
        {
            id: "8",
            title: "Desen No:8",
            image: patternImage
        },
        {
            id: "9",
            title: "Desen No:9",
            image: patternImage
        },
        {
            id: "10",
            title: "Desen No:10",
            image: patternImage
        }]

]

export const dataAddZone = [

    {
        id: "1",
        title: "1.Bölge",
        subtitle: "(Ön Kısım)",
        image: zone
    },
    {
        id: "2",
        title: "1.Bölge",
        subtitle: "(Ön Kısım)",
        image: zone
    },
    {
        id: "3",
        title: "1.Bölge",
        subtitle: "(Ön Kısım)",
        image: zone
    },
    {
        id: "4",
        title: "1.Bölge",
        subtitle: "(Ön Kısım)",
        image: zone
    },
    {
        id: "5",
        title: "1.Bölge",
        subtitle: "(Ön Kısım)",
        image: zone
    }
]

export const dataNick = [
    {
        id: "asda",
        title: "mert"
    },
    {
        id: "2",
        title: "blacksea"
    },
    {
        id: "3",
        title: "jmt"
    },

]

export const dataAdress = [
    {
        id: "0",
        title: "EV",
        username: "mert karadeniz",
        adress: "adres 1",
        city: "İstanbul",
        district: "Kadıköy",
        neighborhood: "Yeni Cuma",
    },
    {
        id: "1",
        title: "İŞ",
        username: "mert karadeniz",
        adress: "adres 2",
        city: "Trabzon",
        district: "Merkez",
        neighborhood: "Pelitli",
    },
    {
        id: "2",
        title: "EV",
        username: "mert karadeniz",
        adress: "Acıbadem Mah. Kekevi Sok. No:16 Daire:3 Üsküdar / İstanbul",
        city: "Trabzon",
        district: "Merkez",
        neighborhood: "Pelitli",
    },

]

export const dataCrediaCard = [
    {
        id: "1",
        title: "YapıKredi",
        cardnumber: "**** **** **** 3453",
        cardtype: card,

    },
    {
        id: "2",
        title: "Garanti",
        cardnumber: "**** **** **** 1231",
        cardtype: card,

    },
    {
        id: "3",
        title: "Ziraat",
        cardnumber: "**** **** **** 3232",
        cardtype: card,

    },

]


export const dataTableSize = [
    [
        {
            id: "1",
            text: "Beden"
        },
        {
            id: "2",
            text: "Genişlik (cm)"
        },
        {
            id: "3",
            text: "Yükseklik (cm)"
        },
        {
            id: "4",
            text: "Etek Ucu (cm)"
        },
        {
            id: "5",
            text: "Kol Boyu (cm)"
        },
    ],
    [
        {
            id: "6",
            text: "XXS"
        },
        {
            id: "7",
            text: "3"
        },
        {
            id: "8",
            text: "5"
        },
        {
            id: "9",
            text: "12"
        },
        {
            id: "10",
            text: "43"
        },
    ],
    [
        {
            id: "11",
            text: "XXX"
        },
        {
            id: "12",
            text: "23"
        },
        {
            id: "13",
            text: "45"
        },
        {
            id: "14",
            text: "56"
        },
        {
            id: "15",
            text: "67"
        },
    ],
    [
        {
            id: "16",
            text: "M"
        },
        {
            id: "17",
            text: "23"
        },
        {
            id: "18",
            text: "45"
        },
        {
            id: "19",
            text: "56"
        },
        {
            id: "20",
            text: "67"
        },
    ],
    [
        {
            id: "21",
            text: "L"
        },
        {
            id: "22",
            text: "23"
        },
        {
            id: "23",
            text: "45"
        },
        {
            id: "24",
            text: "56"
        },
        {
            id: "25",
            text: "67"
        },
    ],
    [
        {
            id: "26",
            text: "XL"
        },
        {
            id: "27",
            text: "23"
        },
        {
            id: "28",
            text: "45"
        },
        {
            id: "29",
            text: "56"
        },
        {
            id: "30",
            text: "67"
        },
    ]
]

export const dataOrderStatus = [
    [
        {
            id: "0",
            productStatus: 0,
            productFinish: false,
            productData: {
                id: "1",
                productName: "asd",
                ColorSize: "Blue | M",
                PayType: "Kredi Kartı / Tek Çekim",
                TotalPrice: "65 TL",
            }
            ,
            cargoData: {
                id: "1",
                cargoCompany: "MNG",
                cargoDate: "03.02.2021 | 14:50",
                deliveryDate: "05.02.2021",
                trackingNumber: "232323234",
            }
        },
    ],
    [
        {
            id: "1",
            productStatus: 2,
            productFinish: false,
            productData: {
                id: "1",
                productName: "asd",
                ColorSize: "Blue | M",
                PayType: "Kredi Kartı / Tek Çekim",
                TotalPrice: "65 TL",
            }
            ,
            cargoData: {
                id: "1",
                cargoCompany: "MNG",
                cargoDate: "03.02.2021 | 14:50",
                deliveryDate: "05.02.2021",
                trackingNumber: "232323234",
            }
        },
    ],
    [
        {
            id: "2",
            productStatus: 3,
            productFinish: false,
            productData: {
                id: "1",
                productName: "asd",
                ColorSize: "Blue | M",
                PayType: "Kredi Kartı / Tek Çekim",
                TotalPrice: "65 TL",
            }
            ,
            cargoData: {
                id: "1",
                cargoCompany: "MNG",
                cargoDate: "03.02.2021 | 14:50",
                deliveryDate: "05.02.2021",
                trackingNumber: "232323234",
            }
        },
    ]

]

export const dataMyOrders = [
    {
        id: "0",
        orderNumber: "123123123",
        orderDate: "02.02.2021",
        orderStatus: 0,
    },
    {
        id: "1",
        orderNumber: "123123123",
        orderDate: "02.02.2021",
        orderStatus: 2,
    },
    {
        id: "2",
        orderNumber: "123123123",
        orderDate: "02.02.2021",
        orderStatus: 3,
    },
]

export const dataMyCoupon = [
    {
        id: "0",
        title: "İlk Alışverişine Özel",
        subtitle: "150 TL ve üzeri alışverişlerde geçerli",
        coupon: "%10 İNDİRİM",
        dateTitle: "Bitiş Tarihi: 8 Haziran 2021",
        colorLight: "#D5C0FF",
        colorDark: "#5523BD",
    },
    {
        id: "1",
        title: "10. Siparişine Özel",
        subtitle: "10. sipariş sonrası tüm ürünlerde geçerli",
        coupon: "%5 İNDİRİM",
        dateTitle: "Bitiş Tarihi: 9 Haziran 2021",
        colorLight: "#CCF4FF",
        colorDark: "#23A3C7",
    },
    {
        id: "2",
        title: "Tişörtlerde Geçerli",
        subtitle: "Sadece tişört grubunda geçerlidir.",
        coupon: "%5 İNDİRİM",
        dateTitle: "Bitiş Tarihi: 10 Mayıs 2021",
        colorLight: "#FFC8BC",
        colorDark: "#E9310B",
    },
]

const dataDesignParams = {


}


export const dataNotification = [
    {
        id: "0",
        title: "Ücret iadesi tanımlı kartına aktarılmıştır.",
        content: "08.04.2021 tarihinde oluşturduğun 20201120 numaralı siparişin 08.04.2021 tarihinde kargoya verilmiştir. Kargo takip bilgilerine sipariş detayından erişebilirsin.",
        icon: iconInvoice,

    },
    {
        id: "1",
        title: "20111 numaralı siparişin kargoya verilmiştir.",
        content: "08.04.2021 tarihinde oluşturduğun 20201120 numaralı siparişin 08.04.2021 tarihinde kargoya verilmiştir. Kargo takip bilgilerine sipariş detayından erişebilirsin.",
        icon: iconCargo,
    },
    {
        id: "2",
        title: "20110 numaralı siparişin teslim edilmiştir.",
        content: "08.04.2021 tarihinde oluşturduğun 20201120 numaralı siparişin 08.04.2021 tarihinde kargoya verilmiştir. Kargo takip bilgilerine sipariş detayından erişebilirsin.",
        icon: iconStock,
    },
    {
        id: "3",
        title: "20109 numaralı siparişin teslim edilmiştir.",
        content: "08.04.2021 tarihinde oluşturduğun 20201120 numaralı siparişin 08.04.2021 tarihinde kargoya verilmiştir. Kargo takip bilgilerine sipariş detayından erişebilirsin.",
        icon: iconWasDelivered,
    },
]

export const dataCity = [
    {
        id: "0",
        label: "İstanbul",
        value: "İstanbul",
    },
    {
        id: "1",
        label: "Ankara",
        value: "Ankara",

    },
    {
        id: "2",
        label: "İzmir",
        value: "İzmir",
    },
]

export const dataDistrict = [
    {
        id: "0",
        label: "Kadıköy",
        value: "Kadıköy",
    },
    {
        id: "1",
        label: "Beşiktaş",
        value: "Beşiktaş",
    },
    {
        id: "2",
        label: "Nişantaşı",
        value: "Nişantaşı",
    },
]


export const dataNeighborhood = [
    {
        id: "0",
        label: "Kirazlı",
        value: "Kirazlı",
    },
    {
        id: "1",
        label: "Yeni Cuma",
        value: "Yeni Cuma",
    },
    {
        id: "2",
        label: "Manolya",
        value: "Manolya",
    },
]


export const dataSSS = [
    {
        id: "0",
        icon: accountProfile,
        title: "Üyelik & Profil",
    },
    {
        id: "1",
        icon: security,
        title: "Güvenlik",
    },
    {
        id: "3",
        icon: orderPay,
        title: "Ödeme & Sipariş",
    },
    {
        id: "4",
        icon: notification,
        title: "Bildirim & Bülten",
    },
    {
        id: "5",
        icon: change,
        title: "İade & Değişim",
    },
    {
        id: "6",
        icon: addressCargo,
        title: "Kargo & Adres",
    },
    {
        id: "7",
        icon: sales,
        title: "Satış & Pazarlama",
    },
    {
        id: "8",
        icon: productAndDesign,
        title: "Ürün & Tasarımlar",
    },
    {
        id: "9",
        icon: settings,
        title: "Ayarlar",
    },
]

export const dataSSSProfile = [
    {
        id: "0",
        title: "Ödemeyi Nasıl Yapabilirim?",
        content: "Ödemeyi Nasıl Yapabilirim?",
    },
    {
        id: "1",
        title: "Kredi kartına taksit yapıyor musunuz?",
        content: "Kredi kartına taksit yapıyor musunuz?",
    },
    {
        id: "2",
        title: "Ödeme güvenliğini nasıl sağlanıyor?",
        content: "Ödeme güvenliğini nasıl sağlanıyor?",
    },
    {
        id: "3",
        title: "Siparişimi ne zaman iptal edebilirim?",
        content: "Siparişini kargoya vermediğimiz sürece istediğin zaman iptal edebilirsin. İptal etmek için sipariş takip ekranından siparişini bulup iptal et'e tıklaman yeterli. Eğer, siparişin kargoya verildikten sonra siparişini iptal etmek istersen, iptal sebebin ve sipariş numaran ile birlikte info@4mysize.com adresine mail atman yeterli olacak.",
    },
    {
        id: "4",
        title: "Sipariş verebilmek için üyelik şart mı?",
        content: "Sipariş verebilmek için üyelik şart mı?",
    },
]
