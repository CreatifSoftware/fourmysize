import axios from "axios"
import {API_HEADERS, API_URL} from "../contants"
import {
    createToken,
    refreshToken,
    register,
    loginWithEmail,
    forgotOtpCode,
    forgotOtpCodeConfirm,
    registerOtpCode,
    registerOtpCodeConfirm,
    resetPassword
} from "../auth/authRequest"
import * as P from "../endpoints";
import {isSuccess, isUnauthorized, tokenControl} from "../helpers";

export const callCreateToken = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.CREATE_TOKEN_ENDPOINT,
            createToken(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callRefreshToken = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.REFRESH_TOKEN_ENDPOINT,
            refreshToken(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callLoginWithEmail = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.LOGIN_WITH_EMAIL_ENDPOINT,
            loginWithEmail(params),
            {headers: API_HEADERS}
        ).then(response => {
            tokenControl(response, callLoginWithEmail, params).then(r => resolve(r))
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callForgotOptCode = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.FORGOT_OPT_CODE_ENDPOINT,
            forgotOtpCode(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callForgotOptCodeConfirm = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.FORGOT_OPT_CODE_CONFIRM_ENDPOINT,
            forgotOtpCodeConfirm(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callRegisterOptCode = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.REGISTER_OPT_CODE_ENDPOINT,
            registerOtpCode(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callRegisterOptCodeConfirm = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.REGISTER_OPT_CODE_CONFIRM_ENDPOINT,
            registerOtpCodeConfirm(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callResetPassword = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.RESET_PASSWORD_ENDPOINT,
            resetPassword(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}

export const callRegister = (params) => {
    return new Promise((resolve, reject) => {
        axios.post(
            API_URL + P.REGISTER_ENDPOINT,
            register(params),
            {headers: API_HEADERS}
        ).then(response => {
            resolve(response)
        })
            .catch(error => {
                reject(error)
            })
    })
}


