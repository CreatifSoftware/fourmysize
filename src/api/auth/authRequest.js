export const createToken = (params) => {
    const jsonBody = {
        "customerId": params.customerId,
    }

    return jsonBody
}

export const refreshToken = (params) => {
    const jsonBody = {
        "customerId": params.customerId,
        "refreshToken": params.refreshToken,
    }

    return jsonBody
}

export const loginWithEmail = (params) => {
    const jsonBody = {
        "email": params.email,
        "password": params.password,
    }

    return jsonBody
}

export const forgotOtpCode = (params) => {
    const jsonBody = {
        "phone": params.phone,
    }

    return jsonBody
}

export const registerOtpCode = (params) => {
    const jsonBody = {
        "phone": params.phone,
        "languageId": params.languageId
    }

    return jsonBody
}

export const forgotOtpCodeConfirm = (params) => {
    const jsonBody = {
        "phone": params.phone,
        "otpCode": params.otpCode,
    }

    return jsonBody
}

export const registerOtpCodeConfirm = (params) => {
    const jsonBody = {
        "phone": params.phone,
        "otpCode": params.otpCode,
    }

    return jsonBody
}

export const resetPassword = (params) => {
    const jsonBody = {
        "token": params.token,
        "newPassword": params.newPassword,
    }

    return jsonBody
}

export const register = (params) => {

    const jsonBody = {
        customerId: params.customerId,
        password: params.password,
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        phone: params.phone,
        languageId: params.languageId,
        registrationSource: params.registrationSource,
        token: params.token,
    }

    return jsonBody
}

export const loginWithFacebook = (params) => {
    const jsonBody = {
        "facebookId": params.facebookId,
    }

    return jsonBody
}

export const loginWithTwitter = (params) => {
    const jsonBody = {
        "twitterId": params.twitterId,
    }

    return jsonBody
}

export const loginWithGoogle = (params) => {
    const jsonBody = {
        "googleId": params.googleId,
    }

    return jsonBody
}
