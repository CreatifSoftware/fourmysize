import {Platform} from "react-native"

export const APP_VERSION = "1.0"
export const LANG_ID = "T"

export const API_URL = "https://identity.dummydev.net/api/"
export const API_HEADERS = {
    'languageId': 1,
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}
