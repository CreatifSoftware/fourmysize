import {LANG_ID} from "../constants"
import {callRefreshToken} from "./auth/authServices";
import {useSelector} from "react-redux";


export const isSuccess = (response) => {
    return response.data.status === 100
}

export const getErrorMessage = (response) => {
    switch (response.data.message) {
        case "Command.Customer.NotFound" :
            return "Bu mail adresi bulunamadı"
        case "Command.Customer.InvalidCredentials" :
            return "Şifreniz geçersizdir"
        default : return (response.data.message)
    }
}

export const isUnauthorized = (response) => {
    return response.data.status === 201
}

export const isAnotherDeviceLogin = (response) => {
    return response.data.status === 202
}


export const tokenControl = (response, callFunction, params) => {
    return new Promise((resolve, reject) => {

        if (isUnauthorized(response)) {
            callRefreshToken(params)
                .then(response => {

                    if (isSuccess(response)) {
                        console.log("callRefreshToken", response)

                        resolve(response)
                        callFunction(params)
                    }
                })
                .catch(error => {
                    resolve(response)
                    console.log(error + "Error")
                })
        } else
            resolve(response)
    })
}
