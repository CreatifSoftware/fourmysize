export const CREATE_TOKEN_ENDPOINT = "account/createtoken"
export const REFRESH_TOKEN_ENDPOINT = "account/refreshtoken"

export const LOGIN_WITH_EMAIL_ENDPOINT = "account/loginwithemail"

export const REGISTER_ENDPOINT = "account/register"
export const REGISTER_OPT_CODE_ENDPOINT = "account/phoneverification"
export const REGISTER_OPT_CODE_CONFIRM_ENDPOINT = "account/confirmphoneverificationotpcode"

export const FORGOT_OPT_CODE_ENDPOINT = "account/forgotpassword"
export const FORGOT_OPT_CODE_CONFIRM_ENDPOINT = "account/confirmresetpasswordotpcode"
export const RESET_PASSWORD_ENDPOINT = "account/resetpassword"



