import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import ViewPager from '@react-native-community/viewpager';
import {
    bg_topRight, bg_bottomLeft, bg_bottomRight, bg_topLeft, HDImageFour, HDImageOne, HDImageThree, HDImageTwo
} from "../../assets/Images";
import {getLocalizedString} from "../../utils/localization";
import {
    LOCALIZED_HOWDESIGN_FOUR_SUBTITLE,
    LOCALIZED_HOWDESIGN_FOUR_TITLE,
    LOCALIZED_HOWDESIGN_ONE_SUBTITLE,
    LOCALIZED_HOWDESIGN_ONE_TITLE,
    LOCALIZED_HOWDESIGN_THREE_SUBTITLE,
    LOCALIZED_HOWDESIGN_THREE_TITLE,
    LOCALIZED_HOWDESIGN_TWO_SUBTITLE,
    LOCALIZED_HOWDESIGN_TWO_TITLE,
} from "../../utils/constants";
import AntDesign from "react-native-vector-icons/AntDesign";

const HowDesign = (props) => {

    const [page, setPage] = React.useState(0)

    React.useEffect(()=>{
        this.viewPager.setPage(page);
    },[page])

    return (
        <View style={styles.container}>
            <ViewPager
                ref={(viewPager) => {this.viewPager = viewPager}}
                style={styles.viewPager}
                       initialPage={page}
                       showPageIndicator={true}
                       pageIndicatorColor={Colors.Primary}
                       onPageScrollStateChanged={event => (event)}>

                <View style={styles.view} key="1">
                    <Image style={styles.background} source={bg_topRight}/>
                    <Image style={styles.image} source={HDImageOne}/>
                    <View style={[styles.space, {left: 0}]}/>

                    <Text style={styles.title}>{getLocalizedString(LOCALIZED_HOWDESIGN_ONE_TITLE)}</Text>
                    <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_HOWDESIGN_ONE_SUBTITLE)}</Text>

                    <View style={styles.arrows}>
                        <TouchableOpacity style={styles.arrowright} onPress={() => setPage(page + 1)}>
                            <Text style={styles.next}>Sonraki</Text>
                            <AntDesign name={"arrowright"} size={hp(15)} color={Colors.Primary}/>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.view} key="2">
                    <Image style={styles.background} source={bg_topLeft}/>
                    <Image style={styles.image} source={HDImageTwo}/>
                    <View style={[styles.space, {right: 0}]}/>

                    <Text
                        style={[styles.title, {color: Colors.Primary}]}>{getLocalizedString(LOCALIZED_HOWDESIGN_TWO_TITLE)}</Text>
                    <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_HOWDESIGN_TWO_SUBTITLE)}</Text>


                    <View style={styles.arrows}>
                        <TouchableOpacity style={styles.arrowleft} onPress={() => setPage(page -1)}>
                            <AntDesign name={"arrowleft"} size={hp(15)} color={Colors.Primary}/>
                            <Text style={styles.next}>Önceki</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.arrowright} onPress={() => setPage(page + 1)}>
                            <Text style={styles.next}>Sonraki</Text>
                            <AntDesign name={"arrowright"} size={hp(15)} color={Colors.Primary}/>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={styles.view} key="3">
                    <Image style={styles.background} source={bg_topLeft}/>
                    <Image style={styles.image} source={HDImageThree}/>
                    <View style={[styles.space, {left: 0}]}/>

                    <Text style={styles.title}>{getLocalizedString(LOCALIZED_HOWDESIGN_THREE_TITLE)}</Text>
                    <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_HOWDESIGN_THREE_SUBTITLE)}</Text>

                    <View style={styles.arrows}>
                        <TouchableOpacity style={styles.arrowleft} onPress={() => setPage(page - 1)}>
                            <AntDesign name={"arrowleft"} size={hp(15)} color={Colors.Primary}/>
                            <Text style={styles.next}>Önceki</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.arrowright} onPress={() => setPage(page + 1)}>
                            <Text style={styles.next}>Sonraki</Text>
                            <AntDesign name={"arrowright"} size={hp(15)} color={Colors.Primary}/>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.view} key="4">
                    <Image style={styles.background} source={bg_topRight}/>
                    <Image style={styles.image} source={HDImageFour}/>
                    <View style={[styles.space, {right: 0}]}/>

                    <Text
                        style={[styles.title, {color: Colors.Primary}]}>{getLocalizedString(LOCALIZED_HOWDESIGN_FOUR_TITLE)}</Text>
                    <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_HOWDESIGN_FOUR_SUBTITLE)}</Text>

                    <View style={styles.arrows}>
                        <TouchableOpacity style={styles.arrowleft} onPress={() => setPage(page - 1)}>
                            <AntDesign name={"arrowleft"} size={hp(15)} color={Colors.Primary}/>
                            <Text style={styles.next}>Önceki</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ViewPager>
        </View>
    )
}

export default HowDesign

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    space: {
        width: wp(185),
        height: "100%",
        position: "absolute",
        zIndex: 1,
        backgroundColor: Colors.white
    },
    viewPager: {

        flex: 1,
    },
    view: {
        alignItems: "center",
    },
    background: {
        top:hp(-50),
        position: "absolute",
        width: wp(375),
    },
    image: {
        marginTop: hp(37),
        zIndex: 2,
    },
    title: {
        width: wp(185),
        fontSize: wp(26),
        lineHeight: hp(37),
        fontWeight: "700",
        textAlign: "center",
        color: Colors.Secondary,
        fontFamily: "Gilroy-Bold",
        zIndex: 2,
        marginBottom: hp(15),
    },
    subtitle: {
        fontSize: wp(14),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(24),
        width: hp(284),
        textAlign: "center",
        zIndex: 2,
    },
    next: {
        textAlign: "center",
        color: Colors.Primary,
        fontSize: wp(12),
        fontWeight: "600",
        zIndex: 2,
    },
    arrows: {
        width: wp(377),
        flexDirection: "row",
        position: "absolute",
        bottom: hp(50),
        zIndex: 2,
        justifyContent: "center"
    },
    arrowleft: {
        marginTop: hp(21),
        flexDirection: "row",
        alignItems: "center",
        zIndex: 2,
        marginRight: wp(20),
    },
    arrowright: {
        marginTop: hp(21),
        flexDirection: "row",
        alignItems: "center",
        zIndex: 2,
    }
})
