import React from "react";
import {Image, StyleSheet, View, Text, FlatList, TouchableOpacity} from "react-native";
import {hp, wp} from "../../../../styles/Dimen";
import {Colors} from "../../../../styles/Colors";
import {dataTableSize} from "../../../../api/data";


const SizeTableContent = (props) => {

    var RandomNumber = Math.floor(Math.random() * 1000) + 1;

    const ItemTableSize = (items, index) => {
        return (
            <View key={"view4_" + RandomNumber + "_" + index}
                  style={index % 2 === 0 ? styles.itemcontainer : styles.itemcontainerNoColor}>
                {
                    items.map(row => {
                        var RandomNumber = Math.floor(Math.random() * 1000) + 1;

                        return <View key={"view3_" + RandomNumber + "_" + row.id}>
                            <Text key={"text3_" + RandomNumber + "_" + row.id}
                                  style={styles.itemtitle}>{row.text}</Text>
                        </View>
                    })
                }
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <FlatList data={dataTableSize}

                      renderItem={({item, index}) => ItemTableSize(item, index)}
                      showsHorizontalScrollIndicator={false}
                      keyExtractor={item => item[0].id}
                      pagingEnabled={true}
                      style={styles.flatlist}
            />
        </View>
    )
}

export default SizeTableContent

const styles = StyleSheet.create({
    container: {
        height: hp(188),
    },
    flatlist: {
        flexGrow: 1,
        marginTop: hp(20)
    },
    itemcontainer: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        borderRadius:  hp(10),
        backgroundColor: Colors.line_tab,
    },
    itemcontainerNoColor: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",

    },
    itemtitle: {
        textAlign: "center",
        fontSize: wp(12),
        color: Colors.title_active,
        fontWeight: "400",
        width: wp(48),
        marginRight: wp(8),
        lineHeight: hp(12),

        marginTop: hp(10),
        marginBottom: hp(5),
    },
    itemColor: {},

})
