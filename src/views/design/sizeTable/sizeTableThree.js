import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, Modal, ScrollView, Platform} from "react-native";
import {hp, wp} from "../../../styles/Dimen";
import {Colors} from "../../../styles/Colors";
import {BlurView} from "@react-native-community/blur";
import Styled from 'styled-components';
import Ionicons from "react-native-vector-icons/Ionicons";
import {ButtonPrimary} from "../../../components/buttons/buttonPrimary";
import {ButtonSecondary} from "../../../components/buttons/buttonSecondary";

const SizeTableThree = (props) => {
    return (<>
            {props.isShow &&
            <Modal animationType={"slide"} transparent>
                <View style={styles.container}>
                    <BlurWrapper
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={9}
                        reducedTransparencyFallbackColor="white"
                    />

                    <View style={styles.card}>

                        <TouchableOpacity style={styles.exitview} onPress={() => props.onPress(false, true, false)}>
                            <Ionicons name={"close"} color={Colors.Primary} size={wp(20)} style={styles.exit}/>
                        </TouchableOpacity>

                        <Text style={styles.title}>MySize Önerisi</Text>
                        <Text style={styles.textsize}>S</Text>

                        <Text style={styles.subtitle}>%98 gibi bir oranlar S bedenin sana uygun olacağını düşünüyoruz.
                            Dilersen hemen sepete ekleyip kaldığın yerden tişörtünü tasarlamaya devam edebilirsin ya da
                            yeniden hesaplama yapabilirsin.</Text>


                        <View style={styles.footer}>

                            <ButtonSecondary
                                onPress={() => props.onPress(false, true, false)}
                                text={"Yeniden Hesapla"}
                                style={styles.btnagain}
                                fontSize={hp(12)}
                                buttonColor={Colors.content_text}
                                iconVisible={false}
                            />

                        </View>
                    </View>
                </View>
            </Modal>
            }
        </>
    )
}

export default SizeTableThree

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    card: {
        height: hp(419),
        width: wp(320),
        backgroundColor: Colors.white,
        borderRadius:  hp(20),
        shadowColor: "#666666",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    exitview: {
        position: "absolute",
        right: Platform.OS === "ios" ? wp(-5) : wp(10),
        top: Platform.OS === "ios" ? hp(-30) : hp(10),
    },
    exit: {
        borderWidth: 1,
        height: hp(24),
        width: hp(24),
        borderRadius:  hp(12),
        textAlign: "center",
        justifyContent: "center",
        borderColor: Colors.Primary,
        alignItems: "center",
    },
    title: {
        marginTop: hp(27),
        fontSize: wp(20),
        fontWeight: "600",
        textAlign: "center",
        fontFamily: "Gilroy-bold"
    },
    textsize: {
        fontSize: wp(90),
        fontWeight: "400",
        textAlign: "center",
        color: Colors.Primary,
        marginTop: hp(19),

    },
    subtitle: {
        marginTop: hp(19),
        marginLeft: hp(22),
        marginRight: hp(22),
        marginBottom: hp(34),
        fontSize: wp(12),
        fontWeight: "400",
        textAlign: "center",
        lineHeight: hp(16),
        color: Colors.content_text
    },
    footer: {
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        marginTop: hp(25),
        height: hp(160),
        width: wp(123),
    },
    btngo: {
        height: hp(40),
        width: wp(276),
    }, btnagain: {
        marginTop: hp(15),
        height: hp(40),
        width: wp(276),
    }


})

const BlurWrapper = Styled(BlurView)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
