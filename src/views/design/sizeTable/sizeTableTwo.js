import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, Modal, ScrollView, Platform} from "react-native";
import {hp, wp} from "../../../styles/Dimen";
import {Colors} from "../../../styles/Colors";
import {BlurView} from "@react-native-community/blur";
import Styled from 'styled-components';
import Ionicons from "react-native-vector-icons/Ionicons";
import {GroupTabs} from "../../../components/tabs/tabs";
import {ButtonPrimary} from "../../../components/buttons/buttonPrimary";
import SeekBar from "../../../components/seekBars/seekBar";

const groupTabsData = [
    {
        key: "0",
        text: "Regular",
    },
    {
        key: "1",
        text: "Fit",
    },
    {
        key: "2",
        text: "Asymmetric",
    },
    {
        key: "3",
        text: "Oversize",
    },
];

const SizeTableTwo = (props) => {

    const [index, setIndex] = React.useState(0);

    return (<>
            {props.isShow &&
            <Modal animationType={"slide"} transparent>
                <View style={styles.container}>
                    <BlurWrapper
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={9}
                        reducedTransparencyFallbackColor="white"
                    />

                    <View style={styles.card}>

                        <TouchableOpacity style={styles.exitview} onPress={() => props.onPress(true, false, false)}>
                            <Ionicons name={"close"} color={Colors.Primary} size={wp(20)} style={styles.exit}/>
                        </TouchableOpacity>

                        <Text style={styles.title}>MySize Önerisi</Text>
                        <Text style={styles.subtitle}>4MySize dünyası sana sınırsız özgürlük tanıyor. Eğer beden
                            bilgilerinden emin değilsen
                            üzülme. Boy ve kilo bilgilerinle beraber giymeyi tercih ettiğin tarzı seç, biz de sana en
                            uygun bedeni bulalım.</Text>

                        <View style={{alignItems: "center", justifyContent: "center", marginLeft: wp(15)}}>
                            <ScrollView style={styles.grouptab} showsHorizontalScrollIndicator={false}
                                        horizontal={true}>
                                <GroupTabs
                                    callBackIndex={(index) => {
                                        setIndex(index);
                                    }}
                                    selectTab={0}
                                    values={groupTabsData}
                                    color={Colors.Primary}
                                    fontSize={hp(11)}/>
                            </ScrollView>
                        </View>

                        <View style={styles.footer}>

                            <SeekBar onChange={(value) => (value)} title={"BOYUN"}/>

                            <SeekBar onChange={(value) => (value)} title={"KİLON"}/>

                            <ButtonPrimary
                                onPress={() => props.onPress(false, false, true)}
                                text={"MySize Bedenini Bul"}
                                style={styles.btngo}
                                fontSize={hp(12)}
                                buttonColor={Colors.Primary}
                                iconVisible={false}
                            />

                        </View>
                    </View>
                </View>
            </Modal>
            }
        </>
    )
}

export default SizeTableTwo

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    card: {
        height: hp(419),
        width: wp(320),
        backgroundColor: Colors.white,
        borderRadius:  hp(20),
        shadowColor: "#666666",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    exitview: {
        position: "absolute",
        right: Platform.OS === "ios" ? wp(-5) : wp(10),
        top: Platform.OS === "ios" ? hp(-30) : hp(10),
    },
    exit: {
        borderWidth: 1,
        height: hp(24),
        width: hp(24),
        borderRadius:  hp(12),
        textAlign: "center",
        justifyContent: "center",
        borderColor: Colors.Primary,
        alignItems: "center",
    },
    title: {
        marginTop: hp(27),
        fontSize: wp(20),
        fontWeight: "600",
        textAlign: "center",
        fontFamily: "Gilroy-bold"
    }, subtitle: {
        marginTop: hp(19),
        marginLeft: hp(22),
        marginRight: hp(22),
        fontSize: wp(12),
        fontWeight: "400",
        textAlign: "center",
        lineHeight: hp(16),
        color: Colors.content_text
    },
    grouptab: {
        marginTop: hp(20),
    },
    footer: {
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        marginTop: hp(25),
        height: hp(160),
        width: wp(123),
    },
    findsize: {
        marginTop: hp(15),
        borderRadius:  hp(5),
        height: hp(32),
        width: wp(276),
        backgroundColor: Colors.PrimaryLight,
        justifyContent: "center",
        alignItems: "center",
    },
    findtext: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.Primary,
        textAlign: "center",
    },
    btngo: {
        marginTop: hp(50),
        height: hp(40),
        width: wp(276),
    }

})

const BlurWrapper = Styled(BlurView)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
