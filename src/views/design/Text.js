import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, TextInput, Dimensions} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AddZone from "./AddZone";
import ColorPicker from "../../components/pickers/colorPicker";
import {iconColorPicker} from "../../assets/Icon";
import {useDispatch, useSelector} from "react-redux";
import allActions from "../../redux/action";
import connect from "react-redux/lib/connect/connect";

function DesignText(props) {

    const dispatch = useDispatch()
    const {overline, underline, italic, bold, textColor, text, isColor, textZone} = useSelector(state => state.design)


    const [overlineState, setOverLine] = React.useState(overline)
    const [underlineState, setunderline] = React.useState(underline)
    const [italicState, setItalic] = React.useState(italic)
    const [boldState, setBold] = React.useState(bold)
    const [colorState, setColor] = React.useState(textColor)
    const [textState, setText] = React.useState(text)
    const [selectColor, setSelectColor] = React.useState(isColor)
    const [currentZone, setCurrentZone] = React.useState({id: textZone});

    const [isShow, setShow] = React.useState(false);
    const [showZone, setshowZone] = React.useState(false);

    const params = {
        overline: overlineState,
        underline: underlineState,
        italic: italicState,
        bold: boldState,
        textColor: colorState,
        text: textState,
        isColor: selectColor,
        textZone: currentZone.id,
    }

    React.useEffect(() => {
        dispatch(allActions.textAction(params))
    }, [overlineState, underlineState, italicState, boldState, colorState, isColor, textState, selectColor, currentZone])


    const TextTool = (props) => {
        return <>
            {!props.active &&
            <View style={{flexDirection: "row"}}>
                <TouchableOpacity onPress={() => {
                    setOverLine(overlineState === "line-through" ? "none" : "line-through")
                    setunderline("none")
                }}>
                    <Text style={styles.textoverline}>Aa</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    setunderline(underlineState === "underline" ? "none" : "underline")
                    setOverLine("none")
                }}>
                    <Text style={styles.textunderline}>U</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => setItalic(italicState === "italic" ? "normal" : "italic")}>
                    <Text style={styles.textitalic}>I</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => setBold(boldState === "Gilroy-Bold" ? "Gilroy-Regular" : "Gilroy-Bold")}>
                    <Text style={styles.textbold}>B</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    if (selectColor)
                        setColor(Colors.black)
                    setSelectColor(!selectColor)
                }}>
                    {selectColor ?
                        <View style={[styles.item, {backgroundColor: colorState}]}>
                            <AntDesign name={"check"} size={hp(15)} color={Colors.white}/>
                        </View> : <View style={[styles.item, {backgroundColor: colorState}]}/>
                    }
                </TouchableOpacity>

                <TouchableOpacity onPress={() => setShow(!isShow)}>
                    <View style={[styles.item]}>
                        <Image source={iconColorPicker}/>
                    </View>
                </TouchableOpacity>
            </View>
            }
        </>
    }


    return (<>
            {showZone === false ?
                <View style={[styles.container, props.style]}>
                    <View style={styles.content}>

                        <View style={styles.header}>
                            <TextTool active={isShow}/>

                            <ColorPicker
                                active={isShow}
                                onPress={(color, active) => {
                                    setShow(active)
                                    setColor(color)
                                    setSelectColor(true)
                                }}/>
                        </View>

                        <View style={styles.footer}>
                            <View style={styles.card}>

                                <Text style={[styles.cardtext,
                                    {
                                        textDecorationLine: overlineState === "none" ? underlineState : overlineState,
                                        fontStyle: italicState,
                                        fontFamily: boldState,
                                        color: colorState,
                                    }]}>{textState === "" ? "4 MySize" : textState}</Text>
                                <View
                                    style={{backgroundColor: Colors.input_background, width: wp(2), height: hp(30)}}/>

                                <TextInput
                                    autoFocus={true}
                                    placeholder={"4 MySize"}
                                    style={styles.input}
                                    onChangeText={(text1 => setText(text1))}
                                    value={textState}
                                />
                            </View>

                            <TouchableOpacity onPress={() => setshowZone(true)}>
                                <View style={styles.next}>
                                    <MaterialIcons name={"navigate-next"} size={hp(20)} color={Colors.Secondary}/>
                                    <Text style={styles.nexttext}>Eklenecek Bölge</Text>

                                </View>
                            </TouchableOpacity>

                            <View style={styles.info}>
                                <Text style={styles.infotext}>Ekleyeceğiniz yazı baskı yapılacaktır.</Text>
                            </View>
                        </View>
                    </View>
                </View>
                :
                <AddZone onPress={(showZone, currentZone) => {
                    setshowZone(showZone);
                    setCurrentZone(currentZone);
                }}
                         currentId={currentZone}
                         label={"Yazıya Göz At"}/>
            }
        </>
    )
}


export default DesignText

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {},
    header: {
        marginTop: hp(30),
        marginLeft: wp(30),
        flexDirection: "row",
    },
    textoverline: {
        marginRight: wp(30),
        textDecorationLine: 'line-through',
    },
    textunderline: {
        marginRight: wp(30),
        textDecorationLine: 'underline'
    },
    textitalic: {
        marginRight: wp(30),
        fontStyle:"italic"
    },
    textbold: {
        marginRight: wp(100),
        fontFamily: 'Gilroy-bold'
    }, item: {
        borderRadius: hp(20),
        height: hp(20),
        width: hp(20),
        top: hp(-5),
        marginRight: wp(20),
        backgroundColor: Colors.Secondary,
        justifyContent: "center",
        alignItems: "center",

    },
    card: {
        height: hp(80),
        width: wp(304),
        marginLeft: wp(22),
        marginRight: wp(22),
        borderRadius: hp(20),
        marginTop: hp(10),
        backgroundColor: Colors.line_tab,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },
    cardtext: {
        marginRight: wp(30),
        marginLeft: wp(30),
    },
    input: {
        color: Colors.title_active,
        height: hp(40),
        width: wp(150),
        marginLeft: wp(30),
    },
    next: {
        flexDirection: "row",
        marginTop: hp(10),
        justifyContent: "center",
        alignItems: "center"
    },
    nexttext: {
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "600",
        color: Colors.Secondary,
    },
    info: {
        flexDirection: "row",
        marginTop: hp(10),
        justifyContent: "center",
        alignItems: "center"
    },
    infotext: {
        textAlign: "center",
        fontSize: wp(14),
        fontWeight: "600",
        color: Colors.title_active,
    },

})
