import React from "react";
import {Image, StyleSheet, View, Text, FlatList, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataNick} from "../../api/data";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AddZone from "./AddZone";
import {useDispatch, useSelector} from "react-redux";
import allActions from "../../redux/action";

//import ItemSize from "../items/sizeItem";

function DesignNick(props) {

    const dispatch = useDispatch()
    const {nick, nickZone} = useSelector(state => state.design)

    const [selectNick, setselectNick] = React.useState({id: nick});

    const [showZone, setshowZone] = React.useState(false);
    const [currentZone, setCurrentZone] = React.useState({id: nickZone});


    const params = {
        nick: selectNick.id,
        nickZone: currentZone.id,

    }

    React.useEffect(() => {
        dispatch(allActions.nickAction(params))
    }, [selectNick])


    const handleNick = (item) => {
        setselectNick(item)
    }

    const NickItems = ({item}) => {
        return <ItemNick
            item={item}
            select={selectNick}
            onPress={handleNick}
        />
    }

    const ItemNick = (props) => {
        return (
            <TouchableOpacity onPress={() => props.onPress(props.item)}><View
                style={props.item.id === props.select.id ? styles.itemSizeContainerActive : styles.itemSizeContainer}>
                <Text
                    style={props.item.id === props.select.id ? styles.itemtitleActive : styles.itemtitle}>{props.item.title}</Text>
            </View></TouchableOpacity>)
    }

    return (<>
            {showZone === false ?
                <View style={[styles.container, props.style]}>
                    <View style={styles.content}>
                        <View style={styles.viewsize}>
                            <Text style={styles.title}>Rumuz</Text>

                            <FlatList data={dataNick}
                                      renderItem={NickItems}
                                      showsHorizontalScrollIndicator={false}
                                      keyExtractor={(item) => (item.id)}
                                      extraData={selectNick}
                                      horizontal={true}
                                      pagingEnabled={true}
                                      style={styles.flatlist}
                            />

                            <TouchableOpacity onPress={() => setshowZone(true)}>
                                <View style={styles.next}>
                                    <MaterialIcons name={"navigate-next"} size={hp(20)} color={Colors.Secondary}/>
                                    <Text style={styles.nexttext}>Eklenecek Bölge</Text>

                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                :
                <AddZone
                    onPress={(showZone, currentZone) => {
                        setshowZone(showZone);
                        setCurrentZone(currentZone);
                    }}
                    currentId={currentZone}
                    label={"Rumuza Göz At"}/>
            }
        </>
    )
}

export default DesignNick


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        width: wp(345),
    },
    viewsize: {
        flexDirection: "column",
    },
    title: {
        fontSize: wp(16),
        fontWeight: "700",
        fontFamily: "Gilroy-Bold",
        marginTop: hp(26),
        marginLeft: wp(15),
        marginBottom: hp(12),
    },
    flatlist: {
        flexGrow: 1,

    }, itemSizeContainerActive: {
        height: hp(40),
        marginLeft: wp(18),
        justifyContent: "center",
        backgroundColor: Colors.Primary,
        borderRadius: hp(15),
    },
    itemSizeContainer: {
        height: hp(40),
        marginLeft: wp(18),
        justifyContent: "center",
        backgroundColor: Colors.line_tab,
        borderRadius: hp(15),
    },
    itemtitle: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "400",
    },
    itemtitleActive: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        color: Colors.white,
        fontSize: wp(12),
        fontWeight: "400",
    },
    next: {
        flexDirection: "row",
        marginTop: hp(40),
        justifyContent: "center",
        alignItems: "center"
    },
    nexttext: {
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "600",
        color: Colors.Secondary,
    },

})
