import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, TouchableWithoutFeedback, FlatList} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataColor, dataPrinting} from "../../api/data";
import AntDesign from "react-native-vector-icons/AntDesign";
import {useDispatch, useSelector} from "react-redux";
import allActions from "../../redux/action";
import connect from "react-redux/lib/connect/connect";

function DesignPrinting(props) {

    const dispatch = useDispatch()
    const {printing} = useSelector(state => state.design)


    const [selectPrinting, setselectPrinting] = React.useState({id: printing});

    const params = {
        printing: selectPrinting.id,
    }

    React.useEffect(() => {
        dispatch(allActions.printingAction(params))
    }, [selectPrinting])


    const handlePrinting = (item) => {
        setselectPrinting(item)
    }

    const PrintingItems = ({item}) => {
        return <ItemPrinting
            item={item}
            select={selectPrinting}
            onPress={handlePrinting}
        />
    }

    const ItemPrinting = (props) => {
        return (
            <>
                <TouchableOpacity onPress={() => props.onPress(props.item)}>
                    <View style={[styles.itemcontainer,
                        {backgroundColor: props.item.id === props.select.id ? Colors.Primary : Colors.line_tab}]}>

                        <TouchableWithoutFeedback onPress={() => alert(index)}>
                            <View
                                style={[styles.icon, {backgroundColor: props.item.id === props.select.id ? Colors.white : Colors.Secondary}]}>
                                <AntDesign name={"question"} size={hp(15)}
                                           color={props.item.id === props.select.id ? Colors.Primary : Colors.white}/>
                            </View>
                        </TouchableWithoutFeedback>

                        <Text style={[styles.itemtitle,
                            {color: props.item.id === props.select.id ? Colors.white : Colors.title_active}]}>{props.item.title}</Text>
                    </View>
                </TouchableOpacity>
            </>)
    }

    return (
        <View style={[styles.container, props.style]}>
            <View style={styles.content}>
                <FlatList data={dataPrinting}
                          numColumns={3}
                          renderItem={PrintingItems}
                          showsHorizontalScrollIndicator={false}
                          keyExtractor={(item) => (item.id)}
                          extraData={selectPrinting}
                          pagingEnabled={true}
                          style={styles.flatlist}
                />
            </View>
        </View>
    )
}

export default DesignPrinting

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {},
    itemcontainer: {
        width: wp(90),
        height: hp(60),
        justifyContent: "center",
        borderRadius: hp(10),
        backgroundColor: Colors.Primary,
        marginRight: wp(15),

        marginTop: hp(15),
        marginLeft: wp(10)
    },
    image: {
        width: wp(53),
        height: hp(77),
    },
    itemtitle: {
        marginLeft: wp(17),
        marginRight: wp(17),
        marginTop: hp(15),
        width: wp(57),
        lineHeight: hp(15),
        textAlign: "center",
        fontSize: wp(12),
        color: Colors.content_text,
        fontWeight: "400",
        marginBottom: hp(5),
    },
    itemSelect: {
        borderRadius: hp(20),
        height: hp(18),
        width: hp(18),
        backgroundColor: Colors.line_tab,
        justifyContent: "center",
        alignItems: "center",

    },
    flatlist: {
        flexGrow: 1
    },
    icon: {
        height: hp(16),
        width: hp(16),
        position: "absolute",
        right: wp(5),
        top: hp(5),
        backgroundColor: Colors.Secondary,
        borderRadius: hp(30),
        justifyContent: "center",
        alignItems: "center",
        zIndex: 100,
    }
})
