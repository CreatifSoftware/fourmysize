import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, FlatList} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataColor} from "../../api/data";
import AntDesign from "react-native-vector-icons/AntDesign";
import connect from "react-redux/lib/connect/connect";
import allActions from "../../redux/action";
import {useDispatch, useSelector} from "react-redux";

function DesignColor(props) {

    const dispatch = useDispatch()
    const {color} = useSelector(state => state.design)

    const [selectColor, setselectColor] = React.useState({id: color});

    const params = {
        color: selectColor.id,
    }

    React.useEffect(() => {
        dispatch(allActions.colorAction(params))
    }, [selectColor])


    const handleColor = (item) => {
        setselectColor(item)
    }

    const ColorItems = ({item}) => {
        return <ItemSize
            item={item}
            select={selectColor}
            onPress={handleColor}
        />
    }

    const ItemSize = (props) => {
        return (
            <TouchableOpacity onPress={() => props.onPress(props.item)}>
                <View style={[styles.item, {backgroundColor: props.item.color}]}>
                    {props.item.id === props.select.id ? <AntDesign name={"check"} size={hp(20)} color={
                        props.item.color === '#FFFFFF' ? Colors.black : Colors.white}
                    /> : null}
                </View>
            </TouchableOpacity>)
    }

    return (
        <View style={[styles.container, props.style]}>
            <View style={styles.content}>
                <FlatList data={dataColor}
                          numColumns={5}
                          renderItem={ColorItems}
                          showsHorizontalScrollIndicator={false}
                          keyExtractor={(item) => (item.id)}
                          extraData={selectColor}
                          pagingEnabled={true}
                          style={styles.flatlist}
                />
            </View>
        </View>
    )
}


export default DesignColor

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {},
    item: {
        borderRadius: hp(20),
        height: wp(35),
        width: wp(35),
        marginRight: wp(32),
        marginTop: hp(26),
        justifyContent: "center",
        alignItems: "center",
    }, flatlist: {
        flexGrow: 1,
        marginLeft: wp(20),

    },
})
