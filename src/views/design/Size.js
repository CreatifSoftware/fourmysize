import React from "react";
import {Image, StyleSheet, View, Text, FlatList, TouchableOpacity, Modal} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataSize, dataStyle} from "../../api/data";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {useNavigation} from "@react-navigation/native";
import SizeTableOne from "./sizeTable/sizeTableOne";
import SizeTableTwo from "./sizeTable/sizeTableTwo";
import SizeTableThree from "./sizeTable/sizeTableThree";
import connect from "react-redux/lib/connect/connect";
import {useDispatch, useSelector} from "react-redux";
import allActions from "../../redux/action";

function DesignSize(props) {

    const dispatch = useDispatch()
    const {size, style} = useSelector(state => state.design)

    const [selectSize, setselectSize] = React.useState({id: size});
    const [selectStyle, setselectStyle] = React.useState({id: style});

    const [showSizeTableOne, setshowSizeTableOne] = React.useState(false);
    const [showSizeTableTwo, setshowSizeTableTwo] = React.useState(false);
    const [showSizeTableThree, setshowSizeTableThree] = React.useState(false);

    const params = {
        size: selectSize.id,
        style: selectStyle.id,
    }


    const handleSize = (item) => {
        dispatch(allActions.sizeAction(params))
        setselectSize(item)
    }

    const SizeItems = ({item}) => {
        return <ItemSize
            item={item}
            select={selectSize}
            onPress={handleSize}
        />
    }

    const handleStyle = (item) => {
        dispatch(allActions.sizeAction(params))
        setselectStyle(item)
    }

    const StyleItems = ({item}) => {
        return <ItemStyle
            item={item}
            select={selectStyle}
            onPress={handleStyle}
        />
    }

    const ItemSize = (props) => {
        return <TouchableOpacity onPress={() => props.onPress(props.item)}><View
            style={props.item.id === props.select.id ?
                styles.itemSizeContainerActive :
                styles.itemSizeContainer}>
            <Text
                style={props.item.id === props.select.id ?
                    styles.itemtitleActive :
                    styles.itemtitle}>{props.item.size}</Text>
        </View></TouchableOpacity>
    }

    const ItemStyle = (props) => {
        return <TouchableOpacity onPress={() => props.onPress(props.item)}><View
            style={props.item.id === props.select.id ?
                styles.itemSizeContainerActive :
                styles.itemSizeContainer}>
            <Text
                style={props.item.id === props.select.id ?
                    styles.itemtitleActive :
                    styles.itemtitle}>{props.item.size}</Text>
        </View></TouchableOpacity>
    }

    return (
        <>
            <SizeTableOne isShow={showSizeTableOne} onPress={(oneshow, twoshow, threeshow) => {

                setshowSizeTableOne(oneshow)
                setshowSizeTableTwo(twoshow)
                setshowSizeTableThree(threeshow)
            }}/>
            <SizeTableTwo isShow={showSizeTableTwo} onPress={(oneshow, twoshow, threeshow) => {
                setshowSizeTableOne(oneshow)
                setshowSizeTableTwo(twoshow)
                setshowSizeTableThree(threeshow)

            }}/>
            <SizeTableThree isShow={showSizeTableThree} onPress={(oneshow, twoshow, threeshow) => {
                setshowSizeTableOne(oneshow)
                setshowSizeTableTwo(twoshow)
                setshowSizeTableThree(threeshow)
            }}/>

            <View style={[styles.container, props.style]}>
                <View style={styles.content}>
                    <View style={styles.viewsize}>
                        <View style={{flexDirection: "row"}}>
                            <Text style={styles.title}>Beden</Text>

                            <TouchableOpacity style={styles.sizetable}
                                              onPress={() => setshowSizeTableOne(true)}>
                                <View style={styles.next}>
                                    <MaterialCommunityIcons name={"hanger"} size={hp(15)} color={Colors.Primary}/>
                                    <Text style={styles.nexttext}>Ölçü Tablosu</Text>

                                </View>
                            </TouchableOpacity>
                        </View>

                        <FlatList data={dataSize}
                                  renderItem={SizeItems}
                                  showsHorizontalScrollIndicator={false}
                                  keyExtractor={(item) => (item.id)}
                                  extraData={selectSize}
                                  horizontal={true}
                                  style={styles.flatlist}
                        />
                    </View>

                    <View>
                        <Text style={styles.title}>Tarz</Text>
                        <FlatList data={dataStyle}
                                  renderItem={StyleItems}
                                  showsHorizontalScrollIndicator={false}
                                  keyExtractor={(item) => item.id}
                                  extraData={selectStyle}
                                  horizontal={true}
                                  style={styles.flatlist}
                        />
                    </View>

                </View>
            </View>
        </>
    )
}


export default DesignSize

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {},
    viewsize: {
        flexDirection: "column",
    },
    title: {
        fontSize: wp(16),
        fontWeight: "700",
        fontFamily: "Gilroy-Bold",
        marginTop: hp(24),
        marginLeft: wp(15),
        marginBottom: hp(10),
    },
    flatlist: {
        flexGrow: 1,

    }, itemSizeContainerActive: {
        height: hp(40),
        marginLeft: wp(18),
        justifyContent: "center",
        backgroundColor: Colors.Primary,
        borderRadius: hp(15),
    },
    itemSizeContainer: {
        height: hp(40),
        marginLeft: wp(18),
        justifyContent: "center",
        backgroundColor: Colors.line_tab,
        borderRadius: hp(15),
    },
    itemtitle: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "400",
    },
    itemtitleActive: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        color: Colors.white,
        fontSize: wp(12),
        fontWeight: "400",
    },
    sizetable: {
        position: "absolute",
        right: 0,
    }, next: {
        flexDirection: "row",
        marginTop: hp(26),
        marginRight: wp(15),
        justifyContent: "center",
        alignItems: "center"
    },
    nexttext: {
        textAlign: "center",
        fontSize: wp(10),
        fontWeight: "600",
        color: Colors.Primary,
        marginLeft: wp(5),
    }, modalContainer: {
        flexGrow: 1,
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: 'transparent',
    },

})
