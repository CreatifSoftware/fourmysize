import React from "react";
import {Image, StyleSheet, View, Text, ScrollView, FlatList, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {GroupTabsFilter} from "../../components/tabs/tabsFilter";
import {dataPattern, dataSize} from "../../api/data";
import {patternImage} from "../../assets/Images";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AddZone from "./AddZone";

const groupTabsData = [
    {
        key: "1",
        text: "Abstract",
    },
    {
        key: "2",
        text: "Bilim",
    },
    {
        key: "3",
        text: "Doğa",
    },
    {
        key: "4",
        text: "Spor",
    },
    {
        key: "5",
        text: "Uzay",
    },
];

function DesignPattern(props) {

    const [index, setIndex] = React.useState(0);
    const [selectPattern, setselectPattern] = React.useState(-1);
    const [showZone, setshowZone] = React.useState(false);
    const [currentZone, setCurrentZone] = React.useState(0);


    React.useEffect(() => {
        setIndex(index)
    }, [index]);

    const handlePattern = (item) => {
        setselectPattern(item)
    }

    const PrintingItems = ({item}) => {
        return <ItemPattern
            item={item}
            select={selectPattern}
            onPress={handlePattern}
        />
    }

    const ItemPattern = (props) => {
        return (<>
            <TouchableOpacity onPress={() => props.onPress(props.item)}>
                <View style={styles.itemcontainer}>
                    <Image style={styles.image} source={props.item.image}/>
                    <Text style={styles.itemtitle}>{props.item.title}</Text>

                    <View
                        style={[styles.itemSelect, {backgroundColor: props.item.id === props.select.id ? Colors.Primary : Colors.line_tab}]}>
                        {props.item.id === props.select.id &&
                        <AntDesign name={"check"} size={hp(12)} color={Colors.white}/>}
                    </View>
                </View>
            </TouchableOpacity>
        </>)
    }

    return (<>
        {showZone === false ?

            <View style={[styles.container, props.style]}>
                <View style={styles.content}>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>

                        <GroupTabsFilter
                            callBackIndex={(index) => {
                                setIndex(index);
                            }}
                            values={groupTabsData}
                        />
                    </ScrollView>

                    <FlatList data={dataPattern[index]}
                              renderItem={PrintingItems}
                              showsHorizontalScrollIndicator={false}
                              keyExtractor={(item) => (item.id)}
                              extraData={selectPattern}
                              horizontal={true}
                              style={styles.flatlist}
                    />

                    <TouchableOpacity onPress={() => setshowZone(true)}>
                        <View style={styles.footer}>
                            <MaterialIcons name={"navigate-next"} size={hp(20)} color={Colors.Secondary}/>
                            <Text style={styles.footertext}>Eklenecek Bölge</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View> :
            <AddZone onPress={(showZone, currentZone) => {
                setshowZone(showZone);
                setCurrentZone(currentZone);
            }}
                     currentId={currentZone} label={"Desenlere Göz At"}/>
        }


    </>)
}

export default DesignPattern

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    content: {
        marginTop: hp(20)
    },
    flatlist: {

        marginTop: hp(10)
    },
    itemcontainer: {
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        width: wp(53),
        height: hp(77),
    },
    itemtitle: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        fontSize: wp(10),
        color: Colors.content_text,
        fontWeight: "400",
        marginBottom: hp(5),
    },
    itemSelect: {
        borderRadius:  hp(20),
        height: hp(18),
        width: hp(18),
        backgroundColor: Colors.line_tab,
        justifyContent: "center",
        alignItems: "center",

    },
    footer: {
        flexDirection: "row",
        marginTop: hp(15),
        justifyContent: "center",
        alignItems: "center"
    },
    footertext: {
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "600",
        color: Colors.Secondary,
    }
})
