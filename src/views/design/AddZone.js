import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, FlatList} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import AntDesign from "react-native-vector-icons/AntDesign";
import {dataAddZone} from "../../api/data";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AddressItem from "../../components/items/addressItem";

const AddZone = (props) => {

    const [selectZone, setselectZone] = React.useState(props.currentId);

    const handleAddZone = (item) => {
        setselectZone(item)
    }

    const AddZoneItems = ({item}) => {
        return <ItemAddZone
            item={item}
            select={selectZone}
            onPress={handleAddZone}
        />
    }


    const ItemAddZone = (props) => {
        return (<>
            <TouchableOpacity onPress={() => props.onPress(props.item)}>
                <View style={styles.itemcontainer}>
                    <Image style={styles.image} source={props.item.image}/>
                    <Text style={styles.itemtitle}>{props.item.title}</Text>
                    <Text style={styles.itemsubtitle}>{props.item.subtitle}</Text>

                    <View
                        style={[styles.itemSelect, {backgroundColor: props.item.id === props.select.id ? Colors.Primary : Colors.line_tab}]}>
                        {props.item.id === props.select.id &&
                        <AntDesign name={"check"} size={hp(12)} color={Colors.white}/>}
                    </View>
                </View>
            </TouchableOpacity>
        </>)
    }

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <FlatList data={dataAddZone}
                          renderItem={AddZoneItems}
                          showsHorizontalScrollIndicator={false}
                          keyExtractor={(item) => (item.id)}
                          extraData={selectZone}
                          horizontal={true}

                          style={styles.flatlist}
                />

                <TouchableOpacity onPress={() => props.onPress(false, selectZone)}>
                    <View style={styles.footer}>
                        <MaterialIcons name={"navigate-next"} size={hp(20)} color={Colors.Secondary}/>
                        <Text style={styles.footertext}>{props.label}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default AddZone

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        marginTop: hp(20)
    },
    flatlist: {

        marginTop: hp(10)
    },
    itemcontainer: {
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        width: wp(53),
        height: hp(77),
    },
    itemtitle: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        fontSize: wp(14),
        color: Colors.title_active,
        fontWeight: "700",
        fontFamily: "Gilroy-bold",
        marginTop: hp(10),
        marginBottom: hp(5),
    },
    itemsubtitle: {
        marginLeft: wp(17),
        marginRight: wp(17),
        textAlign: "center",
        fontSize: wp(10),
        color: Colors.content_text,
        fontWeight: "400",
        marginBottom: hp(5),
    },
    itemSelect: {
        borderRadius:  hp(20),
        height: hp(18),
        width: hp(18),
        backgroundColor: Colors.line_tab,
        justifyContent: "center",
        alignItems: "center",

    },
    footer: {
        flexDirection: "row",
        marginTop: hp(10),
        justifyContent: "center",
        alignItems: "center"
    },
    footertext: {
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "600",
        color: Colors.Secondary,
    }
})
