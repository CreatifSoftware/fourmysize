import React from "react";
import {Image, StyleSheet, View, Text, Modal, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import Styled from "styled-components";
import {BlurView} from "@react-native-community/blur";
import PropTypes from "prop-types";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";

const SuccessPopup = (props) => {
    return <>
        {
            props.isShow &&
            <Modal animationType={"slide"} transparent>
                <View style={styles.container}>
                    <BlurWrapper
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={9}
                        reducedTransparencyFallbackColor="white"/>

                    <View style={styles.card}>
                        <TouchableOpacity style={styles.exitview} onPress={() => props.onPress(false)}>
                            <Ionicons name={"close"} color={Colors.Success} size={wp(20)} style={styles.exit}/>
                        </TouchableOpacity>

                        <Entypo name={"check"} size={hp(34)} color={Colors.Success}/>
                        <Text style={styles.title}>{props.title}</Text>
                    </View>
                </View>
            </Modal>
        }
    </>
}

SuccessPopup.propTypes = {
    isShow: PropTypes.bool,
    onPress:PropTypes.func,
    title:PropTypes.string,
}

export default SuccessPopup

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    card: {
        width: wp(310),
        height: hp(187),
        backgroundColor: Colors.white,
        borderRadius: hp(24),
        shadowColor: "#a4a4a4",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        width: wp(214),
        marginTop: hp(32),
        fontSize: wp(20),
        fontWeight: "600",
        lineHeight: hp(24),
        color: Colors.Success,
        fontFamily: "Gilroy-bold",
        textAlign:"center",
    },
    exitview: {
        position: "absolute",
        right: wp(10),
        top: hp(10),
    },
    exit: {
        borderWidth: 1,
        height: hp(24),
        width: hp(24),
        borderRadius: hp(12),
        textAlign: "center",
        justifyContent: "center",
        borderColor: Colors.Success,
        alignItems: "center",
    },
})

const BlurWrapper = Styled(BlurView)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
