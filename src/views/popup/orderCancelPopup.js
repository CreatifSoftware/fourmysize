import React from "react";
import {Image, StyleSheet, View, Text, Modal, TouchableOpacity, Alert} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import Styled from "styled-components";
import {BlurView} from "@react-native-community/blur";
import PropTypes from "prop-types";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import MultiTextInput from "../../components/inputs/multiTextInput";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {ButtonSecondary} from "../../components/buttons/buttonSecondary";
import p from "./successPopup";
import SuccessPopup from "./successPopup";

const OrderCancelPopup = (props) => {


    const [show, setshow] = React.useState(false);

    const handleCancel = () => {
        props.onPress(false)
        setshow(true)
    }

    return <>
        {
            props.isShow &&
            <Modal animationType={"slide"} transparent>
                <View style={styles.container}>
                    <BlurWrapper
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={9}
                        reducedTransparencyFallbackColor="white"
                    />

                    <View style={styles.card}>

                        <TouchableOpacity style={styles.exitview} onPress={() => props.onPress(false)}>
                            <Ionicons name={"close"} color={Colors.Primary} size={hp(20)} style={styles.exit}/>
                        </TouchableOpacity>

                        <Text style={styles.title}>{props.title}</Text>
                        <MultiTextInput
                            placeHolder={"Lütfen bir kaç şey yazıınız..."}
                            style={styles.multiText}
                        />

                        <ButtonPrimary
                            onPress={handleCancel}
                            text={"Siparişimi İptal Et"}
                        />

                        <ButtonSecondary
                            onPress={() => props.onPress(false)}
                            text={"Vazgeç"}
                            buttonColor={Colors.content_text}
                        />
                    </View>
                </View>
            </Modal>
        }
        {
            show &&
            <SuccessPopup
                isShow={show}
                onPress={(is) => setshow(is)}
                title={"İptal Talebin İşleme Alınmıştır"}/>
        }
    </>
}

OrderCancelPopup.propTypes = {
    isShow: PropTypes.bool,
    onPress: PropTypes.func,
    title: PropTypes.string,
}

export default OrderCancelPopup

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    card: {
        width: wp(316),
        height: hp(364),
        backgroundColor: Colors.white,
        borderRadius: hp(24),
        shadowColor: "#a4a4a4",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
        justifyContent: "center",
        alignItems: "center",

    },
    title: {
        width: wp(276),
        marginTop: hp(32),

        fontSize: wp(14),
        fontWeight: "700",
        lineHeight: hp(24),
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        textAlign: "center",
    },
    multiText: {
        width: wp(276),
        height: hp(143),
        marginTop: hp(11),
    },
    exitview: {
        position: "absolute",
        right: wp(-5),
        top: hp(-30),
    },
    exit: {
        borderWidth: 1,
        height: hp(24),
        width: hp(24),
        borderRadius: hp(12),
        textAlign: "center",
        justifyContent: "center",
        borderColor: Colors.Primary,
        alignItems: "center",
    },
})

const BlurWrapper = Styled(BlurView)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
