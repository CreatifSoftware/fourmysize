import React from "react";
import {StyleSheet, View, TouchableOpacity, Text} from "react-native";
import {Colors} from "../../styles/Colors";
import {wp, hp} from "../../styles/Dimen"
import {useNavigation} from "@react-navigation/native";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import {useSelector} from "react-redux";

const OrderButton = (props) => {

    const {orders} = useSelector(state => state.shoppingCart)
    const navigation = useNavigation()

    const OrderDetailsRedirect = () => {
        navigation.navigate("OrderDetails")
    }

    return <View style={styles.container}>
        <TouchableOpacity onPress={OrderDetailsRedirect}>
            {orders.length > 0 && <>
                <View style={styles.ordercount}>
                    <Text style={styles.text}>{orders.length}</Text>
                </View>
            </>}
            <SimpleLineIcons style={styles.icon} color={props.color} size={hp(20)} name={"basket"}/>

        </TouchableOpacity>
    </View>
}

export default OrderButton

const styles = StyleSheet.create({
    container: {},
    icon: {
        height: hp(24),
        width: hp(24),
        marginRight: wp(8),
    },
    ordercount: {
        width: hp(15),
        height: hp(15),
        backgroundColor: Colors.Primary,
        position: "absolute",
        top: hp(-5),
        right: wp(2),
        borderRadius: hp(15),
        justifyContent: "center",
        alignItems: "center",
    },
    text: {
        fontSize: wp(10),
        fontWeight: "700",
        textAlign: "center",
        color: Colors.white
    }
})
