import React from "react";
import {Image, StyleSheet, TouchableOpacity, View} from "react-native";
import {iconMenu, iconMenuWhite} from "../../assets/Icon";
import {hp, wp} from "../../styles/Dimen";
import {useNavigation} from '@react-navigation/native';
import {Colors} from "../../styles/Colors";


const DrawerButton = (props) => {

    const navigation = useNavigation()

    const DrawerOpen = () => {
        navigation.openDrawer()
    }

    return <View style={styles.container}>
        <TouchableOpacity onPress={DrawerOpen}>
            {props.color === Colors.white ?
                <Image style={styles.icon} source={iconMenuWhite}/> :
                <Image style={styles.icon} source={iconMenu}/>}
        </TouchableOpacity>
    </View>
}

export default DrawerButton

const styles = StyleSheet.create({
    container: {},
    icon: {
        height: hp(24),
        width: hp(24),
        marginLeft: wp(8),
    }
})
