import React from "react";
import { View, Text, StyleSheet} from "react-native";
import {Colors} from "../../styles/Colors";
import {wp, hp} from "../../styles/Dimen"

const PriceTitle = (props) => {

    return <View style={styles.container}>
            <Text style={styles.text}>65 TL</Text>
        </View>
}

export default PriceTitle

const styles = StyleSheet.create({
    container: {},
    containerDizayn: {},
    text: {
        fontSize: wp(16),
        fontWeight: "600",
        marginRight: wp(10),
        color: Colors.Secondary_1,
    }
})
