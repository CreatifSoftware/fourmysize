import React from "react";
import {
    Image, StyleSheet, View, Text, TouchableOpacity, LayoutAnimation, Platform, UIManager, Modal
} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import {BlurView} from "@react-native-community/blur";
import {Colors} from "../../styles/Colors";
import {tourOne, tourThree, tourTwo} from "../../assets/Images";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import AntDesign from "react-native-vector-icons/AntDesign";

if (Platform.OS === "android" && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

const DesignTour = (props) => {

    const [istour, setTour] = React.useState(props.active)
    const [istourOne, setTourOne] = React.useState("right")
    const [istourTwo, setTourTwo] = React.useState("left")
    const [istourThree, setTourThree] = React.useState("left")


    const openBox = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        setTourOne(istourOne === "left" ? "right" : "left")

    };

    const openBox2 = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        setTourTwo(istourTwo === "left" ? "right" : "left")

    };

    const openBox3 = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        setTourThree(istourThree === "left" ? "right" : "left")
    };

    return (
        <View>
            {istour === true ?
                <Modal transparent={true} animationType={"slide"}>

                    <BlurView
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={4}
                        reducedTransparencyFallbackColor="white"
                    />

                    {istourOne === "right" &&
                    <View style={istourOne === "left" ? styles.touroneHide : styles.touroneShow}>
                        <Image source={tourOne}/>
                        <TouchableOpacity style={styles.arrow} onPress={() => {
                            openBox()
                            openBox2()
                        }}>
                            <Text style={styles.next}>Sonraki</Text>
                            <AntDesign name={"arrowright"} size={20} color={Colors.Primary}/>
                        </TouchableOpacity>
                    </View>
                    }

                    {istourTwo === "right" &&

                    <View style={istourTwo === "left" ? styles.tourtwoHide : styles.tourtwoShow}>

                        <Image source={tourTwo}/>

                        <TouchableOpacity style={styles.arrow} onPress={() => {
                            openBox2()
                            openBox3()
                        }}>
                            <Text style={styles.next}>Sonraki</Text>
                            <AntDesign name={"arrowright"} size={20} color={Colors.Primary}/>

                        </TouchableOpacity>
                    </View>
                    }

                    {istourThree === "right" &&
                    <View style={istourThree === "left" ? styles.tourthreeHide : styles.tourthreeShow}>
                        <Image source={tourThree}/>

                    </View>
                    }

                    <TouchableOpacity onPress={() => setTour(false)} style={styles.skip}>
                        <MaterialIcons name={"navigate-next"} size={20} color={Colors.Secondary}/>
                        <Text style={styles.skiptext}>Bilgilendirmeyi Geç</Text>
                    </TouchableOpacity>
                </Modal> : null}
        </View>
    )
}

export default DesignTour

const styles = StyleSheet.create({
    container: {
        height: hp(605),
        width: wp(384),
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    skip: {
        position: "absolute",
        bottom: hp(10),
        left: wp(10),
        flexDirection: "row",
        alignItems: "center"
    },
    skiptext: {
        color: Colors.Secondary,
        fontWeight: "600",
        fontSize: wp(12),
    },
    touroneHide: {
        position: "absolute",
        opacity: 1,
        marginTop: hp(120),
        marginLeft: wp(45),
    },
    touroneShow: {
        opacity: 1,
        position: "absolute",
        marginTop: hp(120),
        marginLeft: wp(45),
    },
    tourtwoHide: {
        opacity: 0,
        marginTop: hp(120),
        position: "absolute",
        marginLeft: wp(15),
    },
    tourtwoShow: {
        opacity: 1,
        marginTop: hp(120),
        position: "absolute",
        marginLeft: wp(15),
    },
    tourthreeHide: {
        opacity: 0,
        marginTop: hp(240),
        position: "absolute",
        marginLeft: wp(45),
    },
    tourthreeShow: {
        opacity: 1,
        marginTop: hp(240),
        position: "absolute",
        marginLeft: wp(45),
    },
    next: {
        width: wp(270),
        textAlign: "right",
        color: Colors.Primary,
        fontSize: wp(12),
        fontWeight: "600"
    },
    arrow: {
        marginTop: hp(11),
        flexDirection: "row",
        alignItems: "center",
    }
})
