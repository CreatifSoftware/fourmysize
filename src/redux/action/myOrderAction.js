export const MY_ORDER_REQUEST = 'MY_ORDER_REQUEST'


export function myOrderAction(params) {
    return {
        type: MY_ORDER_REQUEST,
        payload: params,
    }
}
