export const SIZE = 'SIZE'
export const COLOR = 'COLOR'
export const PATTERN = 'PATTERN'
export const EMBROINDERY = 'EMBROINDERY'
export const TEXT = 'TEXT'
export const NICK = 'NICK'
export const PRINTING = 'PRINTING'
export const ADD_TEMPLATE = 'ADD_TEMPLATE'
export const REMOVE_TEMPLATE = 'REMOVE_TEMPLATE'
export const RESET_TEMPLATE = 'RESET_TEMPLATE'

export function sizeAction(params) {
    return {
        type: SIZE,
        payload: {
            size: params.size,
            style: params.style,
        }
    }
}

export function colorAction(params) {
    return {
        type: COLOR,
        payload: {
            color: params.color,
        }
    }
}

export function patternAction(params) {
    return {
        type: PATTERN,
        payload: {
            pattern: params.pattern,
            patternZone: params.patternZone,
        }
    }
}

export function embroideryAction(params) {
    return {
        type: EMBROINDERY,
        payload: {
            embroidery: params.embroidery,
        }
    }
}

export function textAction(params) {
    return {
        type: TEXT,
        payload: {
            overline: params.overline,
            underline: params.underline,
            italic: params.italic,
            bold: params.bold,
            textColor: params.textColor,
            isColor: params.isColor,
            text: params.text,
            textZone: params.textZone,
        }
    }
}

export function nickAction(params) {
    return {
        type: NICK,
        payload: {
            nick: params.nick,
            nickZone: params.nickZone,
        }
    }
}

export function printingAction(params) {
    return {
        type: PRINTING,
        payload: {
            printing: params.printing,
        }
    }
}

export function addTemplateAction(params) {
    return {
        type: ADD_TEMPLATE,
        payload: params
    }
}

export function removeTemplateAction(params) {
    return {
        type: REMOVE_TEMPLATE,
        payload: params
    }
}

export function resetTemplateAction() {
    return {
        type: RESET_TEMPLATE,
    }
}
