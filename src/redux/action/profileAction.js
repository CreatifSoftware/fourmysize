export const PROFILE_EDIT = 'PROFILE_EDIT'
export const ADD_FAVORITE = 'ADD_FAVORITE'
export const REMOVE_FAVORITE = 'REMOVE_FAVORITE'

export function profileAction(params) {
    return {
        type: PROFILE_EDIT,
        payload: {
            user: params,
        }
    }
}

export function addFavoriteAction(params) {
    return {
        type: ADD_FAVORITE,
        payload: params,

    }
}

export function removeFavoriteAction(params) {
    return {
        type: REMOVE_FAVORITE,
        payload: params,

    }
}
