export const ORDER_ADD = 'ORDER_ADD'
export const ORDER_REMOVE = 'ORDER_REMOVE'
export const ORDER_UPDATE = 'ORDER_UPDATE'
export const ORDER_CLEAR = 'ORDER_CLEAR'
export const UPDATE_TOTAL_PRICE = 'UPDATE_TOTAL_PRICE'

export function addOrderAction(params) {
    return {
        type: ORDER_ADD,
        payload: params,
    }
}

export function updateOrderAction(params, index) {
    return {
        type: ORDER_UPDATE,
        payload: params,
        index: index,
    }
}

export function removeOrderAction(params) {
    return {
        type: ORDER_REMOVE,
        payload: params,
    }
}

export function clearOrderAction() {
    return {
        type: ORDER_CLEAR,
        payload: {
            orders: null,
        }
    }
}

export function totalPriceAction() {
    return {
        type: UPDATE_TOTAL_PRICE,
    }
}
