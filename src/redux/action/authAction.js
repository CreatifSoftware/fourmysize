export const REQUEST = 'REQUEST'
export const SUCCESS = 'SUCCESS'
export const FAILURE = 'FAILURE'

export  function authAction(response) {
    return {
        type: REQUEST,
        payload: {
            customerId: response.customerId,
            token: response.token,
            refreshToken: response.refreshToken,
            tokenExpire: response.tokenExpire,
        }
    }
}


