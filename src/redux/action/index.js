import {authAction} from "./authAction";
import {
    addTemplateAction,
    colorAction,
    embroideryAction,
    nickAction,
    patternAction,
    printingAction,
    sizeAction,
    textAction,
    removeTemplateAction,
    resetTemplateAction,
} from "./designAction";
import {
    addOrderAction,
    clearOrderAction,
    removeOrderAction,
    totalPriceAction,
    updateOrderAction
} from "./shoppingCartAction";
import {initialAction} from "./masterAction";
import {myOrderAction} from "./myOrderAction";
import {addFavoriteAction, removeFavoriteAction} from "./profileAction";

export const allActions = {
    initialAction,
    authAction,

    sizeAction,
    colorAction,
    patternAction,
    embroideryAction,
    textAction,
    nickAction,
    printingAction,
    addTemplateAction,
    resetTemplateAction,
    removeTemplateAction,

    addOrderAction,
    removeOrderAction,
    clearOrderAction,
    totalPriceAction,
    updateOrderAction,

    myOrderAction,

    addFavoriteAction,
    removeFavoriteAction,
}

export default allActions
