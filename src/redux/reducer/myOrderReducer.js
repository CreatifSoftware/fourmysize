import {MY_ORDER_REQUEST} from "../action/myOrderAction";

const initialState = {
    orders: [],
}

export function myOrderReducer(state = initialState, action) {

    switch (action.type) {
        case MY_ORDER_REQUEST :
            return {...state, orders:  action.payload}

        default :
            return state
    }
}
