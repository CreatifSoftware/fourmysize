import {ORDER_ADD, ORDER_CLEAR, ORDER_REMOVE, ORDER_UPDATE, UPDATE_TOTAL_PRICE} from "../action/shoppingCartAction";

const initialState = {
    cargoPrice: 10,
    totalPrice: 0,
    orders: [],
}

export function shoppingCartReducer(state = initialState, action) {

    switch (action.type) {
        case ORDER_ADD :
            return {...state, orders: [...state.orders, action.payload]}
        case ORDER_REMOVE :
            return {
                orders: [...state.orders.filter(item => item.id !== action.payload)]
            }
        case ORDER_UPDATE :
            state.orders[action.index] = {...state.orders[action.index], ...action.payload}
            return {...state}
        case ORDER_CLEAR :
            return {orders: []}
        case UPDATE_TOTAL_PRICE :
            let total = 0;
            state.orders.map(item => {
                total += parseInt(item.price)
            })
            return {...state, totalPrice: total}
        default :
            return state
    }
}
