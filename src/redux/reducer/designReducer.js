import {
    COLOR,
    EMBROINDERY,
    NICK,
    PATTERN,
    PRINTING,
    SIZE,
    TEXT,
    ADD_TEMPLATE,
    REMOVE_TEMPLATE,
    RESET_TEMPLATE
} from "../action/designAction";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt} from "../../assets/Images";
import uuid from "react-native-uuid";


const initialState = {
    id: uuid.v4(),
    //Size Component
    size: -1,
    style: -1,
    //Color Component
    color: -1,
    //Pattern Component
    pattern: -1,
    patternZone: -1,
    //Embroidery Component
    embroidery: -1,
    //Nick Component
    nick: -1,
    nickZone: -1,
    //Printing Component
    printing: -1,
    //Text Component
    overline: "none",
    underline: "none",
    italic: "normal",
    bold: "Gilroy-Regular",
    textColor: Colors.title_active,
    isColor: false,
    text: "4 MySize",
    textZone: 0,
    title: "",
    type: "",
    image: threeDModelTshirt,
    myTemplates: []
}


export function designReducer(state = initialState, action) {

    switch (action.type) {
        case SIZE :
            return Object.assign(initialState, state, action.payload);
        case COLOR :
            return Object.assign(initialState, state, action.payload);
        case PATTERN :
            return Object.assign(initialState, state, action.payload);
        case EMBROINDERY :
            return Object.assign(initialState, state, action.payload);
        case TEXT :
            return Object.assign(initialState, state, action.payload);
        case NICK :
            return Object.assign(initialState, state, action.payload);
        case PRINTING :
            return Object.assign(initialState, state, action.payload);
        case ADD_TEMPLATE :
            const index = state.myTemplates.findIndex((item) => item.id === action.payload.id)

            if (index !== -1) {
                state.myTemplates[index] = {...state.myTemplates[index], ...action.payload}
                return {...state}
            } else
                return {...state, myTemplates: [...state.myTemplates, action.payload]}
        case REMOVE_TEMPLATE :
            return {
                ...state, myTemplates: [...state.myTemplates.filter(item => item.id !== action.payload)]
            }
        case RESET_TEMPLATE :
            return Object.assign(
                {
                    myTemplates: state.myTemplates,
                    id: uuid.v4(),
                    size: -1, style: -1,
                    color: -1, pattern: -1,
                    patternZone: -1, embroidery: -1,
                    nick: -1, nickZone: -1, printing: -1,
                    overline: "none", underline: "none",
                    italic: "normal", bold: "Gilroy-Regular",
                    textColor: Colors.title_active,
                    isColor: false, text: "4 MySize",
                    textZone: 0, title: "",
                    type: "", image: threeDModelTshirt,
                }
            )
        default :
            return state
    }
}
