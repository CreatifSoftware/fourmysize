import {PROFILE_EDIT, ADD_FAVORITE, REMOVE_FAVORITE} from "../action/profileAction";


const initialState = {
    user: {
        nameUserName: 'Mert',
        nick: 'blacksea',
        email: 'i@mertkaradeniz.com',
        gsm: '545 854 66 32',
    },
    myFavorites: []
}


export function profileReducer(state = initialState, action) {


    switch (action.type) {
        case PROFILE_EDIT :
            return Object.assign(initialState, state, action.payload);
        case ADD_FAVORITE :
            return {...state, myFavorites: [...state.myFavorites, action.payload]}
        case REMOVE_FAVORITE :
            return {...state, myFavorites: [...state.myFavorites.filter(item => item.id !== action.payload)]}
        default :
            return state
    }
}
