import {authReducer} from "./authReducer";
import {combineReducers} from "redux";
import {masterReducer} from "./masterReducer";
import {designReducer} from "./designReducer";
import {shoppingCartReducer} from "./shoppingCartReducer";
import {profileReducer} from "./profileReducer";
import {myOrderReducer} from "./myOrderReducer";

export const allReducer = combineReducers({
    master: masterReducer,
    auth: authReducer,
    design:designReducer,
    shoppingCart:shoppingCartReducer,
    profile:profileReducer,
    myOrders:myOrderReducer,
})
