import {REQUEST, SUCCESS, FAILURE} from "../action/authAction";

export function authReducer(state = '', action) {

    const initialState = {
        customerId: "",
        token: "",
        refreshToken: "",
        tokenExpire: "",
    }

    switch (action.type) {
        case REQUEST :
            return Object.assign(initialState, state, action.payload);
        case FAILURE :
            return Object.assign(initialState, state);
        case SUCCESS :
            return Object.assign(initialState, state, {users: action.payload});

        default :
            return state
    }
}
