import React from 'react'
import { View, } from 'react-native'
import auth from '@react-native-firebase/auth'
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager, } from 'react-native-fbsdk'


const getInfoFromToken = (token) => {

  const PROFILE_REQUEST_PARAMS = {
    fields: {
      string: 'id,name,first_name,last_name,email',
    },
  }

  const profileRequest = new GraphRequest(
    '/me',
    { token, parameters: PROFILE_REQUEST_PARAMS },
    (error, user) => {
      if (error) {
        console.log('login info has error: ' + error)
      } else {

        console.log('result:', user)
      }
    },
  )
  new GraphRequestManager().addRequest(profileRequest).start()
}

class facebook {

  async onFacebookButtonPress () {


    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions(['public_profile', 'email'])

    if (result.isCancelled) {
      console.log('User cancelled the login process')
      return
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken()

    if (data != null)
      AccessToken.getCurrentAccessToken().then(data => {
        const accessToken = data.accessToken.toString()
        getInfoFromToken(accessToken)
      })

    if (!data) {
      console.log('Something went wrong obtaining access token')
    }

    // Create emailPass.js Firebase credential with the AccessToken
    //const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken)

    // Sign-in the user with the credential
    return true
  }

}

const face = new facebook()
export default face
