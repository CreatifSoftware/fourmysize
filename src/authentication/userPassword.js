import React from 'react'
import { View } from 'react-native'
import auth from '@react-native-firebase/auth'
import firebase from '@react-native-firebase/app'

class apple {

  _handleSubmitSingIn (values) {
    auth()
      .signInWithEmailAndPassword(values.email, values.password)
      .then(() => {
        console.log('User account created & signed in!')
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!')
        }
        if (error.code === 'auth/wrong-password') {
          console.log('Wrong Password')
        }
        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!')
        }

      })
  }

  _handleSubmitCreateAccount (values) {
    auth()
      .createUserWithEmailAndPassword(values.email, values.password)
      .then(() => {
        console.log('User account created & signed in!')
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!')
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!')
        }

        console.error(error)
      })
  }


}

const email = new apple()
export default email
