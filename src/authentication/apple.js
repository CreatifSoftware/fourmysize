/*
import React, {useState, useEffect} from 'react';
import {AppRegistry, StyleSheet, View, Text} from 'react-native';
import appleAuth, {
    AppleButton,
    appleAuthAndroid,

} from '@invertase/react-native-apple-authentication';
import {v4 as uuid} from "uuid";
import 'react-native-get-random-values'
import {nanoid} from "nanoid";

let user=null;

async function fetchAndUpdateCredentialState(updateCredentialStateForUser) {
    if (user === null) {
        updateCredentialStateForUser('N/A');
    } else {
        const credentialState = await appleAuth.getCredentialStateForUser(user);
        if (credentialState === appleAuth.State.AUTHORIZED) {
            updateCredentialStateForUser('AUTHORIZED');
        } else {
            updateCredentialStateForUser(credentialState);
        }
    }
}

/!**
 * Starts the Sign In flow.
 *!/
export async function onApple(updateCredentialStateForUser) {
    console.warn('Beginning Apple Authentication');

    // start a login request
    try {
        const appleAuthRequestResponse = await appleAuth.performRequest({
            requestedOperation: appleAuth.Operation.LOGIN,
            requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
        });

        console.log('appleAuthRequestResponse', appleAuthRequestResponse);

        const {
            user: newUser,
            email,
            nonce,
            identityToken,
            realUserStatus /!* etc *!/,
        } = appleAuthRequestResponse;

        user = newUser;

        fetchAndUpdateCredentialState(updateCredentialStateForUser).catch(error =>
            updateCredentialStateForUser(`Error: ${error.code}`),
        );

        if (identityToken) {
            // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
            console.log(nonce, identityToken);
        } else {
            // no token - failed sign-in?
        }

        if (realUserStatus === appleAuth.UserStatus.LIKELY_REAL) {
            console.log("I'm a real person!");
        }

        console.warn(`Apple Authentication Completed, ${user}, ${email}`);
    } catch (error) {
        if (error.code === appleAuth.Error.CANCELED) {
            console.warn('User canceled Apple Sign in.');
        } else {
            console.error(error);
        }
    }
}

export async function onAppleButtonPress() {

    const rawNonce = nanoid();
    const state = nanoid();

    appleAuthAndroid.configure({
        clientId: 'com.example.client-android',
        redirectUri: 'https://example.com/auth/callback',
        responseType: "form_post",
        scope: "name",
        id_token:"asdasdasd23",
        nonce: rawNonce,
        state,
    });


    const response = appleAuthAndroid.signIn().then(value => console.log(value))
        .catch(e => console.log(e));

    return response
}
*/
