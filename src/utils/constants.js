export const LOCALIZED_WELCOMEPAGE_BTN_INDIVIDUAL = 'welcome_btn_individual'
export const LOCALIZED_WELCOMEPAGE_BTN_CORPORATE = 'welcome_btn_corporate'
export const LOCALIZED_WELCOMEPAGE_BTN_GO = 'welcome_btn_go'
export const LOCALIZED_WELCOMEPAGE_TITLE = 'welcome_title'
export const LOCALIZED_WELCOMEPAGE_TITLE_NAME = 'welcome_title_name'

export const LOCALIZED_LOGIN_TITLE = 'login_title'
export const LOCALIZED_LOGIN_SUB_TITLE = 'login_sub_title'
export const LOCALIZED_LOGIN_EMAIL = 'login_email'
export const LOCALIZED_LOGIN_PASSWORD = 'login_password'
export const LOCALIZED_LOGIN_PASSWORD_REPEATE = 'login_password_repeate'
export const LOCALIZED_LOGIN_BUTTON_GO = 'login_button_go'
export const LOCALIZED_LOGIN_TEXT_REGISTER = 'login_text_register'
export const LOCALIZED_LOGIN_BUTTON_REGISTER = 'login_button_register'
export const LOCALIZED_LOGIN_TEXT_SOCIAL = 'login_text_social'

export const LOCALIZED_REGISTER_TITLE = 'register_title'
export const LOCALIZED_REGISTER_SUB_TITLE = 'register_sub_title'
export const LOCALIZED_REGISTER_USERNAME = 'register_username'
export const LOCALIZED_REGISTER_EMAIL = 'register_email'
export const LOCALIZED_REGISTER_PHONE = 'register_phone'
export const LOCALIZED_REGISTER_PASS = 'register_password'
export const LOCALIZED_REGISTER_CONTRACT = 'register_contract'
export const LOCALIZED_REGISTER_BUTTON_GO = 'register_button_go'
export const LOCALIZED_REGISTER_TEXT_REGISTER = 'register_text_register'
export const LOCALIZED_REGISTER_BUTTON_REGISTER = 'register_button_register'

export const LOCALIZED_FORGOT_TITLE = 'forgot_title'
export const LOCALIZED_FORGOT_BUTTON_GO = 'forgot_button_go'

export const LOCALIZED_HOMEPAGE_PIC_1_TEXT = 'homepage_pic_1_text'
export const LOCALIZED_HOMEPAGE_PIC_2_TEXT = 'homepage_pic_2_text'
export const LOCALIZED_HOMEPAGE_PIC_3_TEXT = 'homepage_pic_3_text'
export const LOCALIZED_HOMEPAGE_PIC_4_TEXT = 'homepage_pic_4_text'
export const LOCALIZED_HOMEPAGE_TITLE = 'homepage_title'
export const LOCALIZED_HOMEPAGE_SUB_TITLE = 'homepage_sub_title'
export const LOCALIZED_HOMEPAGE_BUTTON_1_TEXT = 'homepage_button_1'
export const LOCALIZED_HOMEPAGE_BUTTON_2_TEXT = 'homepage_button_2'

export const LOCALIZED_MYOWNPAGEONE_TITLE = 'myownpageone_title'
export const LOCALIZED_MYOWNPAGEONE_SUB_TITLE = 'myownpageone_sub_title'
export const LOCALIZED_MYOWNPAGEONE_BUTTON_GO = 'myownpageone_button_go'

export const LOCALIZED_MYOWNPAGETWO_TITLE = 'myownpagetwo_title'
export const LOCALIZED_MYOWNPAGETWO_SUB_TITLE = 'myownpagetwo_sub_title'
export const LOCALIZED_MYOWNPAGETWO_BUTTON_GO = 'myownpagetwo_button_go'
export const LOCALIZED_MYOWNPAGETWO_BUTTON_BACK = 'myownpagetwo_button_back'

export const LOCALIZED_MYOWNPAGETHREE_TITLE = 'myownpagethree_title'
export const LOCALIZED_MYOWNPAGETHREE_SUB_TITLE = 'myownpagethree_sub_title'
export const LOCALIZED_MYOWNPAGETHREE_BUTTON_GO = 'myownpagethree_button_go'
export const LOCALIZED_MYOWNPAGETHREE_BUTTON_BACK = 'myownpagethree_button_back'

export const LOCALIZED_DESIGN_ITEMS_COLOR = 'design_items_color'
export const LOCALIZED_DESIGN_ITEMS_SIZE = 'design_items_size'
export const LOCALIZED_DESIGN_ITEMS_EMBROIDERY = 'design_items_embroidery'
export const LOCALIZED_DESIGN_ITEMS_PATTERN = 'design_items_pattern'
export const LOCALIZED_DESIGN_ITEMS_TEXT = 'design_items_text'
export const LOCALIZED_DESIGN_ITEMS_NICK = 'design_items_nick'
export const LOCALIZED_DESIGN_ITEMS_PRINTING = 'design_items_printing'

export const LOCALIZED_ORDER_STATUS_PRODUCT_NAME = 'orderstatus_product_name'
export const LOCALIZED_ORDER_STATUS_COLORSIZE_NAME = 'orderstatus_colorsize_name'
export const LOCALIZED_ORDER_STATUS_PAYTYPE_NAME = 'orderstatus_paytype_name'
export const LOCALIZED_ORDER_STATUS_TOTALPRICE_NAME = 'orderstatus_totalprice_name'
export const LOCALIZED_ORDER_STATUS_CARGO_NAME = 'orderstatus_cargo_name'
export const LOCALIZED_ORDER_STATUS_CARGODATE_NAME = 'orderstatus_cargodate_name'
export const LOCALIZED_ORDER_STATUS_CARGODELIVERY_NAME = 'orderstatus_cargodelivery_name'
export const LOCALIZED_ORDER_STATUS_CARGOTRAKING_NAME = 'orderstatus_cargotraking_name'

export const LOCALIZED_STATUS_ONE_TITLE = 'status_one_title'
export const LOCALIZED_STATUS_ONE_SUBTITLE = 'status_one_subtitle'
export const LOCALIZED_STATUS_ONE_BUTTON_1_TEXT = 'status_one_btn1_text'
export const LOCALIZED_STATUS_ONE_BUTTON_2_TEXT = 'status_one_btn2_text'

export const LOCALIZED_STATUS_TWO_TITLE = 'status_two_title'
export const LOCALIZED_STATUS_TWO_SUBTITLE = 'status_two_subtitle'
export const LOCALIZED_STATUS_TWO_BUTTON_1_TEXT = 'status_two_btn1_text'
export const LOCALIZED_STATUS_TWO_BUTTON_2_TEXT = 'status_two_btn2_text'

export const LOCALIZED_STATUS_THREE_TITLE = 'status_three_title'
export const LOCALIZED_STATUS_THREE_SUBTITLE = 'status_three_subtitle'
export const LOCALIZED_STATUS_THREE_BUTTON_1_TEXT = 'status_three_btn1_text'

export const LOCALIZED_STATUS_FOUR_TITLE = 'status_four_title'
export const LOCALIZED_STATUS_FOUR_SUBTITLE = 'status_four_subtitle'
export const LOCALIZED_STATUS_FOUR_BUTTON_1_TEXT = 'status_four_btn1_text'

export const LOCALIZED_HOWDESIGN_ONE_TITLE = "howdesign_one_title"
export const LOCALIZED_HOWDESIGN_ONE_SUBTITLE = "howdesign_one_subtitle"
export const LOCALIZED_HOWDESIGN_TWO_TITLE = "howdesign_two_title"
export const LOCALIZED_HOWDESIGN_TWO_SUBTITLE = "howdesign_two_subtitle"
export const LOCALIZED_HOWDESIGN_THREE_TITLE = "howdesign_three_title"
export const LOCALIZED_HOWDESIGN_THREE_SUBTITLE = "howdesign_three_subtitle"
export const LOCALIZED_HOWDESIGN_FOUR_TITLE = "howdesign_four_title"
export const LOCALIZED_HOWDESIGN_FOUR_SUBTITLE = "howdesign_four_subtitle"
