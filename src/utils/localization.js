import React from "react";
import {language} from "../api/data";


const langId = 'TR'

export const getLocalizedString = (key) => {
    let filteredList = language.filter(arg => arg.key === key)
    if (filteredList.length > 0) {
        return langId === 'TR' ? filteredList[0].valueTR : null
    }
}

