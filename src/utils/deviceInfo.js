import DeviceInfo from 'react-native-device-info';
import uuid from "react-native-uuid"


export const deviceId = DeviceInfo.getDeviceId();

export const deviceName = () => {
    return new Promise((resolve, reject) => {
        DeviceInfo.getDeviceName().then(deviceName => {
            resolve(deviceName)
        })
    })
}

export const deviceIp = () => {
    return new Promise((resolve, reject) => {
        DeviceInfo.getIpAddress().then(ip => {
            resolve(ip)
        })
    })
}

export const uuidCreate = () => {
    return new Promise((resolve, reject) => {
        resolve(uuid.v4())
    })
}
