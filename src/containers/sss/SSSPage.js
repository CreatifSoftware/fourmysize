import React from "react";
import {Image, StyleSheet, View, Text, FlatList} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataSSS} from "../../api/data";
import SSSItem from "../../components/items/SSSItem";
import {LineBottom} from "../../components/lines/lineBottom";

const SSSPage = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Sana nasıl
                yardımcı olabiliriz?</Text>

            <View style={styles.content}>
                <FlatList
                    style={styles.flatlist}
                    data={dataSSS}
                    renderItem={SSSItem}
                    numColumns={3}
                    contentContainerStyle={styles.flatlist}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={item => item.id}
                />
            </View>
            <LineBottom/>
        </View>
    )
}

export default SSSPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    },
    content: {
        justifyContent: "center",
        alignItems: "center"
    },
    flatlist: {
        flexGrow: 1,
    },
    title: {
        fontSize: wp(21),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        width: wp(183),
        marginTop: hp(22),
        marginLeft: wp(16),
    },
})
