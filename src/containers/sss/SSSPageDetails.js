import React from "react";
import {Image, StyleSheet, View, Text, FlatList} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataSSSProfile} from "../../api/data";
import SSSDetailItem from "../../components/items/SSSDetailItem";
import {LineBottom} from "../../components/lines/lineBottom";
import {headerDefaultConfig} from "../../navigation/stackNavigation";

const SSSPageDetails = ({navigation, route}) => {


    React.useEffect(() => {
        navigation.setOptions(headerDefaultConfig(Colors.white, route.params.title,true))
    }, [])


    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <FlatList
                    style={styles.flatlist}
                    data={dataSSSProfile}
                    renderItem={SSSDetailItem}
                    contentContainerStyle={styles.flatlist}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={item => item.id}
                />
            </View>
            <LineBottom/>
        </View>
    )
}

export default SSSPageDetails

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background
    },
    content: {
        justifyContent: "center",
        alignItems: "center"
    },
    flatlist: {
        flexGrow: 1,
        paddingBottom: hp(40)
    },
})
