import React from "react";
import {Image, StatusBar, StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt, LineDizayn} from "../../assets/Images";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {Select} from "../../components/select/select";
import {LineBottom} from "../../components/lines/lineBottom";
import {getLocalizedString} from "../../utils/localization";
import {LOCALIZED_MYOWNPAGEONE_BUTTON_GO,LOCALIZED_MYOWNPAGEONE_SUB_TITLE,LOCALIZED_MYOWNPAGEONE_TITLE} from "../../utils/constants";

const MyOwnStylePageOne = (props) => {

    const MyOwnStyleTwoRedirect = () => {
        props.navigation.navigate("MyOwnStylePageTwo")
    }

    return <View style={styles.container}>

        <Image style={styles.background} source={LineDizayn}/>


        <View style={styles.content}>


            <Select text={"Yetişkin Erkek"}
                    fontSize={12}
                    textColor={Colors.white}
                    style={{
                        marginTop: hp(30),

                    }}
            />

            <Image style={styles.threeD} source={threeDModelTshirt}/>


            <Text style={styles.title}>{getLocalizedString(LOCALIZED_MYOWNPAGEONE_TITLE)}</Text>
            <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_MYOWNPAGEONE_SUB_TITLE)}</Text>

            <ButtonPrimary
                onPress={MyOwnStyleTwoRedirect}
                text={getLocalizedString(LOCALIZED_MYOWNPAGEONE_BUTTON_GO)}
                style={styles.btngo}
                fontSize={12}
                buttonColor={Colors.Primary}
                iconVisible={true}
            />

            <LineBottom/>
        </View>


    </View>
}

export default MyOwnStylePageOne

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    threeD: {
        width: wp(232),
        height: hp(278),
        resizeMode:"contain"
    },
    content: {
        flex: 1,
        alignItems: "center",
    },

    title: {
        fontSize: wp(26),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-Bold",
        marginTop: hp(30),
        includeFontPadding:false,
        marginBottom: hp(15),
    },
    subtitle: {
        includeFontPadding:false,
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(24),
        width: hp(284),
        textAlign: "center"
    },
    btngo: {
        marginTop: hp(16),
        height: hp(40),
        width: wp(135),
    },

})
