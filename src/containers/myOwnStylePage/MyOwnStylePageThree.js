import React from "react";
import {Image, StatusBar, StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelFabric, LineDizayn} from "../../assets/Images";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {Select} from "../../components/select/select";
import {LineBottom} from "../../components/lines/lineBottom";
import {getLocalizedString} from "../../utils/localization";
import {
    LOCALIZED_MYOWNPAGETHREE_BUTTON_GO,
    LOCALIZED_MYOWNPAGETHREE_SUB_TITLE,
    LOCALIZED_MYOWNPAGETHREE_TITLE,
    LOCALIZED_MYOWNPAGETHREE_BUTTON_BACK
} from "../../utils/constants";

const MyOwnStylePageThree = (props) => {

    const goBack = () => {
        props.navigation.goBack()
    }

    const DizaynHomeRedirect = () => {
        props.navigation.navigate("DizaynHome")
    }

    return <View style={styles.container}>

        <Image style={styles.background} source={LineDizayn}/>

        <View style={styles.content}>


            <Select text={"Yetişkin Erkek"}
                    fontSize={12}
                    textColor={Colors.white}
                    style={{
                        marginTop: hp(31),
                    }}
            />

            <Image style={styles.threeD} source={threeDModelFabric}/>


            <Text style={styles.title}>{getLocalizedString(LOCALIZED_MYOWNPAGETHREE_TITLE)}</Text>
            <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_MYOWNPAGETHREE_SUB_TITLE)}</Text>

            <ButtonPrimary
                onPress={DizaynHomeRedirect}
                fontSize={12}
                text={getLocalizedString(LOCALIZED_MYOWNPAGETHREE_BUTTON_GO)}
                style={styles.btngo}
                buttonColor={Colors.Secondary}
                iconVisible={false}
            />

            <ButtonPrimary
                onPress={goBack}
                fontSize={12}
                text={getLocalizedString(LOCALIZED_MYOWNPAGETHREE_BUTTON_BACK)}
                style={styles.btngo}
                buttonColor={Colors.line_tab}
                textColor={Colors.content_text}
                iconVisible={true}
                btnLeft={true}
                iconColor={Colors.content_text}
            />

            <LineBottom/>
        </View>


    </View>
}

export default MyOwnStylePageThree

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    threeD: {
        marginTop:hp(10),
        width: wp(140),
        height: hp(213),
        resizeMode:"contain"
    },
    content: {
        flex: 1,
        alignItems: "center",
    },
    title: {
        includeFontPadding:false,
        fontSize: wp(26),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-Bold",
        marginTop: hp(30),
        marginBottom: hp(15),
    },
    subtitle: {
        includeFontPadding:false,
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(24),
        width: hp(284),
        textAlign: "center"
    },
    btngo: {
        marginTop: hp(16),
        height: hp(40),
        width: wp(135),
    },

})
