import React from "react";
import {Image, StatusBar, StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt, LineDizayn} from "../../assets/Images";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {Select} from "../../components/select/select";
import {LineBottom} from "../../components/lines/lineBottom";
import {getLocalizedString} from "../../utils/localization";
import {
    LOCALIZED_MYOWNPAGETWO_BUTTON_BACK,
    LOCALIZED_MYOWNPAGETWO_BUTTON_GO,
    LOCALIZED_MYOWNPAGETWO_SUB_TITLE,
    LOCALIZED_MYOWNPAGETWO_TITLE
} from "../../utils/constants";

const MyOwnStylePageTwo = (props) => {

    /** useLayoutEffect(() => {
        navigation.setOptions({
            header: (props) => <StepHeader {...props} stepCount={4} step={3} />,
            headerRight: () => <HeaderCancelButton navigation={navigation} />
        })
    }, [navigation]);
     */

    const goBack = () => {
        props.navigation.goBack()
    }

    const MyOwnStyleThreeRedirect = () => {
        props.navigation.navigate("MyOwnStylePageThree")
    }

    return <View style={styles.container}>

        <Image style={styles.background} source={LineDizayn}/>


        <View style={styles.content}>


            <Select text={"Yetişkin Erkek"}
                    fontSize={12}
                    textColor={Colors.white}
                    style={{
                        marginTop: hp(31),
                    }}
            />

            <Image style={styles.threeD} source={threeDModelTshirt}/>


            <Text style={styles.title}>{getLocalizedString(LOCALIZED_MYOWNPAGETWO_TITLE)}</Text>
            <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_MYOWNPAGETWO_SUB_TITLE)}</Text>

            <ButtonPrimary
                onPress={MyOwnStyleThreeRedirect}
                text={getLocalizedString(LOCALIZED_MYOWNPAGETWO_BUTTON_GO)}
                style={styles.btngo}
                fontSize={12}
                buttonColor={Colors.Primary}
                iconVisible={true}
            />

            <ButtonPrimary
                onPress={goBack}
                text={getLocalizedString(LOCALIZED_MYOWNPAGETWO_BUTTON_BACK)}
                style={styles.btngo}
                fontSize={12}
                buttonColor={Colors.line_tab}
                textColor={Colors.content_text}
                iconVisible={true}
                btnLeft={true}
                iconColor={Colors.content_text}
            />

            <LineBottom/>
        </View>


    </View>
}

export default MyOwnStylePageTwo

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    threeD: {
        width: wp(232),
        height: hp(278),
        resizeMode:"contain"
    },
    content: {
        flex: 1,
        alignItems: "center",
    },

    title: {
        includeFontPadding:false,
        fontSize: wp(26),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-Bold",
        marginTop: hp(30),
        marginBottom: hp(15),
    },
    subtitle: {
        includeFontPadding:false,
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(24),
        width: hp(284),
        textAlign: "center"
    },
    btngo: {
        marginTop: hp(14),
        height: hp(40),
        width: wp(135),
    },

})
