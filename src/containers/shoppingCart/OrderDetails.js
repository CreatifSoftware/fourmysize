import React from "react";
import {StyleSheet, View, Text, TouchableOpacity, RefreshControl, LayoutAnimation, Alert} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {LineBottom} from "../../components/lines/lineBottom";
import OrderItem from "../../components/items/orderItem";
import {dataOrder} from "../../api/data"
import AntDesign from "react-native-vector-icons/AntDesign";
import {SwipeListView} from "react-native-swipe-list-view";
import OrderHideItems from "../../components/items/orderHideItems";
import {useNavigation} from "@react-navigation/native";
import {useDispatch, useSelector} from "react-redux";
import allActions from "../../redux/action";
import uuid from "react-native-uuid";

const OrderDetails = (props) => {

    const dispatch = useDispatch()
    const {orders, totalPrice} = useSelector(state => state.shoppingCart)
    const navigation = useNavigation()

    const param = {
        'id': uuid.v4(),
        'stockStatus': true,
        'title': "Başlık 10",
        'color': "Mavi",
        'size': "S",
        'count': 1,
        'unitPrice': "30",
        'price': "30"
    }

    React.useEffect(() => {
        dispatch(allActions.addOrderAction(param))
    }, [])

    const onRefresh = () => {
    }

    const removeItemDialog = (Id) =>
        Alert.alert(
            "Silme İşlemi",
            "Seçili olan ürünü sepetinizden silmek istiyor musunuz?",
            [
                {
                    text: "İptal",
                    style: "cancel"
                },
                {
                    text: "Sil",
                    style: "destructive",
                    onPress: () => orderHideItemDispatch(Id)
                },

            ]
        );

    const clearItemsDialog = () =>
        Alert.alert(
            "Sepet Temizleme İşlemi",
            "Tüm ürünleriniz silinmek üzere,sepeti temizlemek istiyor musunuz?",
            [
                {
                    text: "İptal",
                    style: "cancel",
                },
                {
                    text: "Tümünü Sil",
                    style: "destructive",
                    onPress: () => dispatch(allActions.clearOrderAction())
                }
            ]
        );

    const orderHideItemDispatch = (Id) => {
        dispatch(allActions.removeOrderAction(Id))
        dispatch(allActions.totalPriceAction())
        LayoutAnimation.configureNext(layoutAnimConfig)
    }

    const orderItemsDispatch = (params, index) => {
        dispatch(allActions.updateOrderAction(params, index))
        dispatch(allActions.totalPriceAction())
    }


    const ListHeader = () => {
        return <View style={styles.headerContainer}>
            <Text style={styles.headertextcount}>Sepetinde {orders.length} Ürün Bulunuyor</Text>

            <TouchableOpacity onPress={() => clearItemsDialog()} style={styles.headerdelete}>
                <AntDesign name={"delete"} size={hp(15)} color={Colors.Primary}/>
                <Text style={styles.headertextdelete}>Sepeti Temizle</Text>
            </TouchableOpacity>
        </View>
    }

    const ListFooter = () => {
        return <View style={styles.footerContainer}>
            <Text style={styles.footertitle}>Ödenecek Tutar</Text>

            <View style={styles.footerrow}>
                <Text style={styles.footerpricetext}>Tutar</Text>
                <Text style={styles.footerprice}>{totalPrice} TL</Text>
            </View>

            <View style={styles.footerrow}>
                <Text style={styles.footerpricetext}>Kargo</Text>
                <Text style={styles.footerprice}>10 TL</Text>
            </View>

            <View style={styles.footerrow}>
                <Text style={styles.footertotaltext}>Toplam</Text>
                <Text style={styles.footertotal}>{parseInt(totalPrice + 10)} TL</Text>
            </View>

            <View style={{alignItems: "center"}}>
                <ButtonPrimary
                    onPress={() => navigation.navigate("PayDetails")}
                    text={"Sepeti Onayla"}
                    style={styles.btngo}
                    buttonColor={Colors.Primary}
                    iconVisible={false}
                />
            </View>
        </View>
    }

    return <View style={styles.container}>

        <SwipeListView
            style={styles.flatlist}
            data={orders}
            refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh}/>}
            renderItem={(item, index) => <OrderItem {...item} {...index}
                                                    onPress={(params, index) => orderItemsDispatch(params, index)}/>}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item.id}
            ListHeaderComponent={() => ListHeader()}
            ListFooterComponent={() => ListFooter()}
            renderHiddenItem={(data, rowMap) => (
                <OrderHideItems
                   // style={}
                    leftText={"Favorilere Ekle"}
                    rightText={"Sepetimden Kaldır"}
                    onChange={() => removeItemDialog(data.item.id)}/>
            )}
            leftOpenValue={90}
            rightOpenValue={-90}/>

        <LineBottom/>

    </View>
}

export default OrderDetails

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        justifyContent: "center",
        alignItems: "center",

    }, background: {
        position: "absolute",
    },
    flatlist: {},

    threeD: {
        marginTop: hp(35),
        width: wp(357),
        height: hp(426),
    },
    content: {
        flex: 1,
        alignItems: "center",
    },

    title: {
        fontSize: wp(26),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-Bold",
        marginTop: hp(32),
        marginBottom: hp(15),
    },
    subtitle: {
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(24),
        width: hp(284),
        textAlign: "center"
    },
    footer: {
        position: "absolute",
        bottom: 0,
    },
    tab1: {
        height: hp(37),
        width: wp(79),
    },
    headerContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: hp(30),
    },
    headerdelete: {
        position: "absolute",
        right: 0,
        flexDirection: "row",
        alignItems: "center",
    },
    headertextcount: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.PagerNormal,
        marginRight: wp(72),
    },
    headertextdelete: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.Error,

        marginLeft: wp(7),
    },
    footerContainer: {
        marginBottom: hp(30),
        marginTop: hp(30),
    },
    footertitle: {
        fontSize: wp(16),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        marginRight: wp(15),
        marginBottom: wp(12),

    },
    footerrow: {
        flexDirection: "row",
        marginBottom: wp(10),

    },
    footerpricetext: {
        fontSize: wp(13),
        fontWeight: "400",
        color: Colors.content_text,
    },
    footerprice: {
        flex: 1,
        fontSize: wp(13),
        fontWeight: "400",
        textAlign: "right",
        color: Colors.title_active,
    },
    footertotaltext: {
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
    },
    footertotal: {
        flex: 1,
        fontSize: wp(16),
        fontWeight: "400",
        textAlign: "right",
        fontFamily: "Gilroy-bold",
        color: Colors.Primary,
    }, btngo: {
        marginTop: hp(33),
        height: hp(48),
        width: wp(305),
    },


})

const layoutAnimConfig = {
    duration: 300,
    update: {
        type: LayoutAnimation.Types.easeInEaseOut,
    },
    delete: {
        duration: 100,
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
    },
};
