import React from "react";
import {StyleSheet, View, Text, TouchableOpacity, FlatList, ScrollView} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {LineBottom} from "../../components/lines/lineBottom";
import OrderItem from "../../components/items/orderItem";
import {dataAdress, dataCrediaCard} from "../../api/data"

import {useNavigation} from "@react-navigation/native"
import AddressItem from "../../components/items/addressItem";
import AntDesign from "react-native-vector-icons/AntDesign";
import CreditCard from "../../components/items/creditCard";
import {useSelector} from "react-redux";
import AddButtonNewItem from "../../components/views/addButtonNewItem";

const PayDetails = (props) => {

    const {orders, totalPrice} = useSelector(state => state.shoppingCart)

    const navigation = useNavigation()

    const [selectAddress, setselectAddress] = React.useState("1");
    const [selectBusinessAdress, setselectBusinessAdress] = React.useState("1");
    const [selectCreditCard, setselectCreditCard] = React.useState("1");

    const [isBusinessAddress, setisBusinessAddress] = React.useState(true);

    const handleAddress = (item) => {
        setselectAddress(item)
    }
    const handleBusines = (item) => {
        setselectBusinessAdress(item)
    }
    const handleCredit = (item) => {
        setselectCreditCard(item)
    }

    const AdressItems = ({item}) => {
        return <AddressItem
            item={item}
            select={selectAddress}
            onPress={handleAddress}
        />
    }
    const BusinessItems = ({item}) => {
        return <AddressItem
            item={item}
            select={selectBusinessAdress}
            onPress={handleBusines}
        />
    }
    const CreditCardItems = ({item}) => {
        return <CreditCard
            item={item}
            select={selectCreditCard}
            onPress={handleCredit}
        />
    }

    const ListFooter = () => {
        return <View style={styles.footerContainer}>
            <Text style={styles.footertitle}>Ödenecek Tutar</Text>

            <View style={styles.footerrow}>
                <Text style={styles.footerpricetext}>Tutar</Text>
                <Text style={styles.footerprice}>{totalPrice} TL</Text>
            </View>

            <View style={styles.footerrow}>
                <Text style={styles.footerpricetext}>Kargo</Text>
                <Text style={styles.footerprice}>10 TL</Text>
            </View>

            <View style={styles.footerrow}>
                <Text style={styles.footertotaltext}>Toplam</Text>
                <Text style={styles.footertotal}>{totalPrice + 10} TL</Text>
            </View>

            <View style={{alignItems: "center"}}>
                <ButtonPrimary
                    onPress={() => navigation.navigate("PayDetails")}
                    text={"Ödeme Yap"}
                    style={styles.btngo}
                    buttonColor={Colors.Primary}
                    iconVisible={false}
                />
            </View>
        </View>
    }


    return <>
        <ScrollView style={styles.container}>
            <View>

                <Text style={styles.title}>Teslimat Adresi</Text>
                <View>
                    <FlatList
                        style={styles.flatlist}
                        data={dataAdress}
                        numColumns={1}
                        horizontal={true}
                        extraData={selectAddress}
                        renderItem={AdressItems}
                        showsHorizontalScrollIndicator={false}
                        ListEmptyComponent={<AddButtonNewItem style={{marginLeft: wp(20)}}
                                                              redirect={"AddAddress"}
                                                              title={"Yeni Adres Tanımla"}/>}
                        keyExtractor={item => item.id}
                    />
                </View>

                <View>
                    <TouchableOpacity
                        onPress={() => setisBusinessAddress(!isBusinessAddress)}
                        style={styles.checkBusiness}>
                        <View style={[styles.checkitem, {
                            backgroundColor: isBusinessAddress ?
                                Colors.Primary : Colors.line_tab
                        }]}>
                            {isBusinessAddress && <AntDesign name={"check"} size={hp(10)} color={Colors.white}/>
                            }
                        </View>
                        <Text>Faturamı teslimat adresime gönder</Text>

                    </TouchableOpacity>
                </View>

                {!isBusinessAddress && <>
                    <Text style={styles.title}>Fatura Adresi</Text>
                    <View>
                        <FlatList
                            style={styles.flatlist}
                            data={dataAdress}
                            numColumns={1}
                            horizontal={true}
                            extraData={selectBusinessAdress}
                            renderItem={BusinessItems}
                            showsHorizontalScrollIndicator={false}
                            ListEmptyComponent={<AddButtonNewItem style={{marginLeft: wp(20)}}
                                                                  redirect={"AddAddress"}
                                                                  title={"Yeni Adres Tanımla"}/>}
                            keyExtractor={item => item.id}
                        />
                    </View>
                </>
                }

                <Text style={styles.title}>Ödeme Yönetimi</Text>

                <View>
                    <FlatList
                        style={styles.flatlist}
                        data={dataCrediaCard}
                        numColumns={1}
                        horizontal={true}
                        extraData={selectCreditCard}
                        renderItem={CreditCardItems}
                        showsHorizontalScrollIndicator={false}
                        ListEmptyComponent={<AddButtonNewItem style={{marginLeft: wp(20)}}
                                                              redirect={"AddAddress"}
                                                              title={"Yeni Kart Tanımla"}/>}
                        keyExtractor={item => item.id}
                    />
                </View>

                <ListFooter/>

            </View>

        </ScrollView>
        <LineBottom/>
    </>
}

export default PayDetails

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    },
    flatlist: {

        height: hp(172),

    },
    content: {
        flex: 1,
        alignItems: "center",
    },
    title: {
        fontSize: wp(16),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",

        marginTop: hp(22),
        marginLeft: wp(16),
    },
    checkBusiness: {
        flexDirection: "row",

        alignItems: "center",
        marginTop: hp(26),
        marginLeft: wp(25),
    },
    checkitem: {
        backgroundColor: Colors.Primary,
        borderRadius: hp(20),
        height: hp(17),
        width: hp(17),
        marginRight: wp(13),

        justifyContent: "center",
        alignItems: "center",
    },
    footerContainer: {
        marginBottom: hp(30),
        marginTop: hp(30),
        marginLeft: hp(20),
        marginRight: hp(20),
    },
    footertitle: {
        fontSize: wp(16),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        marginRight: wp(15),
        marginBottom: wp(12),

    },
    footerrow: {
        flexDirection: "row",
        marginBottom: wp(10),

    },
    footerpricetext: {
        fontSize: wp(13),
        fontWeight: "400",
        color: Colors.content_text,
    },
    footerprice: {
        flex: 1,
        fontSize: wp(13),
        fontWeight: "400",
        textAlign: "right",
        color: Colors.title_active,
    },
    footertotaltext: {
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
    },
    footertotal: {
        flex: 1,
        fontSize: wp(16),
        fontWeight: "400",
        textAlign: "right",
        fontFamily: "Gilroy-bold",
        color: Colors.Primary,
    }, btngo: {
        marginTop: hp(33),
        height: hp(48),
        width: wp(305),
    },


})
