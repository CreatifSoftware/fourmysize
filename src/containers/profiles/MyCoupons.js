import React from "react";
import {StyleSheet, View, Text, FlatList, RefreshControl} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {useDispatch, useSelector} from "react-redux";
import {dataMyCoupon} from "../../api/data";
import EmptyView from "../../components/views/emptyView";
import {getLocalizedString} from "../../utils/localization";
import {LOCALIZED_HOMEPAGE_BUTTON_1_TEXT, LOCALIZED_HOMEPAGE_BUTTON_2_TEXT} from "../../utils/constants";
import CouponItem from "../../components/items/couponItem";
import {useNavigation} from "@react-navigation/native";

const MyCoupons = (props) => {

    const dispatch = useDispatch();
    const navigation = useNavigation()

    const onRefresh = () => {

    }

    const ListEmptyComp = () => {
        return <View style={{alignItems: "center"}}>
            <EmptyView type={"coupon"}
                       title={"İndirim Kuponun Bulunmuyor!"}
                       subtitle={"Contrary to popular belief, Lorem Ipsum is not simply random text. " +
                       "It has roots in a piece of classical Latin literature from 45 BC"}/>

            <ButtonPrimary
                onPress={() => navigation.navigate("PayDetails")}
                text={"Alışverişe Devam Et"}
                style={styles.btngo}
                buttonColor={Colors.Secondary}
                iconVisible={false}
            />
        </View>
    }

    const ListHeader = () => {
        return <>
            <Text style={styles.title}>İndirim Kuponların
                Seni Bekliyor!</Text>
            <Text style={styles.subtitle}>4MySize dünyası sadece sana özel indirim fırsatları tanıyor. İndirim
                kuponlarının kullanım tarihini
                kaçırmadan alışverişlerinde kullanmayı sakın unutma.</Text>
        </>
    }

    return (
        <View style={styles.container}>


            <FlatList
                style={styles.flatlist}
                data={dataMyCoupon}
                renderItem={CouponItem}
                showsVerticalScrollIndicator={false}
                refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh}/>}
                ListEmptyComponent={ListEmptyComp}
                ListHeaderComponent={dataMyCoupon.length > 0 && ListHeader}
                keyExtractor={item => item.id}
            />


        </View>
    )
}

export default MyCoupons

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background
    },
    flatlist: {
        flexGrow: 1,
    },
    title: {
        fontSize: wp(21),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        lineHeight: hp(26),
        width: wp(178),
        marginTop: hp(22),
        marginLeft: wp(16),
    },
    subtitle: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(16),
        width: wp(338),
        marginTop: hp(10),
        marginLeft: wp(16),
    },
    btngo: {
        marginTop: hp(73),
        height: hp(48),
        width: wp(305),
    },
    image: {
        height: hp(266),
        width: wp(375),
        resizeMode: "contain",
    }
})
