import React from "react";
import {StyleSheet, View, Text, FlatList, RefreshControl, ScrollView, Alert, LayoutAnimation} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {useDispatch, useSelector} from "react-redux";
import EmptyView from "../../components/views/emptyView";
import {getLocalizedString} from "../../utils/localization";
import {LOCALIZED_HOMEPAGE_BUTTON_1_TEXT, LOCALIZED_HOMEPAGE_BUTTON_2_TEXT} from "../../utils/constants";
import CouponItem from "../../components/items/couponItem";
import {useNavigation} from "@react-navigation/native";
import {GroupTabs} from "../../components/tabs/tabs";
import MyTemplateItem from "../../components/items/myTemplateItem";
import {LineBottom} from "../../components/lines/lineBottom";
import OrderHideItems from "../../components/items/orderHideItems";
import {SwipeListView} from "react-native-swipe-list-view";
import allActions from "../../redux/action";
import uuid from "react-native-uuid";
import {threeDModelTshirt} from "../../assets/Images";


const MyTemplates = (props) => {

    const dispatch = useDispatch();
    const {myTemplates} = useSelector(state => state.design)
    const navigation = useNavigation()

    const [index, setIndex] = React.useState(0);


    const onRefresh = () => {

    }

    const groupTabsData = [
        {
            key: "0",
            text: "T-Shirt",
        },
        {
            key: "1",
            text: "Hoodie",
        },
        {
            key: "2",
            text: "Sweatshirt",
        },
    ];


    const ListEmptyComp = () => {
        return <View style={{alignItems: "center"}}>
            <EmptyView type={"coupon"}
                       title={"Kayıtlı Şablonun Bulunmuyor!"}
                       subtitle={"Contrary to popular belief, Lorem Ipsum is not simply random text. " +
                       "It has roots in a piece of classical Latin literature from 45 BC"}/>

            <ButtonPrimary
                onPress={() => navigation.navigate("PayDetails")}
                text={"Alışverişe Devam Et"}
                style={styles.btngo}
                buttonColor={Colors.Secondary}
                iconVisible={false}
            />
        </View>
    }

    const ListHeader = () => {
        return <>
            <View style={styles.header}>
                <ScrollView style={styles.grouptab} showsHorizontalScrollIndicator={false}
                            horizontal={true}>
                    <GroupTabs
                        selectTab={index}
                        callBackIndex={(index) => {
                            setIndex(index);
                        }}
                        values={groupTabsData}
                        color={Colors.Secondary}
                        fontSize={hp(11)}/>
                </ScrollView>
            </View>
        </>

    }

    const removeItemDialog = (Id) =>
        Alert.alert(
            "Silme İşlemi",
            "Seçili olan şablonu silmek istiyor musunuz?",
            [
                {
                    text: "İptal",
                    style: "cancel"
                },
                {
                    text: "Sil",
                    style: "destructive",
                    onPress: () => templateHideItemDispatch(Id)
                },
                {cancelable: true}
            ]
        );

    const templateHideItemDispatch = (Id) => {
        dispatch(allActions.removeTemplateAction(Id))
        LayoutAnimation.configureNext(layoutAnimConfig)
    }

    return (
        <View style={styles.container}>
            <SwipeListView
                style={styles.flatlist}
                data={myTemplates}
                renderItem={MyTemplateItem}
                showsVerticalScrollIndicator={false}
                refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh}/>}
                ListEmptyComponent={ListEmptyComp}
                ListHeaderComponent={myTemplates !== null && myTemplates.length > 0 && ListHeader}
                renderHiddenItem={(data, rowMap) => (
                    <OrderHideItems
                        rightText={"Şablonu Sil"}
                        onChange={() => removeItemDialog(data.item.id)}/>
                )}
                keyExtractor={item => item.id}
                rightOpenValue={-90}
            />
            <LineBottom/>
        </View>
    )
}

export default MyTemplates

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        alignItems: "center",
        flexDirection: "column",
    },
    header: {
        height: hp(60),
        alignItems: "center",
        justifyContent: "center",
        marginLeft: wp(15)
    },
    title: {
        fontSize: wp(21),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        lineHeight: hp(26),
        width: wp(178),
        marginTop: hp(22),
        marginLeft: wp(16),
    },
    subtitle: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(16),
        width: wp(338),
        marginTop: hp(10),
        marginLeft: wp(16),
    },
    btngo: {
        marginTop: hp(73),
        height: hp(48),
        width: wp(305),
    },
    image: {
        height: hp(266),
        width: wp(375),
        resizeMode: "contain",
    },
    grouptab: {
        marginTop: hp(20),
    },
})

const layoutAnimConfig = {
    duration: 300,
    update: {
        type: LayoutAnimation.Types.easeInEaseOut,
    },
    delete: {
        duration: 100,
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
    },
};
