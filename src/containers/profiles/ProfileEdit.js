import React from "react";
import {
    StyleSheet,
    View,
    ScrollView,
    KeyboardAvoidingView, Text
} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {LineBottom} from "../../components/lines/lineBottom";
import {dataCity, dataDistrict, dataNeighborhood} from "../../api/data"
import {useNavigation} from "@react-navigation/native"
import CardTextInput from "../../components/inputs/cardTextInput";
import {PreviousNextView} from "react-native-keyboard-manager";
import CardMultiTextInput from "../../components/inputs/cardMultiTextInput";
import CardPicker from "../../components/pickers/cardPicker";
import {dataAdress} from "../../api/data";
import {useDispatch, useSelector} from "react-redux";
import {profileAction} from "../../redux/action/profileAction";
import SuccessPopup from "../../views/popup/successPopup";

const ProfileEdit = (props) => {

    const dispatch = useDispatch();
    const {nameUserName, nick, email, gsm} = useSelector(state => state.profile.user);

    const [formname, setName] = React.useState(nameUserName)
    const [formnick, setNick] = React.useState(nick)
    const [formemail, setEmail] = React.useState(email)
    const [formgsm, setGsm] = React.useState(gsm)
    const [popup, setPopup] = React.useState(false)

    const params = {
        nameUserName: formname,
        nick: formnick,
        email: formemail,
        gsm: formgsm,
    }

    const handleChange = () => {
        setPopup(!popup)
        dispatch(profileAction(params))
    }


    return <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{marginTop: hp(32)}}>

                <SuccessPopup title={"Kullanıcı Bilgileriniz Başarılı Şekilde Düzenlendi"} isShow={popup}
                              onPress={(isShow) => setPopup(isShow)}/>

                <KeyboardAvoidingView behavior="padding">
                    <PreviousNextView>
                        <Text style={styles.title}>Profil Bilgilerin</Text>

                        <CardTextInput
                            onChangeText={(value) => setName(value)}
                            value={formname}
                            title={"İsim-Soyisim"}
                            placeHolder={"Arthur Alanso"}/>

                        <CardTextInput
                            onChangeText={(value) => setNick(value)}
                            value={formnick}
                            title={"Rumuz"}
                            placeHolder={"jmt"}/>

                        <CardTextInput
                            onChangeText={(value) => setEmail(value)}
                            value={formemail}
                            title={"E-Posta"}
                            placeHolder={"example@4mysize.com"}/>

                        <Text style={styles.title}>GSM No</Text>


                        <CardTextInput
                            onChangeText={(value) => setGsm(value)}
                            value={formgsm}
                            title={"GSM No"}
                            placeHolder={"0 545 xxx xx xx"}/>

                        <Text style={styles.title}>Parola Ayarların</Text>

                        <CardTextInput title={"Mevcut Parolan"}
                                       placeHolder={"*******"}/>

                        <CardTextInput title={"Yeni Parolan"}
                                       placeHolder={""}/>

                        <CardTextInput title={"Yeni Parola Tekrar"}
                                       placeHolder={""}/>


                        <View style={{alignItems: "center"}}>
                            <ButtonPrimary
                                onPress={handleChange}
                                text={"Bilgilerimi Güncelle"}
                                style={styles.btngo}
                                buttonColor={Colors.Secondary}
                                iconVisible={false}
                            />
                        </View>
                    </PreviousNextView>
                </KeyboardAvoidingView>
            </View>
        </ScrollView>
        <LineBottom/>
    </View>
}

export default ProfileEdit

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        alignItems: "center",

    },
    btngo: {
        marginTop: hp(33),
        marginBottom: hp(20),
        height: hp(48),
        width: wp(305),
    },
    title: {
        fontSize: wp(16),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",

        marginTop: hp(22),
        marginBottom: hp(20),

        marginLeft: wp(16),
    },

})
