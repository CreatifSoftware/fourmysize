import React from "react";
import {StyleSheet, View, Text, TouchableOpacity, FlatList, Image} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {LineBottom} from "../../components/lines/lineBottom";
import {dataAdress} from "../../api/data"
import {useNavigation} from "@react-navigation/native"
import HistoryOrderItem from "../../components/items/historyOrderItem";
import AddressItem from "../../components/items/addressItem";
import {iconAddtoCard} from "../../assets/Icon";
import * as RootNavigation from "../../navigation/RootNavigation"
import AddButtonNewItem from "../../components/views/addButtonNewItem";
import EmptyView from "../../components/views/emptyView";

const MyAddress = (props) => {

    const navigation = useNavigation()

    const [selectAddress, setselectAddress] = React.useState("1");

    const handleAddress = (item) => {
        setselectAddress(item)
    }

    const AdressItems = ({item}) => {
        return <AddressItem
            item={item}
            select={selectAddress}
            onPress={handleAddress}
        />
    }

    const ListFooter = () => {
        return <AddButtonNewItem redirect={"AddAddress"} title={"Yeni Adres Tanımla"}/>
    }

    return <>
        <View style={styles.container}>

            <FlatList
                style={styles.flatlist}
                data={dataAdress}
                renderItem={AdressItems}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={() => ListFooter()}
                ListFooterComponentStyle={{marginBottom: hp(37)}}
                ListEmptyComponent={<EmptyView
                    type={"address"}
                    title={"Kayıtlı Adresin Bulunmuyor"}
                    subtitle={"Contrary to popular belief, Lorem Ipsum is not simply random text. " +
                    "It has roots in a piece of classical Latin literature from 45 BC"}

                />}
                keyExtractor={item => item.id}
            />
        </View>
        <LineBottom/>
    </>
}

export default MyAddress

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        alignItems: "center",
    },
    btngo: {
        marginTop: hp(33),
        height: hp(48),
        width: wp(305),
    },


})
