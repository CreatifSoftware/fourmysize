import React from "react";
import {
    StyleSheet,
    View,
    ScrollView,
    KeyboardAvoidingView
} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {LineBottom} from "../../components/lines/lineBottom";
import {dataCity, dataDistrict, dataNeighborhood} from "../../api/data"
import {useNavigation} from "@react-navigation/native"
import CardTextInput from "../../components/inputs/cardTextInput";
import {PreviousNextView} from "react-native-keyboard-manager";
import CardMultiTextInput from "../../components/inputs/cardMultiTextInput";
import CardPicker from "../../components/pickers/cardPicker";
import {dataAdress} from "../../api/data";

const AddAddress = ({navigation, route}) => {


    const [form, setForm] = React.useState(dataAdress[route.params.id])

    const onHandleChange = () => {
        setForm({})
    }

    return <View style={styles.container}>
        <ScrollView>
            <View style={{marginTop: hp(32)}}>

                <KeyboardAvoidingView behavior="padding">
                    <PreviousNextView>
                        <CardTextInput
                            onChangeText={(value) => setForm({...form, title: value})}
                            title={"Adres Başlığı"} placeHolder={"Home"}
                            value={form && form.title}
                        />
                        <CardTextInput
                            onChangeText={(value) => setForm({...form, username: value})}
                            title={"İsim-Soyisim"}
                            value={form && form.username}
                            placeHolder={"Martin Touch"}/>
                        <CardPicker
                            onValueChange={value => setForm({...form, city: value})}
                            value={form && form.city}
                            title={"İl"} placeHolder={"İl Seçiniz"} data={dataCity}/>
                        <CardPicker
                            onValueChange={value => setForm({...form, district: value})}
                            value={form && form.district}
                            title={"İlçe"} placeHolder={"İlçe Seçiniz"} data={dataDistrict}/>
                        <CardPicker
                            onValueChange={value => setForm({...form, neighborhood: value})}
                            value={form && form.neighborhood}
                            title={"Mahalle"} placeHolder={"Mahalle Seçiniz"} data={dataNeighborhood}/>

                        <CardMultiTextInput
                            onChangeText={(value) => setForm({...form, adress: value})}
                            title={"Açık Adres"}
                            value={form && form.adress}
                            placeHolder={"Aras sk. No: 12"}/>


                        <View style={{alignItems: "center"}}>
                            <ButtonPrimary
                                onPress={() => navigation.navigate("PayDetails")}
                                text={form ? form.id && "Adresi Güncelle" : "Yeni Adres Tanımla"}
                                style={styles.btngo}
                                buttonColor={Colors.Secondary}
                                iconVisible={false}
                            />
                        </View>
                    </PreviousNextView>
                </KeyboardAvoidingView>
            </View>
        </ScrollView>
        <LineBottom/>
    </View>
}

export default AddAddress

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        alignItems: "center",

    },
    btngo: {
        marginTop: hp(33),
        height: hp(48),
        width: wp(305),
    },


})
