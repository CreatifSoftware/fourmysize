import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, ScrollView} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {profileImage} from "../../assets/Images";
import * as I from "../../assets/Icon";
import {LineBottom} from "../../components/lines/lineBottom";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import * as RootNavigation from "../../navigation/RootNavigation"


const MenuItem = (props) => {

    return <TouchableOpacity onPress={() => RootNavigation.navigate(props.redirect,
        {back: true})} style={styles.menuItem}>
        <Image style={styles.image} source={props.icon}/>
        <View>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.subtitle}>{props.subtitle}</Text>
        </View>
        <MaterialIcons style={styles.icon} name={"navigate-next"} size={25}
                       color={Colors.Primary}/>
    </TouchableOpacity>
}

const ProfileMain = (props) => {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.header}>
                    <Image style={styles.profileImage} source={profileImage}/>
                </View>

                <View style={styles.infoView}>
                    <Text style={styles.username}>Dina Akçay</Text>
                    <Text style={styles.status}>Online</Text>
                </View>

                <View style={styles.content}>

                    <MenuItem
                        redirect={"ProfileEdit"}
                        icon={I.iconInfo}
                        title={"Kullanıcı Bilgilerim"}
                        subtitle={"Profil bilgilerini, e-posta adresini, GSM ve parola bilgilerini güncelleyebilirsin"}
                    />

                    <MenuItem
                        redirect={"MyOrders"}
                        isBack={true}
                        icon={I.iconMyOrders}
                        title={"Siparişlerim"}
                        subtitle={"Tüm siparişlerini görebilir ve siparişlerinin takibini yapabilirsin."}
                    />

                    <MenuItem
                        redirect={"MyTemplates"}
                        icon={I.iconMyShema}
                        title={"Kayıtlı Şablonlarım"}
                        subtitle={"Tasarımlarını görebilir ve tasarımlarına kaldığın yerden devam edebilirsin."}
                    />

                    <MenuItem
                        redirect={"MyFavorites"}
                        icon={I.iconFavorite}
                        title={"Favorilerim"}
                        subtitle={"Favorilerine eklediğin ürünleri görebilir ve hemen sepetine ekleyebilirsin."}
                    />

                    <MenuItem
                        redirect={"MyAddress"}
                        icon={I.iconAddress}
                        title={"Adres Bilgilerim"}
                        subtitle={"Kayıtlı sipariş ve fatura adreslerini düzenleyebilir veya yeni adres tanımlayabilirsin."}
                    />

                    <MenuItem
                        redirect={""}
                        icon={I.iconWallet}
                        title={"Kayıtlı Kartlarım"}
                        subtitle={"Birden fazla kart tanımlayabilir ve dilediğin kartı alışverişlerinde kullanabilirsin."}
                    />

                    <MenuItem
                        redirect={"CouponPage"}
                        icon={I.iconTicket}
                        title={"İndirim Kuponlarım"}
                        subtitle={"Sadece sana özel tanımlanan indirim kuponlarının takibini yapabilirsin."}
                    />

                    <MenuItem
                        redirect={"NotificationSetting"}
                        icon={I.iconNoti}
                        title={"Bildirim Ayarların"}
                        subtitle={"Sadece sana özel tanımlanan indirim kuponlarının takibini yapabilirsin."}
                    />


                    <View style={{width: "100%", alignItems: "center",marginBottom:hp(30)}}>
                        <TouchableOpacity style={styles.exitView}>
                            <Ionicons name={"exit-outline"} size={20}
                                      color={Colors.content_text}/>
                            <Text style={styles.exitText}>Çıkış Yap</Text>

                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <LineBottom/>
        </View>
    )
}

export default ProfileMain

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
    },
    header: {
        zIndex: 100,
        height: hp(80),
        borderBottomLeftRadius: hp(15),
        borderBottomRightRadius: hp(15),
        backgroundColor: Colors.Primary,
        alignItems: "center",
    },
    profileImage: {
        bottom: hp(-21),
        borderRadius: hp(61),
        height: hp(112),
        width: hp(112),

    },
    infoView: {
        alignItems: "center",
        marginTop: hp(70),
    },
    content: {
        flex: 1,
        marginTop: hp(15),
    },
    username: {
        fontSize: wp(18),
        fontWeight: "700",
        fontFamily: "Gilroy-bold",
        color: Colors.title_active,
        marginBottom: hp(10),
    },
    status: {
        fontSize: wp(14),
        fontWeight: "400",
        color: Colors.Success,
    },
    menuItem: {
        width: wp(327),
        marginBottom: hp(27),
        marginLeft: wp(15),
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    title: {
        marginLeft: wp(10),
        fontSize: wp(17),
        fontWeight: "700",
        fontFamily: "Gilroy-bold",
        color: Colors.title_active,
    },
    subtitle: {
        width: wp(214),
        marginTop: hp(7),
        marginLeft: wp(10),
        fontSize: wp(11),
        fontWeight: "400",
        lineHeight: hp(11),
        color: Colors.content_text,
    },
    image: {
        height: hp(65),
        width: hp(58),
        resizeMode: "contain",
    },
    icon: {
        flex: 1,
        textAlign: "right",

    },
    exitView: {
        marginTop: hp(20),
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: Colors.line_tab,
        width: wp(97),
        height: hp(34),
        borderRadius: hp(10),
        justifyContent: "center",
    },
    exitText: {
        fontSize: wp(12),
        fontWeight: "400",
        marginLeft: wp(5),
        color: Colors.content_text,
    },

})
