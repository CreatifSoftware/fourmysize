import React from "react";
import {
    View,
    StyleSheet,
    Image,
    Text,
    StatusBar,
    TouchableOpacity,
    TouchableWithoutFeedback, Alert
} from "react-native";
import {Line} from "../../assets/Images";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";
import {FloatingTextInput} from "../../components/inputs/floatingTextInput";
import {PreviousNextView} from "react-native-keyboard-manager"
import {
    iconEposta,
    iconApple,
    iconFacebook,
    iconGmail
} from "../../assets/Icon";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {getLocalizedString} from "../../utils/localization";
import {
    LOCALIZED_FORGOT_BUTTON_GO,
    LOCALIZED_FORGOT_TITLE,
    LOCALIZED_LOGIN_EMAIL,
    LOCALIZED_LOGIN_BUTTON_REGISTER,
    LOCALIZED_LOGIN_TEXT_REGISTER,
    LOCALIZED_LOGIN_TEXT_SOCIAL, LOCALIZED_REGISTER_PHONE
} from "../../utils/constants";
import {callForgotOptCode} from "../../api/auth/authServices";
import {getErrorMessage, isSuccess} from "../../api/helpers";
import * as lng from "../../utils/constants";
import * as Icon from "../../assets/Icon";
import * as RootNavigation from "../../navigation/RootNavigation";
import {loadingAction} from "../../redux/action/masterAction";
import {useDispatch} from "react-redux";

const ForgotPassword = (props) => {

    const dispatch = useDispatch()

    const [phone, setPhone] = React.useState("");

    function phoneParam() {
        this.phone = phone.replace(/\s/g, '')
    }

    const forgotPassSend = () => {
        dispatch(loadingAction(true))
        callForgotOptCode(new phoneParam())
            .then(response => {
                dispatch(loadingAction(false))
                if (isSuccess(response)) {
                    RootNavigation.navigate("OptConfirm", {phone: phone})
                } else {
                    Alert.alert("Başarısız!", "Telefon Numaranız Geçersizdir");
                }
            })
            .catch(error => {
                dispatch(loadingAction(false))
                Alert.alert("Başarısız!", "Telefon Numaranız Geçersizdir");
            })
    }

    return <View style={styles.container}>
        <StatusBar hidden/>
        <Image style={styles.background} source={Line}/>


        <View style={styles.content}>

            <View style={styles.card}>

                <Text style={styles.title}>{getLocalizedString(LOCALIZED_FORGOT_TITLE)}</Text>

                <PreviousNextView>
                    <FloatingTextInput
                        autoFocus={true}
                        label={getLocalizedString(lng.LOCALIZED_REGISTER_PHONE)}
                        placeholder={"0 ___ ___ __ __"}
                        placeholderTextColor={Colors.Primary}
                        style={styles.eposta}
                        keyboardType='phone-pad'
                        iconSource={Icon.iconGSM}
                        type={"cel-phone"}
                        returnKeyType={"next"}
                        onChangeText={text => {
                            setPhone(text);
                        }}
                        value={phone}
                        error={""}
                    />

                </PreviousNextView>

                <View style={styles.btnview}>
                    <ButtonPrimary
                        onPress={forgotPassSend}
                        text={getLocalizedString(LOCALIZED_FORGOT_BUTTON_GO)}
                        style={styles.btngo}
                        buttonColor={Colors.Primary}
                        iconVisible={true}
                    />
                </View>

                <Text style={styles.accounttitle}>
                    {getLocalizedString(LOCALIZED_LOGIN_TEXT_REGISTER)} <TouchableWithoutFeedback
                    onPress={() => props.navigation.navigate("Register")}>
                    <Text
                        style={styles.account}>{getLocalizedString(LOCALIZED_LOGIN_BUTTON_REGISTER)}</Text>
                </TouchableWithoutFeedback>
                </Text>
            </View>

            <View style={styles.footer}>

                <Text style={styles.socialview}>{getLocalizedString(LOCALIZED_LOGIN_TEXT_SOCIAL)}</Text>

                <View style={{flexDirection: "row", marginTop: hp(10)}}>
                    <TouchableOpacity>
                        <Image source={iconFacebook}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={iconGmail}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={iconApple}/>
                    </TouchableOpacity>
                </View>

            </View>

        </View>
    </View>;
};

export default ForgotPassword;


const styles = StyleSheet.create({
    container: {
        flex: 1,
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    content: {
        marginTop: hp(230),
        flex: 1,
    },
    logoHorizontal: {
        marginTop: hp(10),
    },
    icon: {
        marginTop: hp(20),
        marginLeft: wp(20),
    },
    logo: {
        position: "absolute",
        width: "100%",
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    card: {

        borderRadius: hp(50),
        backgroundColor: "white",
    },
    footer: {
        position: "absolute",
        bottom: 0,
        height: hp(235),
        width: wp(375),
        backgroundColor: Colors.Secondary,
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        height: hp(60),
        width: wp(176),
        marginTop: hp(35),
        marginLeft: wp(35),
        fontSize: wp(24),
        fontWeight: "bold",
        fontFamily: "Gilroy-Bold"
    },

    eposta: {
        color: Colors.title_active,
    },
    btnview: {
        marginTop: hp(23),
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    btngo: {
        height: hp(50),
        width: wp(250),
    },
    accounttitle: {
        textAlign: "center",
        marginTop: hp(20),
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.login_title,
        marginBottom: hp(23),
    },
    account: {
        fontSize: wp(12),
        fontWeight: "800",
        textDecorationLine: 'underline',
        color: Colors.Secondary,
    },
    socialview: {
        marginTop: hp(90),
        color: Colors.white,
        fontWeight: "600",
        fontSize: wp(12)
    }

});
