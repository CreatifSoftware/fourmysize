import React from "react";
import {
    View, StyleSheet, Image, Text, StatusBar,
    TouchableWithoutFeedback, KeyboardAvoidingView, TouchableOpacity, Alert
} from "react-native";
import {Line} from "../../assets/Images";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";

import {FloatingTextInput} from "../../components/inputs/floatingTextInput";
import {PreviousNextView} from "react-native-keyboard-manager"
import {
    iconEposta,
    iconPassword,
    iconPassShow,
    iconQuestion,
    iconApple,
    iconFacebook,
    iconGmail,
    iconPassHide
} from "../../assets/Icon";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {callLoginWithEmail} from "../../api/auth/authServices";
import {deviceId, deviceIp, deviceName} from "../../utils/deviceInfo";
import {getLocalizedString} from "../../utils/localization";
import {getErrorMessage, isSuccess} from "../../api/helpers";
import * as lng from "../../utils/constants";
import {connect, useDispatch, useSelector} from "react-redux";
import allActions from "../../redux/action";
import {
    LoginButton,
    AccessToken,
    GraphRequest,
    GraphRequestManager,
} from 'react-native-fbsdk';
import facebook from "../../authentication/facebook";
import google from "../../authentication/google";
//import {onApple, onAppleButtonPress} from "../../authentication/apple";

import {errorAction, loadingAction, signOutAction} from "../../redux/action/masterAction";
import {authAction} from "../../redux/action/authAction";
import {CommonActions} from "@react-navigation/native";

const Login = (props) => {

    const dispatch = useDispatch()
    const {customerId, refreshToken} = useSelector(state => state.auth)


    const [hide, setHide] = React.useState(true);
    const [userForm, setUserFrom] = React.useState({email: "test3", password: "123"});
    const {email, password} = userForm;
    const [devicename, setDevicename] = React.useState();
    const [deviceip, setDeviceIp] = React.useState();
    const [credentialStateForUser, updateCredentialStateForUser] = React.useState(-1);

    const [emailError, setEmailError] = React.useState("");
    const [passwordError, setPasswordError] = React.useState("");

    React.useEffect(() => {
        deviceName().then(name => setDevicename(name))
        deviceIp().then(name => setDeviceIp(name))
    }, [devicename, deviceip])

    const goBack = () => {
        props.navigation.goBack();
    }

    const RegisterRedirect = () => {
        props.navigation.navigate("Register")
    }

    const ForgotPassRedirect = () => {
        props.navigation.navigate("ForgotPassword")
    }

    function params() {
        this.email = email;
        this.password = password;
        this.customerId = customerId;
        this.refreshToken = refreshToken;
    }

    const getErrorMessageInput = (response) => {
        setEmailError("");
        setPasswordError("")

        switch (response.data.message) {
            case "Command.Customer.NotFound" :
                return setEmailError("Bu mail adresi bulunamadı")
            case "Command.Customer.InvalidCredentials" :
                return setPasswordError("Şifreniz geçersizdir")
            default : return ""
        }
    }

    const authLoginSend = () => {
        dispatch(loadingAction(true))

        callLoginWithEmail(new params())
            .then(response => {
                dispatch(loadingAction(false))
                if (isSuccess(response)) {
                    dispatch(props.onUpdateAuth(response.data.data))
                    dispatch(signOutAction(false))
                } else {
                   // getErrorMessageInput(response)
                      Alert.alert("Warning!", getErrorMessage(response))
                }
            })
            .catch(error => {
                dispatch(errorAction(false))
                Alert.alert("Error!", error)
            })
    }

    const IndividualClick = () => {

    }


    return <View style={styles.container}>
        <StatusBar hidden/>
        <Image style={styles.background} source={Line}/>

        {/*console.log("refreshToken: ",refreshToken)*/}

        <View style={styles.content}>


            <View style={styles.card}>

                <Text style={styles.title}>{getLocalizedString(lng.LOCALIZED_LOGIN_TITLE)}</Text>
                <Text style={styles.subtitle}>{getLocalizedString(lng.LOCALIZED_LOGIN_SUB_TITLE)}</Text>
                <KeyboardAvoidingView behavior="padding">
                    <PreviousNextView>
                        <FloatingTextInput
                            autoFocus={true}
                            label={getLocalizedString((lng.LOCALIZED_LOGIN_EMAIL))}
                            placeholder={getLocalizedString((lng.LOCALIZED_LOGIN_EMAIL))}
                            placeholderTextColor={Colors.Primary}
                            style={styles.eposta}
                            keyboardType='email-address'
                            iconSource={iconEposta}
                            onChangeText={text => setUserFrom({
                                password, email: text
                            })}
                            value={email}
                            error={emailError}
                        />

                        <FloatingTextInput

                            label={getLocalizedString(lng.LOCALIZED_LOGIN_PASSWORD)}
                            placeholder={getLocalizedString(lng.LOCALIZED_LOGIN_PASSWORD)}
                            placeholderTextColor={Colors.Primary}
                            style={styles.eposta}
                            keyboardType='email-address'
                            iconSource={iconPassword}
                            iconPassHideOnPress={() => setHide(!hide)}
                            iconPassHide={hide ? iconPassShow : iconPassHide}
                            iconQuestion={iconQuestion}
                            iconForgotOnPress={ForgotPassRedirect}
                            onChangeText={text => setUserFrom({
                                password: text, email
                            })}
                            value={password}
                            secureTextEntry={hide}
                            error={passwordError}
                        />
                    </PreviousNextView>
                </KeyboardAvoidingView>

                <View style={styles.btnview}>
                    <ButtonPrimary
                        onPress={authLoginSend}
                        text={getLocalizedString(lng.LOCALIZED_LOGIN_BUTTON_GO)}
                        style={styles.btngo}
                        buttonColor={Colors.Primary}
                        iconVisible={true}
                    />
                </View>

                <Text style={styles.accounttitle}>{getLocalizedString(lng.LOCALIZED_LOGIN_TEXT_REGISTER)}{" "}
                    <TouchableWithoutFeedback
                        onPress={RegisterRedirect}><Text
                        style={styles.account}>{getLocalizedString(lng.LOCALIZED_LOGIN_BUTTON_REGISTER)}</Text></TouchableWithoutFeedback>
                </Text>
            </View>

            <View style={styles.footer}>

                <Text style={styles.socialview}>{getLocalizedString(lng.LOCALIZED_LOGIN_TEXT_SOCIAL)}</Text>

                <View style={{flexDirection: "row", marginTop: hp(10)}}>


                    <TouchableOpacity onPress={facebook.onFacebookButtonPress}>
                        <Image source={iconFacebook}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={google.onGoogleButtonPress}>
                        <Image source={iconGmail}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onApple(1)}>
                        <Image source={iconApple}/>
                    </TouchableOpacity>
                </View>

            </View>

        </View>
    </View>;
};

const mapStateToProps = (state) => {
    return state;
}

const mapDispatchToProps = {
    onUpdateAuth: allActions.authAction,
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);


const styles = StyleSheet.create({
    container: {
        flex: 1,
    }, background: {
        width: wp(413),
        position: "absolute",
    }, header: {

        flexDirection: "row",
    },
    content: {
        marginTop: hp(150),
        flex: 1,
    },
    logoHorizontal: {
        marginTop: hp(10),
    },
    icon: {
        marginTop: hp(20),
        marginLeft: wp(20),
    },
    logo: {
        position: "absolute",
        width: "100%",
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    card: {

        borderRadius: hp(50),
        backgroundColor: "white",
    },
    footer: {
        position: "absolute",
        bottom: 0,
        height: hp(235),
        width: wp(375),
        backgroundColor: Colors.Secondary,
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        width: wp(179),
        marginTop: hp(35),
        marginLeft: wp(35),
        fontSize: wp(20),
        fontWeight: "400",
    },
    subtitle: {
        marginTop: hp(5),
        marginLeft: wp(35),
        fontSize: wp(24),
        fontWeight: "bold",
        fontFamily: "Gilroy-Bold"
    },
    eposta: {
        color: Colors.title_active,
    },
    btnview: {
        marginTop: hp(23),
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    btngo: {
        height: hp(50),
        width: wp(230),
    },
    accounttitle: {
        textAlign: "center",
        marginTop: hp(20),
        fontSize: wp(12),
        fontWeight: "400",
        marginBottom: hp(15),
        color: Colors.login_title
    },
    account: {
        fontSize: wp(12),
        fontWeight: "800",
        textDecorationLine: 'underline',
        color: Colors.Secondary,
    },
    socialview: {
        marginTop: hp(70),
        color: Colors.white,
        fontWeight: "600",
        fontSize: wp(12)
    }


});
