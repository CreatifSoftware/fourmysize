import React from 'react'
import {View} from 'react-native'
import HomePage from "../HomePage";
import {Colors} from "../../styles/Colors";
import {CommonActions} from '@react-navigation/native';
import {useSelector} from "react-redux";

const Splash = (props) => {

    const {signOut} = useSelector(state => state.master)

    React.useEffect(() => {
        setTimeout(() => {

            if (signOut === undefined ? false : !signOut) {
                props.navigation.dispatch(CommonActions.reset(
                    {
                        index: 0,
                        routes: [
                            {name: 'HomePage'}
                        ]
                    }));
            } else {
                //alert(!!signOut === false ? false : !signOut)
                props.navigation.dispatch(CommonActions.reset(
                    {
                        index: 1,
                        routes: [
                            {name: 'WelcomePage'}
                        ]
                    }));
            }
        }, 200)
    }, [signOut])

    return (<View style={{flex: 1, backgroundColor: Colors.Primary}}/>)
}

export default Splash
