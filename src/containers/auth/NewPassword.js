import React from "react";
import {
    View,
    StyleSheet,
    Image,
    Text,
    StatusBar,
    TouchableOpacity,
    TouchableWithoutFeedback, Alert
} from "react-native";
import {Line} from "../../assets/Images";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";
import {FloatingTextInput} from "../../components/inputs/floatingTextInput";
import {PreviousNextView} from "react-native-keyboard-manager"
import {
    iconEposta,
    iconApple,
    iconFacebook,
    iconGmail, iconPassword, iconPassShow, iconPassHide, iconQuestion
} from "../../assets/Icon";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {getLocalizedString} from "../../utils/localization";
import {
    LOCALIZED_FORGOT_BUTTON_GO,
    LOCALIZED_FORGOT_TITLE,
    LOCALIZED_LOGIN_EMAIL,
    LOCALIZED_LOGIN_BUTTON_REGISTER,
    LOCALIZED_LOGIN_TEXT_REGISTER,
    LOCALIZED_LOGIN_TEXT_SOCIAL, LOCALIZED_REGISTER_PHONE
} from "../../utils/constants";
import {callResetPassword} from "../../api/auth/authServices";
import {getErrorMessage, isSuccess} from "../../api/helpers";
import * as lng from "../../utils/constants";
import * as Icon from "../../assets/Icon";
import * as RootNavigation from "../../navigation/RootNavigation";

const NewPassword = ({navigation, route}) => {

    const [hide, setHide] = React.useState(true);
    const [hide2, setHide2] = React.useState(true);

    const [password, setPassword] = React.useState("");
    const [passwordTwo, setPasswordTwo] = React.useState("");

    function params() {
        this.token = route.params.token
        this.newPassword = password
    }

    const handleConfirm = () => {
        callResetPassword(new params())
            .then(response => {
                if (isSuccess(response)) {
                    RootNavigation.reset("Login", {})
                } else {
                    Alert.alert("Başarısız!", "Telefon Numaranız Geçersizdir");
                }
            })
            .catch(error => {
                Alert.alert("Başarısız!", "Telefon Numaranız Geçersizdir");
            })
    }

    return <View style={styles.container}>
        <StatusBar hidden/>
        <Image style={styles.background} source={Line}/>


        <View style={styles.content}>

            <View style={styles.card}>

                <Text style={styles.title}>Parola Değiştirme</Text>

                <PreviousNextView>
                    <FloatingTextInput
                        autoFocus={true}
                        label={getLocalizedString(lng.LOCALIZED_LOGIN_PASSWORD)}
                        placeholder={getLocalizedString(lng.LOCALIZED_LOGIN_PASSWORD)}
                        placeholderTextColor={Colors.Primary}
                        style={styles.eposta}
                        keyboardType='email-address'
                        iconSource={iconPassword}
                        iconPassHideOnPress={() => setHide(!hide)}
                        iconPassHide={hide ? iconPassShow : iconPassHide}
                        onChangeText={text => setPassword(text)}
                        value={password}
                        secureTextEntry={hide}
                        error={""}
                    />

                    <FloatingTextInput
                        autoFocus={true}
                        label={"Parola Tekrar"}
                        placeholder={"Parola Tekrar"}
                        placeholderTextColor={Colors.Primary}
                        style={styles.eposta}
                        keyboardType='email-address'
                        iconSource={iconPassword}
                        iconPassHideOnPress={() => setHide2(!hide2)}
                        iconPassHide={hide2 ? iconPassShow : iconPassHide}
                        onChangeText={text => setPasswordTwo(text)}
                        value={passwordTwo}
                        secureTextEntry={hide2}
                        error={""}
                    />

                </PreviousNextView>

                <View style={styles.btnview}>
                    <ButtonPrimary
                        onPress={handleConfirm}
                        text={"Şifreni Değiştir"}
                        style={styles.btngo}
                        buttonColor={Colors.Primary}
                        iconVisible={true}
                    />
                </View>


            </View>

            <View style={styles.footer}/>

        </View>
    </View>;
};

export default NewPassword;


const styles = StyleSheet.create({
    container: {
        flex: 1,
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    content: {
        marginTop: hp(230),
        flex: 1,
    },
    logoHorizontal: {
        marginTop: hp(10),
    },
    icon: {
        marginTop: hp(20),
        marginLeft: wp(20),
    },
    logo: {
        position: "absolute",
        width: "100%",
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    card: {

        borderRadius: hp(50),
        backgroundColor: "white",
    },
    footer: {
        position: "absolute",
        bottom: 0,
        height: hp(235),
        width: wp(375),
        backgroundColor: Colors.Secondary,
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        height: hp(60),
        width: wp(176),
        marginTop: hp(35),
        marginLeft: wp(35),
        fontSize: wp(24),
        fontWeight: "bold",
        fontFamily: "Gilroy-Bold"
    },

    eposta: {
        color: Colors.title_active,
    },
    btnview: {
        marginTop: hp(23),
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    btngo: {
        height: hp(50),
        width: wp(250),
        marginBottom: hp(20),
    },
    accounttitle: {
        textAlign: "center",
        marginTop: hp(20),
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.login_title,
        marginBottom: hp(23),
    },
    account: {
        fontSize: wp(12),
        fontWeight: "800",
        textDecorationLine: 'underline',
        color: Colors.Secondary,
    },
    socialview: {
        marginTop: hp(90),
        color: Colors.white,
        fontWeight: "600",
        fontSize: wp(12)
    }

});
