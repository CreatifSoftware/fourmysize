import React from "react";
import {
    View, StyleSheet, Image, Text,
    StatusBar, TouchableWithoutFeedback, Alert
} from "react-native";
import {Line} from "../../assets/Images";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";
import {FloatingTextInput} from "../../components/inputs/floatingTextInput";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {
    callForgotOptCode,
    callForgotOptCodeConfirm,
    callRegister, callRegisterOptCode, callRegisterOptCodeConfirm,
} from "../../api/auth/authServices";
import {getErrorMessage, isSuccess} from "../../api/helpers";
import CountDown from "react-native-countdown-component";
import * as RootNavigation from "../../navigation/RootNavigation";
import {loadingAction} from "../../redux/action/masterAction";
import {useDispatch} from "react-redux";
import {getLocalizedString} from "../../utils/localization";
import * as lng from "../../utils/constants";
import * as Icon from "../../assets/Icon";

const OtpConfirm = ({navigation, route}) => {

    const dispatch = useDispatch()

    const [code, setCode] = React.useState("");
    const [disabled, setdisabled] = React.useState(false);
    const [count, setCount] = React.useState(120);

    const onFinishCountDown = () => {
        setdisabled(!disabled)
    }

    function phone() {
        this.phone = route.params.phone.replace(/\s/g, '')
        this.otpCode = code.replace(/\s/g, '')
    }

    const againOPTCodeSend = () => {
        if (route.params.user) {
            callRegisterOptCode(new phone())
                .then(response => {
                    setdisabled(!disabled)
                    setCount(count + 1)
                })
                .catch(error => {
                    Alert.alert("Başarısız!", error);
                })
        } else {
            callForgotOptCode(new phone())
                .then(response => {
                    setdisabled(!disabled)
                    setCount(count + 1)
                })
                .catch(error => {
                    Alert.alert("Başarısız!", error);
                })
        }
    }

    const newPasswordPage = () => {
        dispatch(loadingAction(true))
        callForgotOptCodeConfirm(new phone())
            .then(response => {
                dispatch(loadingAction(false))
                if (isSuccess(response)) {
                    RootNavigation.navigate("NewPassword", {token: response.data.data.token})
                    //response.data.data.token
                } else {
                    Alert.alert("Geçersiz!", "Girilen Kod Geçersizdir.")
                }
            })
            .catch(error => {
                dispatch(loadingAction(false))
                alert(error + "Error")
            })
    }

    const homePage = () => {
        callRegister(route.params.user)
            .then(response => {
                console.log(response.data)
                if (isSuccess(response)) {
                    RootNavigation.reset("HomePage", {})
                } else {
                    alert(getErrorMessage(response))
                }
            })
            .catch(error => {
                alert(error )
            })
    }

    const handleSend = () => {
        if (route.params.user) {
           dispatch(loadingAction(true))
            callRegisterOptCodeConfirm(new phone())
                .then(response => {
                    dispatch(loadingAction(false))
                    if (isSuccess(response)) {
                        homePage()
                    } else {
                        Alert.alert("Geçersiz!", "Girilen Kod Geçersizdir.")
                    }
                })
                .catch(error => {
                    dispatch(loadingAction(false))
                    alert(error + "Error")
                })
        } else {
            newPasswordPage()
        }
    }

    return <View style={styles.container}>
        <StatusBar hidden/>
        <Image style={styles.background} source={Line}/>

        <View style={styles.content}>
            <View style={styles.card}>
                <Text style={styles.title}>Numara Doğrulaması</Text>

                <FloatingTextInput
                    autoFocus={true}
                    label={"Kodunuz"}
                    placeholder={"_ _ _ _ _ _"}
                    placeholderTextColor={Colors.Primary}
                    style={styles.eposta}
                    keyboardType='phone-pad'
                    iconSource={Icon.iconGSM}
                    type={"cel-phone"}
                    returnKeyType={"next"}
                    onChangeText={text => setCode(text)}
                    value={code}
                    maxLength={10}
                    error={""}
                    options={{
                        maskType: 'BRL',
                        withDDD: true,
                        dddMask: '9 9 9 9 9 9'
                    }}
                />

                <CountDown
                    until={count}
                    size={hp(18)}
                    onFinish={onFinishCountDown}

                    digitStyle={{backgroundColor: Colors.white}}
                    digitTxtStyle={{color: disabled ? Colors.PlaceHolder : Colors.Primary}}
                    timeToShow={['M', 'S']}
                    timeLabels={{m: null, s: null}}
                    showSeparator
                    separatorStyle={disabled ? styles.seperatorDisabled : styles.seperator}
                />

                <View style={styles.btnview}>
                    <ButtonPrimary
                        onPress={handleSend}
                        text={"Kodu Gönder"}
                        style={styles.btngo}
                        buttonColor={Colors.Primary}
                        iconVisible={true}
                    />
                </View>

                {disabled &&
                <Text style={styles.accounttitle}>
                    Kodunuzun süresi doldu <TouchableWithoutFeedback
                    onPress={againOPTCodeSend}>
                    <Text
                        style={styles.account}>Tekrar Gönder</Text>
                </TouchableWithoutFeedback>
                </Text>
                }

            </View>
            <View style={styles.footer}/>
        </View>
    </View>;
};

export default OtpConfirm;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    content: {
        marginTop: hp(230),
        flex: 1,
    },
    logoHorizontal: {
        marginTop: hp(10),
    },
    icon: {
        marginTop: hp(20),
        marginLeft: wp(20),
    },
    logo: {
        position: "absolute",
        width: "100%",
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    card: {
        borderRadius: hp(50),
        backgroundColor: "white",
    },
    footer: {
        position: "absolute",
        bottom: 0,
        height: hp(235),
        width: wp(375),
        backgroundColor: Colors.Secondary,
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        height: hp(60),
        width: wp(176),
        marginTop: hp(35),
        marginLeft: wp(35),
        fontSize: wp(24),
        fontWeight: "bold",
        fontFamily: "Gilroy-Bold"
    },
    eposta: {
        color: Colors.title_active,
    },
    btnview: {
        marginTop: hp(23),
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    btngo: {
        height: hp(50),
        width: wp(230),
        marginBottom: hp(20),
    },
    accounttitle: {
        textAlign: "center",

        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.login_title,
        marginBottom: hp(23),
    },
    account: {
        fontSize: wp(12),
        fontWeight: "800",
        textDecorationLine: 'underline',
        color: Colors.Secondary,
    },
    socialview: {
        marginTop: hp(90),
        color: Colors.white,
        fontWeight: "600",
        fontSize: wp(12)
    },
    seperator: {
        marginTop: hp(-5),
        color: Colors.Primary
    },
    seperatorDisabled: {
        marginTop: hp(-5),
        color: Colors.PlaceHolder
    }
});
