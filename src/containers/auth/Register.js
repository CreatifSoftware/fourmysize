import React from "react";
import {
    View,
    StyleSheet,
    ScrollView,
    Image,
    Text,
    StatusBar,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    TouchableOpacity, Alert
} from "react-native";
import {Line, LogoHorizontal} from "../../assets/Images";
import AntDesign from "react-native-vector-icons/AntDesign";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";
import {FloatingTextInput} from "../../components/inputs/floatingTextInput";
import {PreviousNextView} from "react-native-keyboard-manager"
import * as Icon from "../../assets/Icon";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {Checked} from "../../components/checkBoxes/checked";
import KeyboardSpacer from "react-native-keyboard-spacer";
import * as lng from "../../utils/constants";
import {getLocalizedString} from "../../utils/localization";
import {callRegisterOptCode} from "../../api/auth/authServices";
import {deviceId} from "../../utils/deviceInfo";
import {getErrorMessage, isSuccess} from "../../api/helpers";
import {connect, useDispatch, useSelector} from "react-redux";
import * as RootNavigation from "../../navigation/RootNavigation"
import {loadingAction} from "../../redux/action/masterAction";

const Register = (props) => {

    const dispatch = useDispatch()
    const {customerId, refreshToken, token} = useSelector(state => state.auth)

    const [phone, setPhone] = React.useState("");
    const [checked, setchecked] = React.useState(false);
    const [passhide, setpassHide] = React.useState(true);
    const [userForm, setUserFrom] = React.useState({username: "", email: "", phonenumber: "", password: ""});
    const {username, email, phonenumber, password} = userForm;

    const goBack = () => {
        props.navigation.goBack();
    }

    const LoginRedirect = () => {
        props.navigation.navigate("Login")
    }

    function params() {
        this.customerId = customerId;
        this.email = email;
        this.phone = phonenumber.replace(/\s/g, '');
        this.password = password;
        this.firstName = username;
        this.lastName = username;
        this.languageId = 0;
        this.registrationSource = 1;
        this.token = token;

    }

    const Send = () => {

        dispatch(loadingAction(true))
        callRegisterOptCode(new params())
            .then(response => {
                dispatch(loadingAction(false))
                RootNavigation.navigate("OptConfirm", {
                    user: new params(),
                    phone: new params().phone
                })
            })
            .catch(error => {
                dispatch(loadingAction(false))
                Alert.alert("Başarısız!", error);
            })
    }

    return (
        <View style={styles.container}>
            <StatusBar hidden/>
            <Image style={styles.background} source={Line}/>
            <View style={styles.content}>

                <View style={styles.card}>

                    <Text style={styles.title}>{getLocalizedString(lng.LOCALIZED_REGISTER_TITLE)}</Text>
                    <Text style={styles.subtitle}>{getLocalizedString(lng.LOCALIZED_REGISTER_SUB_TITLE)}</Text>


                    <PreviousNextView>
                        <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'height' : undefined}>
                            <FloatingTextInput
                                autoFocus={true}
                                label={getLocalizedString(lng.LOCALIZED_REGISTER_USERNAME)}
                                placeholder={getLocalizedString(lng.LOCALIZED_REGISTER_USERNAME)}
                                placeholderTextColor={Colors.Primary}
                                style={styles.eposta}
                                keyboardType='default'
                                iconSource={Icon.iconProfile}
                                onChangeText={text => setUserFrom({
                                    username: text,
                                    password, phonenumber, email
                                })}
                                value={username}
                                error={""}
                            />

                            <FloatingTextInput
                                autoFocus={true}
                                label={getLocalizedString(lng.LOCALIZED_REGISTER_EMAIL)}
                                placeholder={getLocalizedString(lng.LOCALIZED_REGISTER_EMAIL)}
                                placeholderTextColor={Colors.Primary}
                                style={styles.eposta}
                                keyboardType='email-address'
                                iconSource={Icon.iconEposta}
                                onChangeText={text => setUserFrom({
                                    email: text,
                                    password, phonenumber, username
                                })}
                                value={email}
                                error={""}
                            />

                            <FloatingTextInput
                                autoFocus={true}
                                label={getLocalizedString(lng.LOCALIZED_REGISTER_PHONE)}
                                placeholder={"0 ___ ___ __ __"}
                                placeholderTextColor={Colors.Primary}
                                style={styles.eposta}
                                keyboardType='phone-pad'
                                iconSource={Icon.iconGSM}
                                type={"cel-phone"}
                                returnKeyType={"next"}
                                onChangeText={text => {
                                    setPhone(text);
                                    setUserFrom({
                                        phonenumber: text,
                                        password, username, email
                                    })
                                }}
                                value={phonenumber}
                                error={""}
                            />


                            <FloatingTextInput
                                autoFocus={true}
                                label={getLocalizedString(lng.LOCALIZED_REGISTER_PASS)}
                                placeholder={getLocalizedString(lng.LOCALIZED_REGISTER_PASS)}
                                placeholderTextColor={Colors.Primary}
                                style={styles.eposta}
                                keyboardType='default'
                                iconSource={Icon.iconPassword}
                                iconPassHideOnPress={() => setpassHide(!passhide)}
                                iconPassHide={passhide ? Icon.iconPassShow : Icon.iconPassHide}

                                onChangeText={text => setUserFrom({
                                    password: text,
                                    username, phonenumber, email
                                })}
                                secureTextEntry={passhide}
                                value={password}
                                error={""}
                            />
                        </KeyboardAvoidingView>


                    </PreviousNextView>

                    <View style={styles.contract}>
                        <TouchableOpacity onPress={() => setchecked(!checked)}>
                            <Checked isChecked={checked}></Checked>
                        </TouchableOpacity>
                        <Text style={styles.conttext}>
                            {getLocalizedString(lng.LOCALIZED_REGISTER_CONTRACT)}

                            <Text style={styles.subconttext}> </Text>

                            <Text style={styles.subconttext}></Text></Text>
                    </View>

                    <View style={styles.btnview}>
                        <ButtonPrimary
                            onPress={Send}
                            text={getLocalizedString(lng.LOCALIZED_REGISTER_BUTTON_GO)}
                            style={styles.btngo}
                            buttonColor={Colors.Primary}
                            iconVisible={true}
                        ></ButtonPrimary>
                    </View>


                </View>

                <View style={styles.footer}>

                    <Text style={styles.accounttitle}>{getLocalizedString(lng.LOCALIZED_REGISTER_TEXT_REGISTER)}
                        <TouchableWithoutFeedback
                            onPress={LoginRedirect}><Text
                            style={styles.account}> {getLocalizedString(lng.LOCALIZED_REGISTER_BUTTON_REGISTER)}</Text></TouchableWithoutFeedback>
                    </Text>
                </View>

            </View>
        </View>)

};

const mapStateToProps = (state) => {
    return state;
}

export default Register;


const styles = StyleSheet.create({
    container: {
        flex: 1,

    }, background: {
        width: wp(413),
        position: "absolute",
    },
    content: {
        marginTop: hp(70),
        flex: 1,
    },
    logoHorizontal: {
        marginTop: hp(10),
    },
    icon: {
        marginTop: hp(20),
        marginLeft: wp(20),
    },
    logo: {
        position: "absolute",
        width: "100%",
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    card: {

        borderRadius: hp(50),
        backgroundColor: "white",
    },
    footer: {

        position: "absolute",
        bottom: 0,
        height: hp(235),
        width: wp(375),
        backgroundColor: Colors.Secondary,
        zIndex: -1,
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        width: wp(179),
        marginTop: hp(25),
        marginLeft: wp(35),
        fontSize: wp(20),
        fontWeight: "400",
    },
    subtitle: {
        marginTop: hp(5),
        marginLeft: wp(35),
        fontSize: wp(24),

        fontWeight: "bold",
        fontFamily: "Gilroy-Bold"
    },
    eposta: {
        color: Colors.title_active,
    },
    btnview: {
        marginTop: hp(10),
        marginBottom: hp(15),
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    btngo: {
        height: hp(50),
        width: wp(292),
    },
    accounttitle: {
        textAlign: "center",
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.white,
        position: "absolute",
        bottom: hp(20),

    },
    account: {
        fontSize: wp(12),
        fontWeight: "600",
        color: Colors.white
    },
    contract: {
        flexDirection: "row",
        width: wp(256),
        marginLeft: wp(43),
        marginRight: wp(45),
        marginTop: hp(10),
    },
    conttext: {
        fontSize: wp(10),
        fontWeight: "700",
        lineHeight: hp(19),
    },
    subconttext: {
        fontSize: wp(10),
        fontWeight: "700",
        lineHeight: hp(19),
        color: Colors.Primary,
    }
});
