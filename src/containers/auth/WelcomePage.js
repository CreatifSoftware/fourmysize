import React from "react";
import {View, StyleSheet, Image, Text, StatusBar, ActivityIndicator} from "react-native";
import {wp, hp} from "../../styles/Dimen"
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {ButtonSecondary} from "../../components/buttons/buttonSecondary";
import {Line, welcomeImage} from '../../assets/Images'
import {
    LOCALIZED_WELCOMEPAGE_BTN_GO, LOCALIZED_WELCOMEPAGE_BTN_CORPORATE,
    LOCALIZED_WELCOMEPAGE_BTN_INDIVIDUAL, LOCALIZED_WELCOMEPAGE_TITLE, LOCALIZED_WELCOMEPAGE_TITLE_NAME
} from "../../utils/constants";
import {getLocalizedString} from "../../utils/localization";
import {callCreateToken, callRegisteriIndividualCustomer} from "../../api/auth/authServices";
import {getErrorMessage, isSuccess} from "../../api/helpers";
import {deviceId, deviceName, deviceIp, uuidCreate} from "../../utils/deviceInfo";
//import * as uuid from "uuid";
import uuid from "react-native-uuid"
import connect from "react-redux/lib/connect/connect";
import {useDispatch} from "react-redux";
import allActions from "../../redux/action";
import {loadingAction} from "../../redux/action/masterAction";

const WelcomePage = props => {

    const dispatch = useDispatch()

    const [devicename, setDevicename] = React.useState();
    const [deviceip, setDeviceIp] = React.useState();

    React.useEffect(() => {
        deviceName().then(name => setDevicename(name))
        deviceIp().then(name => setDeviceIp(name))
    }, [devicename, deviceip])


    function createToken() {
        this.customerId = uuid.v4()
    }

    const IndividualClick = () => {
        callCreateToken(new createToken())
            .then(response => {
                dispatch(props.onAuth(response.data))
            })
            .catch(error => {
                alert(error + "Error")
            })

        props.navigation.navigate('Login')
    }

    function Guest() {
        this.deviceId = deviceId;
        this.deviceName = devicename;
        this.ipAddress = deviceip;
        this.languageId = 0;
        this.registrationSource = 0;
    }

    const authGuestSend = () => {

    }

    return <View style={styles.container}>
        <StatusBar hidden/>
        <Image style={styles.background} source={Line}/>

        <View style={styles.content}>


            <Image style={styles.image} source={welcomeImage}/>

            <Text style={styles.title}>{getLocalizedString(LOCALIZED_WELCOMEPAGE_TITLE)}<Text
                style={styles.titlename}>{getLocalizedString(LOCALIZED_WELCOMEPAGE_TITLE_NAME)}</Text></Text>


            <View style={styles.footer}>

                <View style={styles.footerTop}>
                    <ButtonPrimary
                        onPress={IndividualClick}
                        text={getLocalizedString(LOCALIZED_WELCOMEPAGE_BTN_INDIVIDUAL)}
                        style={styles.btnIndividual}
                        buttonColor={Colors.Primary}
                    />

                    <ButtonPrimary
                        onPress={IndividualClick}
                        text={getLocalizedString(LOCALIZED_WELCOMEPAGE_BTN_CORPORATE)}
                        style={styles.btnCorporate}
                        buttonColor={Colors.Secondary}
                    />
                </View>

                <View style={styles.footerBottom}>
                    <ButtonSecondary
                        onPress={authGuestSend}
                        text={getLocalizedString(LOCALIZED_WELCOMEPAGE_BTN_GO)}
                        style={styles.btnGuest}
                        iconVisible={true}
                        buttonColor={Colors.title_active}
                    />
                </View>

            </View>
        </View>

    </View>;
};

const mapStateToProp = (state) => {
    return state
}

const mapDispatchToProps = {
    onAuth: allActions.authAction,
}

export default connect(mapStateToProp, mapDispatchToProps)(WelcomePage);


const styles = StyleSheet.create({
    container: {
        flex: 1,
    }, background: {
        position: "absolute",
    },
    scrollview: {
        flex: 1,
    }, content: {
        alignItems: "center",
        justifyContent: "center",
    },
    image: {
        height: hp(400),
        width: wp(305),
    },
    title: {
        fontSize: wp(20),
        width: wp(212),
        height: hp(74),
        marginTop: hp(15),
        textAlign: "center"
    }, titlename: {
        fontSize: wp(22),
        fontWeight: "600",
        color: Colors.Primary,
        fontFamily: "Gilroy-Bold"
    },
    footer: {
        flexDirection: "column",
        marginTop: hp(32),
    },
    btnIndividual: {
        height: hp(48),
        width: wp(141),
        marginRight: wp(9),
    },
    btnCorporate: {
        height: hp(48),
        width: wp(141),
    },
    footerTop: {
        flexDirection: "row"
    },
    footerBottom: {},
    btnGuest: {
        height: hp(48),
        width: wp(290),
        marginTop: hp(19),
    }
});
