import React from "react";
import {Image, StyleSheet, View, Platform, UIManager, LayoutAnimation, ScrollView} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {threeDModelTshirt, LineDizayn} from "../../assets/Images";
import {LineBottom} from "../../components/lines/lineBottom";
import DesignTour from "../../views/tour/designTour";
import DesignSize from "../../views/design/Size";
import DesignColor from "../../views/design/Color";
import DesignEmbroidery from "../../views/design/Embroidery";
import DesignNick from "../../views/design/Nick";
import DesignPattern from "../../views/design/Pattern";
import DesignText from "../../views/design/Text";
import DesignPrinting from "../../views/design/Printing";
import {GroupTabs} from "../../components/tabs/tabs";
import {getLocalizedString} from "../../utils/localization";
import * as lng from "../../utils/constants";
import * as Icon from "../../assets/Icon";
import FloatingButton from "../../components/buttons/floatingButton";

if (Platform.OS === "android" && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

export const groupTabsData = [
    {
        key: "0",
        text: getLocalizedString(lng.LOCALIZED_DESIGN_ITEMS_SIZE),
        iconName: Icon.iconBeden
    },
    {
        key: "1",
        text: getLocalizedString(lng.LOCALIZED_DESIGN_ITEMS_COLOR),
        iconName: Icon.iconRenk
    },
    {
        key: "2",
        text: getLocalizedString(lng.LOCALIZED_DESIGN_ITEMS_PATTERN),
        iconName: Icon.iconDesen
    },
    {
        key: "3",
        text: getLocalizedString(lng.LOCALIZED_DESIGN_ITEMS_EMBROIDERY),
        iconName: Icon.iconNakis
    },
    {
        key: "4",
        text: getLocalizedString(lng.LOCALIZED_DESIGN_ITEMS_TEXT),
        iconName: Icon.iconText
    },
    {
        key: "5",
        text: getLocalizedString(lng.LOCALIZED_DESIGN_ITEMS_NICK),
        iconName: Icon.iconRumuz
    },
    {
        key: "6",
        text: getLocalizedString(lng.LOCALIZED_DESIGN_ITEMS_PRINTING),
        iconName: Icon.iconBaski
    },
];

const actions = [
    {
        text: "Paylaş",
        icon: "sharealt",
        name: "btn_share",
    },
    {
        text: "Şablonu Kaydet",
        icon: "check",
        name: "btn_save",
    },
    {
        text: "Tasarımı Tamamla",
        icon: "checkcircleo",
        name: "btn_finish",
    },
];

const DesignHome = (props) => {

    const [boxPosition, setBoxPosition] = React.useState("left");
    const [index, setIndex] = React.useState(-1);

    const openBox = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        setBoxPosition(boxPosition === "left" ? "right" : "right");
    };

    return (
        <View style={styles.container}>
            <Image style={styles.background} source={LineDizayn}/>
            <View style={{alignItems: "center"}}>
                <Image style={[styles.threeD, boxPosition === "left" ? null : styles.threeDMove]}
                       source={threeDModelTshirt}/>
            </View>

            <View style={styles.tour}>
                <DesignTour active={false}/>
            </View>


            <View style={[styles.footer, boxPosition === "left" ? null : styles.footerMove]}>
                <ScrollView style={{height: hp(40), marginLeft: wp(20)}} showsHorizontalScrollIndicator={false}
                            horizontal={true}>
                    <GroupTabs
                        callBackIndex={(index) => {
                            setIndex(index);
                            openBox();
                        }}
                        values={groupTabsData}
                        color={Colors.Primary}
                        passiveColor={Colors.input_background}/>
                </ScrollView>

                <View style={[styles.card, boxPosition === "left" ? null : styles.cardMove]}>
                    {index === 0 ? <DesignSize/> :
                        index === 1 ? <DesignColor/> :
                            index === 2 ? <DesignPattern/> :
                                index === 3 ? <DesignEmbroidery/> :
                                    index === 4 ? <DesignText/> :
                                        index === 5 ? <DesignNick/> :
                                            index === 6 ? <DesignPrinting/> : null}
                </View>
            </View>

            <FloatingButton
                isDraggerEnabled={false}
                actions={actions}
                x={90}
                y={90}
                color={Colors.Primary}
                onPressItem={name => name}
            />
            <LineBottom/>
        </View>)
}

export default DesignHome

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    threeD: {
        marginTop: hp(35),
        width: wp(357),
        height: hp(426),
        resizeMode: "contain",
    },
    threeDMove: {
        marginTop: hp(35),
        width: wp(241),
        height: hp(287),
    },
    content: {

        alignItems: "center",
    },
    title: {
        fontSize: wp(26),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-Bold",
        marginTop: hp(32),
        marginBottom: hp(15),
    },
    subtitle: {
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(24),
        width: hp(284),
        textAlign: "center"
    },
    btngo: {
        marginTop: hp(16),
        height: hp(40),
        width: wp(135),
    },
    footer: {
        position: "absolute",
        bottom: hp(20),
    },
    footerMove: {
        position: "absolute",
        bottom: hp(230),
    },
    card: {
        position: "absolute",
        bottom: 0,
        height: hp(205),
        marginLeft: wp(16),
        marginRight: wp(16),
        backgroundColor: Colors.input_background,
        borderRadius: hp(20),
    },
    cardMove: {
        position: "absolute",
        bottom: hp(-215),
    },
    tour: {
        zIndex: 100,
        position: "absolute"
    },
})
