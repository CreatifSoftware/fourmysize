import React from "react";
import {Image, StatusBar, StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {statusOne, statusThree, statusFour, statusTwo} from "../../assets/Images";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {LineBottom} from "../../components/lines/lineBottom";
import {getLocalizedString} from "../../utils/localization";
import {
    LOCALIZED_STATUS_FOUR_BUTTON_1_TEXT,
    LOCALIZED_STATUS_FOUR_SUBTITLE,
    LOCALIZED_STATUS_FOUR_TITLE,
    LOCALIZED_STATUS_ONE_BUTTON_1_TEXT,
    LOCALIZED_STATUS_ONE_BUTTON_2_TEXT,
    LOCALIZED_STATUS_ONE_SUBTITLE,
    LOCALIZED_STATUS_ONE_TITLE,
    LOCALIZED_STATUS_THREE_BUTTON_1_TEXT,
    LOCALIZED_STATUS_THREE_SUBTITLE,
    LOCALIZED_STATUS_THREE_TITLE,
    LOCALIZED_STATUS_TWO_BUTTON_1_TEXT,
    LOCALIZED_STATUS_TWO_BUTTON_2_TEXT,
    LOCALIZED_STATUS_TWO_SUBTITLE,
    LOCALIZED_STATUS_TWO_TITLE,
} from "../../utils/constants";
import {ButtonSecondary} from "../../components/buttons/buttonSecondary";

const StatusPage = (props) => {


    const [status, setStatus] = React.useState(3)


    const onHandleOneBtn1 = () => {

    }

    const onHandleOneBtn2 = () => {

    }

    const onHandleTwoBtn1 = () => {

    }

    const onHandleTwoBtn2 = () => {

    }

    const onHandleThreeBtn1 = () => {

    }

    const onHandleFourBtn1 = () => {

    }

    const switchContainerStyle = {
        backgroundColor: status === 2 ? Colors.statusThree :
            status === 3 ? Colors.statusFour : null,
    }

    const switchTitleStyle = {
        marginTop:
            status === 2 ? hp(-120) :
                status === 3 ? hp(-120) : null,
        color:
            status === 0 ? Colors.title_active :
                status === 1 ? Colors.statusTwo :
                    status === 2 ? Colors.statusThreeText : Colors.title_active
    }

    const switchSubTitleStyle = {
        color:
            status === 0 ? Colors.content_text :
                status === 1 ? Colors.content_text :
                    status === 2 ? Colors.statusThreeText : Colors.title_active
    }

    return <View style={[styles.container, switchContainerStyle]}>

        <View style={styles.content}>

            {status === 0 ? <Image style={styles.statusOne} source={statusOne}/> :
                status === 1 ? <Image style={styles.statusOne} source={statusTwo}/> :
                    status === 2 ? <Image source={statusThree}/> : <Image source={statusFour}/>}


            <Text style={[styles.title, switchTitleStyle]}>
                {status === 0 ? getLocalizedString(LOCALIZED_STATUS_ONE_TITLE) :
                    status === 1 ? getLocalizedString(LOCALIZED_STATUS_TWO_TITLE) :
                        status === 2 ? getLocalizedString(LOCALIZED_STATUS_THREE_TITLE)
                            : getLocalizedString(LOCALIZED_STATUS_FOUR_TITLE)}
            </Text>

            <Text style={[styles.subtitle, switchSubTitleStyle]}>
                {status === 0 ? getLocalizedString(LOCALIZED_STATUS_ONE_SUBTITLE) :
                    status === 1 ? getLocalizedString(LOCALIZED_STATUS_TWO_SUBTITLE) :
                        status === 2 ? getLocalizedString(LOCALIZED_STATUS_THREE_SUBTITLE)
                            : getLocalizedString(LOCALIZED_STATUS_FOUR_SUBTITLE)}
            </Text>


            {status === 0 ? <>
                    <ButtonPrimary
                        onPress={onHandleOneBtn1}
                        text={getLocalizedString(LOCALIZED_STATUS_ONE_BUTTON_1_TEXT)}
                        style={styles.btngo}
                        fontSize={17}
                        buttonColor={Colors.Secondary}/>
                    <ButtonSecondary
                        onPress={onHandleOneBtn2}
                        text={getLocalizedString(LOCALIZED_STATUS_ONE_BUTTON_2_TEXT)}
                        style={styles.btngo}
                        fontSize={17}
                        buttonColor={Colors.Secondary}/>
                </> :
                status === 1 ? <>
                        <ButtonPrimary
                            onPress={onHandleTwoBtn1}
                            text={getLocalizedString(LOCALIZED_STATUS_TWO_BUTTON_1_TEXT)}
                            style={styles.btngo}
                            fontSize={17}
                            buttonColor={Colors.statusTwo}/>
                        <ButtonSecondary
                            onPress={onHandleTwoBtn2}
                            text={getLocalizedString(LOCALIZED_STATUS_TWO_BUTTON_2_TEXT)}
                            style={styles.btngo}
                            fontSize={17}
                            buttonColor={Colors.statusTwo}/>
                    </> :
                    status === 2 ?
                        <ButtonPrimary
                            onPress={onHandleThreeBtn1}
                            text={getLocalizedString(LOCALIZED_STATUS_THREE_BUTTON_1_TEXT)}
                            style={styles.btngo}
                            fontSize={17}
                            textColor={Colors.statusThreeText}
                            buttonColor={Colors.white}/>

                        : <ButtonPrimary
                            onPress={onHandleFourBtn1}
                            text={getLocalizedString(LOCALIZED_STATUS_FOUR_BUTTON_1_TEXT)}
                            style={styles.btngo}
                            fontSize={17}
                            buttonColor={Colors.Primary}/>
            }

            <LineBottom/>
        </View>


    </View>
}

export default StatusPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    }, background: {
        position: "absolute",
    },
    statusOne: {
        marginTop: hp(108),
        width: wp(177),
        height: hp(225),
    },
    statusThree: {},
    content: {
        flex: 1,
        alignItems: "center",
    },

    title: {
        width: wp(300),
        fontSize: wp(26),
        fontWeight: "700",
        textAlign: "center",
        lineHeight: hp(37),
        color: Colors.title_active,
        fontFamily: "Gilroy-Bold",
        marginTop: hp(32),
        marginBottom: hp(15),
    },
    subtitle: {
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
        lineHeight: hp(24),
        width: hp(284),
        textAlign: "center"
    },
    btngo: {
        marginTop: hp(16),
        height: hp(48),
        width: wp(240),
    },

})
