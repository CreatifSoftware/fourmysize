import React from "react";
import {StyleSheet, View, Text, FlatList, RefreshControl} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {LineBottom} from "../../components/lines/lineBottom";
import {dataMyOrders} from "../../api/data"
import HistoryOrderItem from "../../components/items/historyOrderItem";
import EmptyView from "../../components/views/emptyView";
import {getLocalizedString} from "../../utils/localization";
import {LOCALIZED_HOMEPAGE_BUTTON_1_TEXT, LOCALIZED_HOMEPAGE_BUTTON_2_TEXT} from "../../utils/constants";
import {headerDefaultConfig} from "../../navigation/stackNavigation";
import {useDispatch, useSelector} from "react-redux";
import allActions from "../../redux/action";

const MyOrders = ({navigation, route}) => {

    const dispatch = useDispatch()
    const {orders} = useSelector(state => state.myOrders)

    React.useEffect(() => {

        dispatch(allActions.myOrderAction(dataMyOrders))

        if (route.params !== undefined)
            navigation.setOptions(headerDefaultConfig(Colors.white, "Siparişlerim", route.params.back))
        else
            navigation.setOptions(headerDefaultConfig(Colors.white, "Siparişlerim", false))
    }, [])

    const onRefresh = () => {

    }

    const ListFooter = () => {
        return <View style={styles.footerContainer}>
            <ButtonPrimary
                onPress={() => navigation.navigate("PayDetails")}
                text={"Alışverişe Devam Et"}
                style={styles.btngo}
                buttonColor={Colors.Secondary}
                iconVisible={false}/>
        </View>
    }

    const ListEmptyComp = () => {
        return <>
            <EmptyView type={"order"}
                       title={"Henüz Siparişin Bulunmuyor!"}
                       subtitle={"Contrary to popular belief, Lorem Ipsum is not simply random text. " +
                       "It has roots in a piece of classical Latin literature from 45 BC"}/>
            <ButtonPrimary
                onPress={() => navigation.navigate("MyOwnStylePageOne")}
                text={getLocalizedString(LOCALIZED_HOMEPAGE_BUTTON_1_TEXT)}
                style={styles.btncreatedizayn}
                buttonColor={Colors.PrimaryLight}
                iconColor={Colors.Primary}
                textColor={Colors.Primary}
                iconVisible={true}/>

            <ButtonPrimary
                text={getLocalizedString(LOCALIZED_HOMEPAGE_BUTTON_2_TEXT)}
                style={styles.btnspecialdizayn}
                buttonColor={Colors.SecondaryLight}
                iconColor={Colors.Secondary}
                textColor={Colors.Secondary}
                iconVisible={true}
                fontSize={wp(15)}/>
        </>
    }

    return <>
        <View style={styles.container}>
            <FlatList
                style={styles.flatlist}
                data={orders}
                renderItem={HistoryOrderItem}
                showsVerticalScrollIndicator={false}
                refreshControl={<RefreshControl refreshing={false} onRefresh={onRefresh}/>}
                ListHeaderComponent={orders !== null && orders.length !== 0 &&
                <Text style={styles.title}>Sipariş Geçmişin</Text>}
                ListFooterComponent={orders !== null && orders.length !== 0 && <ListFooter/>}
                ListFooterComponentStyle={{marginBottom: hp(50)}}
                ListEmptyComponent={ListEmptyComp}
                keyExtractor={item => item.id}/>
        </View>
        <LineBottom/>
    </>
}

export default MyOrders

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        alignItems: "center",
    },
    title: {
        fontSize: wp(16),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        marginTop: hp(22),
        marginLeft: wp(16),
    },
    footerContainer: {
        marginBottom: hp(50),
        alignItems: "center"
    },
    btngo: {
        marginTop: hp(33),
        height: hp(48),
        width: wp(305),
    },
    btncreatedizayn: {
        height: hp(50),
        width: wp(326),
        marginBottom: hp(20),
        marginTop: hp(20),
    },
    btnspecialdizayn: {
        height: hp(50),
        width: wp(326),
        marginBottom: hp(20),
    }
})

