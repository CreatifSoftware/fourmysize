import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity, Modal, Platform} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {BlurView} from "@react-native-community/blur";
import Styled from 'styled-components';
import Ionicons from "react-native-vector-icons/Ionicons";
import {GroupTabs} from "../../components/tabs/tabs";
import {star} from "../../assets/Images";
import {useNavigation} from "@react-navigation/native";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import StarRating from 'react-native-star-rating';
import MultiTextInput from "../../components/inputs/multiTextInput";

const groupTabsData = [
    {
        key: "0",
        text: "Hayır",
    },
    {
        key: "1",
        text: "Kararsızım",
    },
    {
        key: "2",
        text: "Evet",
    },

];

const OrderRating = (props) => {
    const navigation = useNavigation();

    const [index, setIndex] = React.useState(-1);
    const [rating, setRating] = React.useState(-1);

    const onStarRatingPress = (rating) => {
        setRating(rating)
    }

    return (
        <>
            {props.isShow &&

            <Modal animationType={"slide"} transparent>
                <View style={styles.container}>
                    <BlurWrapper
                        style={styles.absolute}
                        blurType="light"
                        blurAmount={9}
                        reducedTransparencyFallbackColor="white"
                    />

                    <View style={styles.card}>

                        <TouchableOpacity style={styles.exitview} onPress={() => props.onPress(false)}>
                            <Ionicons name={"close"} color={Colors.Primary} size={wp(20)} style={styles.exit}/>
                        </TouchableOpacity>

                        <Text style={styles.title}>Alışverişinden Memnun Kaldın mı?</Text>


                        <StarRating
                            disabled={false}
                            maxStars={5}
                            rating={rating}
                            emptyStar={'star-o'}
                            fullStar={'star'}
                            halfStar={'star-half'}
                            iconSet={'FontAwesome'}
                            emptyStarColor={Colors.RatingO}
                            halfStarColor={Colors.Primary}
                            fullStarColor={Colors.Primary}
                            starSize={22}
                            containerStyle={{
                                width: wp(246),
                                marginTop: hp(22),


                            }}
                            selectedStar={(rating) => onStarRatingPress(rating)}
                        />

                        <View style={{width: wp(276), flexDirection: "row", justifyContent: "space-between"}}>
                            <Text style={styles.ratingtext}>Hiç Memnun Kalmadım</Text>
                            <Text style={styles.ratingtext}>Çok Memnun Kaldım</Text>
                        </View>

                        <Text style={styles.title}>Arkadaşlarına Tavsiye Eder misin?</Text>

                        <View style={{marginLeft: wp(15), marginTop: hp(20)}}>

                            <GroupTabs
                                callBackIndex={(index) => {
                                    setIndex(index);
                                }}
                                values={groupTabsData}
                                color={Colors.Secondary}
                                passiveColor={Colors.input_background}
                                fontSize={11}/>

                        </View>

                        <Text style={styles.title}>Alışverişinle İlgili Yorumun</Text>

                        <MultiTextInput

                        />


                        <ButtonPrimary
                            fontSize={12}
                            text={"Değerlendir"}
                            style={styles.btngo}
                            buttonColor={Colors.Secondary}
                            iconVisible={false}
                        />


                    </View>


                </View>
            </Modal>

            }

        </>
    )
}

export default OrderRating

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    card: {
        height: hp(483),
        width: wp(320),
        backgroundColor: Colors.white,
        borderRadius:  hp(20),
        shadowColor: "#666666",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
        alignItems: "center",
    },
    exitview: {
        position: "absolute",
        right: Platform.OS === "ios" ? wp(-5) : wp(10),
        top: Platform.OS === "ios" ? hp(-30) : hp(10),
    },
    exit: {
        borderWidth: 1,
        height: hp(24),
        width: hp(24),
        borderRadius:  hp(12),
        textAlign: "center",
        justifyContent: "center",
        borderColor: Colors.Primary,
        alignItems: "center",
    },
    title: {
        marginTop: hp(27),
        fontSize: wp(14),
        fontWeight: "700",
        textAlign: "center",
        fontFamily: "Gilroy-bold"
    },
    grouptab: {
        marginTop: hp(20),
    },
    btngo: {

        height: hp(44),
        width: wp(276),
    },
    ratingtext: {
        height: hp(22),
        width: wp(51),
        fontSize: wp(8),
        marginTop: hp(10),
        fontWeight: "400",
        textAlign: "center",
        color: Colors.content_text,
    },
})

const BlurWrapper = Styled(BlurView)`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;
