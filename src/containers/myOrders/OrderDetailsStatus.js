import React from "react";
import {StyleSheet, View, Text, TouchableOpacity, Image} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {LineBottom} from "../../components/lines/lineBottom";
import {LineDizayn, threeDModelTshirt} from "../../assets/Images";
import {getLocalizedString} from "../../utils/localization";
import {
    LOCALIZED_ORDER_STATUS_PRODUCT_NAME,
    LOCALIZED_ORDER_STATUS_CARGO_NAME,
    LOCALIZED_ORDER_STATUS_CARGODATE_NAME,
    LOCALIZED_ORDER_STATUS_CARGODELIVERY_NAME,
    LOCALIZED_ORDER_STATUS_CARGOTRAKING_NAME,
    LOCALIZED_ORDER_STATUS_COLORSIZE_NAME,
    LOCALIZED_ORDER_STATUS_PAYTYPE_NAME,
    LOCALIZED_ORDER_STATUS_TOTALPRICE_NAME
} from "../../utils/constants";
import {ButtonSecondary} from "../../components/buttons/buttonSecondary";
import CustomProgressSteps from "../../components/progress/progressSteps";
import {dataOrderStatus} from "../../api/data";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import OrderRating from "./OrderRating";
import OrderCancelPopup from "../../views/popup/orderCancelPopup";
import {useDispatch, useSelector} from "react-redux";

const OrderDetailsStatus = ({navigation, route}) => {

    const dispatch = useDispatch()
    const {orders} = useSelector(state => state.myOrders)

    let productStatus;
    const productData = dataOrderStatus[route.params.id].map(x => x.productData)
    const cargoData = dataOrderStatus[route.params.id].map(x => x.cargoData)
    const CargoStatusId = "2";

    const [show, setshow] = React.useState(false);
    const [showCancelPopup, setShowCancelPopup] = React.useState(false);

    const [form, setFrom] = React.useState({
        text1: "", text2: "", text3: "", text4: "", productStatus: 0,
    })

    React.useEffect(() => {
        productStatus = parseInt(dataOrderStatus[route.params.id].map(x => x.productStatus).toString())

        let text1 = productStatus === 0 || productStatus === 1 ? productData.map(x => x.productName) : cargoData.map(x => x.cargoCompany)
        let text2 = productStatus === 0 || productStatus === 1 ? productData.map(x => x.ColorSize) : cargoData.map(x => x.cargoDate)
        let text3 = productStatus === 0 || productStatus === 1 ? productData.map(x => x.PayType) : cargoData.map(x => x.deliveryDate)
        let text4 = productStatus === 0 || productStatus === 1 ? productData.map(x => x.TotalPrice) : cargoData.map(x => x.trackingNumber)

        setFrom({text1, text2, text3, text4, productStatus: productStatus})

    }, [])


    const handleOrderCancel = () => {
        setShowCancelPopup(true)
    }

    const handleWhereCargo = () => {

    }

    const handleRating = () => {
        setshow(true)
    }

    return <View style={styles.container}>

        <OrderCancelPopup
            isShow={showCancelPopup}
            onPress={(is) => setShowCancelPopup(is)}
            title={"İptal sebebini bizimle paylaşabilir misin?"}
        />

        <Image style={styles.background} source={LineDizayn}/>
        <Image style={styles.threeD} source={threeDModelTshirt}/>

        <View style={styles.card}>

            <CustomProgressSteps
                activeStep={form.productStatus}
                isComplete={form.productStatus === 3}
            />

            {form.productStatus !== 3 ? <>
                    <View style={styles.subview}>
                        <Text style={styles.title}>

                            {form.productStatus.toString() === CargoStatusId ?
                                getLocalizedString(LOCALIZED_ORDER_STATUS_CARGO_NAME) :
                                getLocalizedString(LOCALIZED_ORDER_STATUS_PRODUCT_NAME)}
                        </Text>
                        <Text style={styles.subtitle}>: {form.text1}</Text>
                    </View>

                    <View style={styles.subview}>
                        <Text style={styles.title}>
                            {form.productStatus.toString() === CargoStatusId ?
                                getLocalizedString(LOCALIZED_ORDER_STATUS_CARGODATE_NAME) :
                                getLocalizedString(LOCALIZED_ORDER_STATUS_COLORSIZE_NAME)}
                        </Text>
                        <Text style={styles.subtitle}>: {form.text2}</Text>
                    </View>

                    <View style={styles.subview}>
                        <Text style={styles.title}>
                            {form.productStatus.toString() === CargoStatusId ?
                                getLocalizedString(LOCALIZED_ORDER_STATUS_CARGODELIVERY_NAME) :
                                getLocalizedString(LOCALIZED_ORDER_STATUS_PAYTYPE_NAME)}
                        </Text>
                        <Text style={styles.subtitle}>: {form.text3}</Text>
                    </View>

                    <View style={styles.subview}>
                        <Text style={styles.title}>
                            {form.productStatus.toString() === CargoStatusId ?
                                getLocalizedString(LOCALIZED_ORDER_STATUS_CARGOTRAKING_NAME) :
                                getLocalizedString(LOCALIZED_ORDER_STATUS_TOTALPRICE_NAME)}
                        </Text>
                        <Text style={styles.subtitle}>: {form.text4}</Text>
                    </View>

                    <View style={{alignItems: "center"}}>

                        {form.productStatus.toString() === CargoStatusId ?
                            <ButtonSecondary
                                onPress={handleWhereCargo}
                                text={"Kargom Nerede"}
                                style={styles.btngo}
                                iconVisible={false}
                                buttonColor={Colors.Secondary}
                            /> :
                            <ButtonSecondary
                                onPress={handleOrderCancel}
                                text={"Siparişi İptal Et"}
                                style={styles.btngo}
                                iconVisible={false}
                                buttonColor={Colors.Error}
                            />
                        }

                    </View>
                </> :
                <View style={{alignItems: "center"}}>

                    <Text style={styles.cargofinishtext}>Siparişin 07.02.2021 tarihinde
                        teslim edilmiştir.</Text>

                    <TouchableOpacity onPress={handleRating}>
                        <View style={styles.cargofinishbtn}>
                            <Text style={styles.cargofinishtitle}>Siparişini Değerlendirmek İster misin?</Text>
                            <Text style={styles.cargofinishsubtitle}>Sipariş deneyimini değerlendirerek bir sonraki
                                alışverişinde %10 indirim kazanabilirsin.
                            </Text>

                            <View style={styles.cargofinishicon}>
                                <MaterialIcons name={"navigate-next"} size={hp(15)} color={Colors.white}/>
                            </View>
                        </View>
                    </TouchableOpacity>

                </View>
            }

            <LineBottom/>

        </View>

        <OrderRating
            isShow={show} onPress={(show) => {
            setshow(show)
        }}/>
    </View>
}

export default OrderDetailsStatus

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        alignItems: "center",
    }, background: {
        position: "absolute",
        width: wp(413),
    },
    threeD: {
        width: wp(232),
        height: hp(278),
        resizeMode: "contain",
        zIndex: 100,
    },
    card: {


        paddingTop: hp(20),
        position: "absolute",
        bottom: 0,
        height: hp(357),
        width: wp(375),
        backgroundColor: Colors.white,
        borderTopStartRadius: hp(30),
        borderTopRightRadius: hp(30),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
    },
    subview: {

        flexDirection: "row",
        marginLeft: wp(40),
        marginBottom: hp(15),
    },
    title: {
        width: wp(120),
        fontSize: wp(14),
        fontWeight: "700",
        color: Colors.content_text,
        fontFamily: "Gilroy-bold"
    },
    subtitle: {
        width: wp(190),
        fontSize: wp(14),
        fontWeight: "700",
        color: Colors.content_text,

    },
    btngo: {
        marginTop: hp(16),
        height: hp(48),
        width: wp(193),
    },
    cargofinishtext: {
        marginTop: hp(30),
        width: wp(300),
        fontSize: wp(16),
        fontWeight: "400",
        color: Colors.content_text,
        textAlign: "center",

    },
    cargofinishbtn: {
        marginTop: hp(40),
        paddingLeft: wp(23),
        height: hp(81),
        width: wp(345),
        backgroundColor: Colors.Primary,
        borderRadius: hp(15),
        shadowColor: "#868686",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 0.9,
        justifyContent: "center",
    },
    cargofinishtitle: {
        fontSize: wp(14),
        fontWeight: "700",
        color: Colors.white,
        fontFamily: "Gilroy-bold",
        marginBottom: hp(3),
    },
    cargofinishsubtitle: {
        fontSize: wp(12),
        fontWeight: "400",
        color: Colors.brown,
        width: hp(261),
        marginBottom: hp(3),
        lineHeight: hp(15),
    },
    cargofinishicon: {

        position: "absolute",
        right: wp(23),
        height: hp(24),
        width: hp(24),
        borderRadius: hp(40),
        backgroundColor: Colors.PrimaryDark,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
    }

})
