import React from "react";
import {Image, StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {
    DrawerContentScrollView,
    DrawerItem,
} from '@react-navigation/drawer';
import Ionicons from "react-native-vector-icons/Ionicons";
import {LogoW, profileImage} from "../../assets/Images"
import {
    iconHome, iconSSS, iconNotification, iconCampaing,
    iconSettings, iconHowDesign, iconBascet
} from "../../assets/Icon";
import Animated from "react-native-reanimated";
import {connect, useDispatch} from "react-redux";
import {signOutAction} from "../../redux/action/masterAction";
import * as RootNavigation from "../../navigation/RootNavigation"
import {CommonActions} from "@react-navigation/native";
const {interpolateNode, Extrapolate} = Animated;

const DrawerPage = (props) => {
    const dispatch = useDispatch()

    const {state, progress, navigation} = props;

    const opacity = interpolateNode(progress, {
        inputRange: [0, 0.5, 1],
        outputRange: [0, 0.1, 1],
        extrapolate: Extrapolate.CLAMP
    });

    return (
        <Animated.View style={[styles.container, {opacity: opacity}]}>
            <DrawerContentScrollView {...props} scrollEnabled={false} contentContainerStyle={styles.container}>

                <Image style={styles.logo} source={LogoW}/>

                <DrawerItem
                    label="Anasayfa"
                    labelStyle={styles.drawerLabel}
                    style={styles.drawerItem}
                    onPress={() => props.navigation.navigate('HomePage')}
                    icon={() => <Image source={iconHome}/>}
                />
                <DrawerItem
                    label="Hesap Ayarlarım"
                    labelStyle={styles.drawerLabel}
                    style={styles.drawerItem}
                    onPress={() => props.navigation.navigate('ProfileMain')}
                    icon={() => <Image source={iconSettings}/>}
                />
                <DrawerItem
                    label="Siparişlerim"
                    labelStyle={styles.drawerLabel}
                    style={styles.drawerItem}
                    onPress={() => props.navigation.navigate('MyOrders')}
                    icon={() => <Image source={iconBascet}/>}
                />

                <DrawerItem
                    label="Nasıl Tasarlarım"
                    labelStyle={styles.drawerLabel}
                    style={styles.drawerItem}
                    onPress={() => props.navigation.navigate('HowDesign')}
                    icon={() => <Image source={iconHowDesign}/>}
                />

                <DrawerItem
                    label="Kampanyalar"
                    labelStyle={styles.drawerLabel}
                    style={styles.drawerItem}
                    onPress={() => props.navigation.navigate('Contact')}
                    icon={() => <Image source={iconCampaing}/>}
                />

                <DrawerItem
                    label="Bildirimler"
                    labelStyle={styles.drawerLabel}
                    style={styles.drawerItem}
                    onPress={() => props.navigation.navigate('NotificationPage')}
                    icon={() => <Image source={iconNotification}/>}
                />

                <DrawerItem
                    label="S.S.S"
                    labelStyle={styles.drawerLabel}
                    style={styles.drawerItem}
                    onPress={() => props.navigation.navigate('SSSPage')}
                    icon={() => <Image source={iconSSS}/>}
                />

                <View style={styles.footer}>
                    <View>
                        <Image style={styles.profile} source={profileImage}/>
                        <View style={styles.status}/>
                    </View>

                    <View>
                        <Text style={styles.uname}>Dina Akçay</Text>

                        <TouchableOpacity onPress={() => {
                            dispatch(signOutAction(true))
                        }}>
                            <View style={styles.exitview}>
                                <Ionicons name={"md-exit-outline"} size={13} color={Colors.white}/>
                                <Text style={styles.exit}>Çıkış Yap</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>

            </DrawerContentScrollView>
        </Animated.View>
    )
}


export default DrawerPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Primary
    },
    logo: {
        marginTop: hp(33),
        marginLeft: wp(30),
        marginBottom: hp(47),
    },
    drawerItem: {

        marginLeft: wp(32),

    },
    drawerLabel: {
        fontSize: wp(18),
        fontWeight: "400",
        color: 'white',
        marginLeft: wp(-20),

    },
    footer: {
        marginTop: hp(15),
        marginLeft: wp(40),
        flexDirection: "row",

    },
    profile: {
        borderRadius: hp(10),
        width: hp(51),
        height: hp(51),
        marginRight: wp(12),
    },
    status: {
        borderRadius: hp(20),
        width: hp(12),
        height: hp(12),
        position: "absolute",
        top: hp(-5),
        right: wp(10),
        borderWidth: 2,
        backgroundColor: Colors.supplement_1,
        borderColor: Colors.white,
    },
    uname: {
        fontSize: wp(16),
        fontWeight: "700",
        fontFamily: "Gilroy-Bold",
        color: Colors.white,
        marginBottom: hp(4)
    },
    exitview: {
        flexDirection: "row",
        alignItems: "center",
    },
    exit: {
        fontSize: wp(12),
        fontWeight: "400",
        marginLeft: wp(3),
        color: Colors.white,
    }
});
