import React from "react";
import {View, StyleSheet, ScrollView, Image, Text} from "react-native";
import {Colors} from "../styles/Colors";
import {wp, hp} from "../styles/Dimen"
import {ButtonPrimary} from "../components/buttons/buttonPrimary";
import {getLocalizedString} from "../utils/localization";
import {
    LOCALIZED_HOMEPAGE_BUTTON_1_TEXT,
    LOCALIZED_HOMEPAGE_PIC_1_TEXT,
    LOCALIZED_HOMEPAGE_BUTTON_2_TEXT,
    LOCALIZED_HOMEPAGE_PIC_2_TEXT,
    LOCALIZED_HOMEPAGE_PIC_3_TEXT,
    LOCALIZED_HOMEPAGE_PIC_4_TEXT,
    LOCALIZED_HOMEPAGE_SUB_TITLE,
    LOCALIZED_HOMEPAGE_TITLE
} from "../utils/constants";
import {homePageImage} from "../assets/Images";

const HomePage = (props) => {

    const MyOwnStyleRediret = () => {
        props.navigation.navigate("MyOwnStylePageOne")
    }

    return <View style={styles.container}>

        <Image style={styles.image} source={homePageImage}/>

        <View style={styles.footer}>
            <Text  style={styles.title}>{getLocalizedString(LOCALIZED_HOMEPAGE_TITLE)}</Text>
            <Text style={styles.subtitle}>{getLocalizedString(LOCALIZED_HOMEPAGE_SUB_TITLE)}</Text>

            <ButtonPrimary
                onPress={MyOwnStyleRediret}
                text={getLocalizedString(LOCALIZED_HOMEPAGE_BUTTON_1_TEXT)}
                style={styles.btncreatedizayn}
                buttonColor={Colors.PrimaryLight}
                iconColor={Colors.Primary}
                textColor={Colors.Primary}
                iconVisible={true}
            />

            <ButtonPrimary
                text={getLocalizedString(LOCALIZED_HOMEPAGE_BUTTON_2_TEXT)}
                style={styles.btnspecialdizayn}
                textColor={Colors.Primary}
                buttonColor={Colors.SecondaryLight}
                iconColor={Colors.Secondary}
                textColor={Colors.Secondary}
                iconVisible={true}
                fontSize={wp(15)}
            />
        </View>
    </View>;
};

export default HomePage


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background,
        alignItems: "center",
    }, background: {
        flex: 1,
    },
    image: {
        marginTop: hp(5),
        height: hp(294),
        width: wp(342),
        resizeMode: "contain",
    },
    footer: {
        marginTop: hp(35),
        alignItems: "center"
    },
    title: {
        fontSize: wp(35),
        fontWeight: "400",
        includeFontPadding:false,
        color: Colors.Secondary,
    },
    subtitle: {
        fontSize: wp(50),
        height:hp(50),
        includeFontPadding:false,
        textAlign:"center",
        fontWeight: "700",
        color: Colors.Secondary,
        fontFamily: "Gilroy-Bold",
    },
    btncreatedizayn: {
        height: hp(50),
        width: wp(326),
        marginBottom: hp(20),
        marginTop: hp(20),
    },
    btnspecialdizayn: {
        height: hp(50),
        width: wp(326),
        marginBottom: hp(20),
    }

});
