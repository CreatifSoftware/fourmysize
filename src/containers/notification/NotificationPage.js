import React from "react";
import {Image, StyleSheet, View, Text, FlatList} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {dataNotification, dataSSSProfile} from "../../api/data";
import SSSDetailItem from "../../components/items/SSSDetailItem";
import {LineBottom} from "../../components/lines/lineBottom";
import {headerDefaultConfig} from "../../navigation/stackNavigation";
import NotificationItem from "../../components/items/notificationItem";
import EmptyView from "../../components/views/emptyView";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {getLocalizedString} from "../../utils/localization";
import {LOCALIZED_HOMEPAGE_BUTTON_1_TEXT, LOCALIZED_HOMEPAGE_BUTTON_2_TEXT} from "../../utils/constants";

const NotificationPage = (props) => {

    const ListEmptyComp = () => {
        return <View style={{alignItems:"center"}}>
            <EmptyView type={"notification"}
                       title={"Yeni Bildirim Bulunmuyor!"}
                       subtitle={"Contrary to popular belief, Lorem Ipsum is not simply random text. " +
                       "It has roots in a piece of classical Latin literature from 45 BC"}/>


            <ButtonPrimary
                text={'Bildirim Tercihlerim'}
                style={styles.btngo}
                buttonColor={Colors.Primary}
                fontSize={wp(17)}
            />
        </View>
    }

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <FlatList
                    style={styles.flatlist}
                    data={dataNotification}
                    renderItem={NotificationItem}
                    contentContainerStyle={styles.flatlist}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={item => item.id}
                    ListEmptyComponent={ListEmptyComp}
                />
            </View>
            <LineBottom/>
        </View>
    )
}

export default NotificationPage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background
    },
    content: {
        justifyContent: "center",
        alignItems: "center"
    },
    flatlist: {
        flexGrow: 1,
        paddingBottom: hp(40)
    },
    btngo: {
        marginTop: hp(73),
        height: hp(48),
        width: wp(222),
    },
})
