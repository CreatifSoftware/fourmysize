import React from "react";
import {Image, StyleSheet, View, Text} from "react-native";
import {hp, wp} from "../../styles/Dimen";
import {Colors} from "../../styles/Colors";
import {notificationImage} from "../../assets/Images";
import ToggleSwitch from "../../components/switches/toggleSwitch";
import {ButtonPrimary} from "../../components/buttons/buttonPrimary";
import {useDispatch, useSelector} from "react-redux";
import {notificationCampaingAction, notificationEmailSmsAction} from "../../redux/action/masterAction";
import SuccessPopup from "../../views/popup/successPopup";

const NotificationSettings = (props) => {

    const dispatch = useDispatch();
    const {notificationCampaign, notificationEmailSms} = useSelector(state => state.master)

    const [notiOne, setNotiOne] = React.useState(notificationCampaign)
    const [notiTwo, setNotiTwo] = React.useState(notificationEmailSms)
    const [popup, setPopup] = React.useState(false)

    const handleChange = () => {
        dispatch(notificationCampaingAction(notiOne));
        dispatch(notificationEmailSmsAction(notiTwo));
        setPopup(true)
    }

    return (
        <View style={styles.container}>
            <SuccessPopup title={"Bildirim Tercihlerin Güncellendi"} isShow={popup}
                          onPress={(isShow) => setPopup(isShow)}/>

            <Text style={styles.title}>Sana bildirim
                göndermemizi ister misin?</Text>

            <Image style={styles.image} source={notificationImage}/>

            <ToggleSwitch
                onPress={() => {
                    setNotiOne(!notiOne);
                }}
                isOpen={notiOne}
                label={"Kampanya ve duyurularla ilgili bildirim almak istiyorum."}
            />

            <ToggleSwitch
                onPress={() => {
                    setNotiTwo(!notiTwo);
                }}
                isOpen={notiTwo}
                label={"4MySize tarafından e-posta veya SMS gönderimine onay veriyorum."}
            />

            <View style={{alignItems: "center"}}>
                <ButtonPrimary
                    onPress={handleChange}
                    text={"Tercihlerimi Güncelle"}
                    style={styles.btngo}
                    buttonColor={Colors.Primary}
                    iconVisible={false}
                />
            </View>
        </View>
    )
}

export default NotificationSettings

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.background
    }, title: {
        fontSize: wp(21),
        fontWeight: "700",
        color: Colors.title_active,
        fontFamily: "Gilroy-bold",
        width: wp(252),
        marginTop: hp(22),
        marginLeft: wp(16),
    },
    btngo: {
        marginTop: hp(53),
        height: hp(48),
        width: wp(238),
    },
    image: {
        height: hp(266),
        width: wp(375),
        resizeMode: "contain",
    }
})
