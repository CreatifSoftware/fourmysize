import * as React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerPage from "../containers/drawerPage/DrawerPage";
import {StackRoot} from "./stackNavigation";
import Animated, {interpolateNode} from "react-native-reanimated";
import DeviceInfo from "react-native-device-info";
import {Colors} from "../styles/Colors";
import {hp} from "../styles/Dimen"

const Drawer = createDrawerNavigator();
export const AnimatedContext = React.createContext(void 0);

export const DrawerNavigation = (props) => {

    const {interpolateNode, Extrapolate} = Animated;
    let screenStyle = null;

    return (<Drawer.Navigator

            drawerStyle={{
                backgroundColor: Colors.Primary,
                width: DeviceInfo.isTablet() ? 'auto' : '80%'
            }}

            drawerType='back'
            initialRouteName="HomePage"
            overlayColor="transparent"
            sceneContainerStyle={{backgroundColor: Colors.Primary}}
            drawerContent={props => {

                const scale = interpolateNode(props.progress, {
                    inputRange: [0, 1],
                    outputRange: [1, 0.85],
                    extrapolate: Extrapolate.CLAMP
                });

                const borderRadius = interpolateNode(props.progress, {
                    inputRange: [0, 1],
                    outputRange: [0, 20],
                    extrapolate: Extrapolate.CLAMP
                });

                screenStyle = {
                    transform: [{
                        scaleY: scale,
                    }],
                    borderRadius,
                };

                return <DrawerPage {...props} />
            }}>

            <Drawer.Screen name="Drawer">
                {props => <StackRoot  {...props} style={{...screenStyle}}/>}
            </Drawer.Screen>

        </Drawer.Navigator>
    );
}

export default DrawerNavigation
