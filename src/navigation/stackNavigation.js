import React from 'react'
import {StyleSheet} from "react-native";
import {createStackNavigator, Header, TransitionSpecs} from '@react-navigation/stack'
import {Colors} from "../styles/Colors";
import WelcomePage from "../containers/auth/WelcomePage";
import HomePage from "../containers/HomePage";
import Splash from "../containers/auth/Splash";
import Register from "../containers/auth/Register";
import Login from "../containers/auth/Login";
import ForgotPassword from "../containers/auth/ForgotPassword";
import MyOwnStylePageOne from "../containers/myOwnStylePage/MyOwnStylePageOne";
import MyOwnStylePageTwo from "../containers/myOwnStylePage/MyOwnStylePageTwo";
import MyOwnStylePageThree from "../containers/myOwnStylePage/MyOwnStylePageThree";
import HeaderTitle from "../views/header/headerTitle";
import DrawerButton from "../views/header/drawerButton";
import OrderButton from "../views/header/orderButton";
import GoBackButton from "../views/header/back";
import OrderDetails from "../containers/shoppingCart/OrderDetails";
import DesignHome from "../containers/designPage/DesignHome";
import PriceTitle from "../views/header/priceTitle";
import PayDetails from "../containers/shoppingCart/PayDetails";
import ProfileMain from "../containers/profiles/ProfileMain";
import OrderDetailsStatus from "../containers/myOrders/OrderDetailsStatus";
import MyOrders from "../containers/myOrders/MyOrders";
import StatusPage from "../containers/designPage/StatusPage";
import MyAddress from "../containers/profiles/MyAddress";
import AddAddress from "../containers/profiles/AddAddress";
import SSSPage from "../containers/sss/SSSPage";
import SSSPageDetails from "../containers/sss/SSSPageDetails";
import NotificationSettings from "../containers/notification/NotificationSettings";
import Animated from "react-native-reanimated";
import HowDesign from "../views/howDesign/howDesign";
import {hp} from "../styles/Dimen";
import OtpConfirm from "../containers/auth/OtpConfirm";
import NewPassword from "../containers/auth/NewPassword";
import MyCoupons from "../containers/profiles/MyCoupons";
import ProfileEdit from "../containers/profiles/ProfileEdit";
import MyTemplates from "../containers/profiles/MyTemplates";
import NotificationPage from "../containers/notification/NotificationPage";
import MyFavorites from "../containers/profiles/MyFavorites";

const RootStack = createStackNavigator();
const HomeStack = createStackNavigator();
const LoginStack = createStackNavigator();
const DesignStack = createStackNavigator();
const DesignHomeStack = createStackNavigator();
const OrderStack = createStackNavigator();
const MyOrderStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const SSSStack = createStackNavigator();
const NotificationStack = createStackNavigator();
const HowDesignStack = createStackNavigator();

export const headerDefaultConfig = (color = "white",
                                    title = "",
                                    isBack = false,
                                    transparent = false,
                                    iconColor = "black",
                                    priceButton) => {
    return {
        headerTitle: props => <HeaderTitle {...props} isIcon={title ? false : true}
                                           color={iconColor}
                                           title={title}/>,
        headerTintColor: 'white',
        headerTransparent: transparent,
        headerStyle: {
            backgroundColor: color,
            elevation: 0,
            shadowOpacity: 1,
            shadowColor: color,
            shadowRadius: 0,
            shadowOffset: {
                height: 7,
            },
            borderBottomLeftRadius: hp(10),
            borderBottomRightRadius: hp(10),
        },
        headerLeft: props => (isBack ? <GoBackButton {...props}/> : <DrawerButton {...props} color={iconColor}/>),
        headerRight: props => (!priceButton ? <OrderButton {...props} color={iconColor}/> : <PriceTitle/>),
    }
}


export function StackRoot(props) {
    const {style} = props;

    return (
        <Animated.View style={[styles.animatedView, style]}>
            <RootStack.Navigator
                initialRouteName={"HomePage"}
                screenOptions={{headerShown: false}}>

                <RootStack.Screen name="HomePage" component={StackHome}/>
                <RootStack.Screen name="MyOwnStylePageOne" component={StackDesign}/>
                <RootStack.Screen name="DizaynHome" component={StackDesignHome}/>
                <RootStack.Screen name="OrderDetails" component={StackOrder}/>
                <RootStack.Screen name="ProfileMain" component={StackProfile}/>
                <RootStack.Screen name="MyOrders" component={StackMyOrder}/>
                <RootStack.Screen name="NotificationPage" component={StackNotification}/>
                <RootStack.Screen name="SSSPage" component={StackSSS}/>
                <RootStack.Screen name="HowDesign" component={StackHowDesign}/>

            </RootStack.Navigator>
        </Animated.View>
    );
}

export default function StackLogin() {
    return (
        <LoginStack.Navigator
            initialRouteName={"Splash"}
            screenOptions={{
                headerTitle: props => {
                    return (<HeaderTitle {...props} isIcon={true}/>);
                },
                headerTransparent: true,
                headerLeft: props => (<GoBackButton {...props}/>),
            }}>

            <LoginStack.Screen name="HomePage" component={HomePage}/>
            <LoginStack.Screen name="WelcomePage" component={WelcomePage} options={{headerShown: false}}/>
            <LoginStack.Screen name="Splash" component={Splash} options={{headerShown: false}}/>
            <LoginStack.Screen name="Login" component={Login}/>
            <LoginStack.Screen name="Register" component={Register}/>
            <LoginStack.Screen name="OptConfirm" component={OtpConfirm}/>
            <LoginStack.Screen name="ForgotPassword" component={ForgotPassword}/>
            <LoginStack.Screen name="NewPassword" component={NewPassword}/>

        </LoginStack.Navigator>
    );
}

export function StackHome() {
    return (
        <HomeStack.Navigator initialRouteName={"HomePage"}
                             screenOptions={headerDefaultConfig(Colors.input_background)}>
            <HomeStack.Screen name="HomePage" component={HomePage}/>
        </HomeStack.Navigator>
    );
}

function StackDesign() {
    return (
        <DesignStack.Navigator
            screenOptions={headerDefaultConfig(Colors.white, "Kendi Tarzın")}>

            <DesignStack.Screen name="MyOwnStylePageOne" component={MyOwnStylePageOne}/>
            <DesignStack.Screen name="MyOwnStylePageTwo" component={MyOwnStylePageTwo}/>
            <DesignStack.Screen name="MyOwnStylePageThree" component={MyOwnStylePageThree}/>
        </DesignStack.Navigator>
    );
}

function StackDesignHome() {
    return (
        <DesignHomeStack.Navigator
            screenOptions={headerDefaultConfig(Colors.white, "Dizayn Et", true)}>

            <DesignHomeStack.Screen name="DizaynHome" component={DesignHome}/>
            <DesignHomeStack.Screen
                options={headerDefaultConfig(Colors.white, "", false, true)}
                name="StatusPage"
                component={StatusPage}/>

        </DesignHomeStack.Navigator>
    );
}

function StackOrder() {
    return (
        <OrderStack.Navigator
            initialRouteName={"MyOrders"}
            screenOptions={headerDefaultConfig(Colors.white, "Sipariş Özetin")}>

            <OrderStack.Screen name="OrderDetails" component={OrderDetails}
                               options={headerDefaultConfig(Colors.white, "Sipariş Özetin")}/>
            <OrderStack.Screen name="PayDetails"
                               options={headerDefaultConfig(Colors.white, "Sipariş Özetin", true)}
                               component={PayDetails}/>
        </OrderStack.Navigator>
    );
}

function StackMyOrder() {
    return (
        <MyOrderStack.Navigator>
            <MyOrderStack.Screen name="MyOrders"
                                 component={MyOrders}/>
            <MyOrderStack.Screen name="OrderDetailsStatus"
                                 options={headerDefaultConfig(Colors.white, "Sipariş Detayın", true)}
                                 component={OrderDetailsStatus}/>
        </MyOrderStack.Navigator>
    );
}

function StackProfile() {
    return (
        <ProfileStack.Navigator initialRouteName={"ProfileMain"}>

            <ProfileStack.Screen name="ProfileMain"
                                 options={headerDefaultConfig(Colors.Primary, "", false, false, Colors.white)}
                                 component={ProfileMain}/>

            <ProfileStack.Screen name="ProfileEdit"
                                 options={headerDefaultConfig(Colors.white, "Kullanıcı Bilgilerim", true)}
                                 component={ProfileEdit}/>

            <ProfileStack.Screen name="MyOrders"
                                 component={MyOrders}/>

            <MyOrderStack.Screen name="OrderDetailsStatus"
                                 options={headerDefaultConfig(Colors.white, "Sipariş Detayın", true)}
                                 component={OrderDetailsStatus}/>

            <ProfileStack.Screen name={"MyTemplates"}
                                 options={headerDefaultConfig(Colors.white, "Kayıtlı Şablonlarım", true)}
                                 component={MyTemplates}/>

            <ProfileStack.Screen name={"MyAddress"}
                                 options={headerDefaultConfig(Colors.white, "Adres Bilgilerim", true)}
                                 component={MyAddress}/>

            <ProfileStack.Screen name={"AddAddress"}
                                 options={headerDefaultConfig(Colors.white, "Yeni Adres Tanımla", true)}
                                 component={AddAddress}/>

            <ProfileStack.Screen name={"CouponPage"}
                                 options={headerDefaultConfig(Colors.white, "İndirim Kuponlarım", true)}
                                 component={MyCoupons}/>

            <ProfileStack.Screen name={"NotificationSetting"}
                                 options={headerDefaultConfig(Colors.white, "Bildirim Ayarları", true)}
                                 component={NotificationSettings}/>

            <ProfileStack.Screen name={"MyFavorites"}
                                 options={headerDefaultConfig(Colors.white, "Favorilerim", true)}
                                 component={MyFavorites}/>
        </ProfileStack.Navigator>
    );
}

function StackSSS() {
    return (
        <SSSStack.Navigator
            screenOptions={headerDefaultConfig(Colors.white, "Sık Sorulan Sorular", false)}>
            <SSSStack.Screen name="SSSPage" component={SSSPage}/>
            <SSSStack.Screen name="SSSPageDetails" component={SSSPageDetails}/>
        </SSSStack.Navigator>
    );
}

function StackNotification() {
    return (
        <NotificationStack.Navigator
            screenOptions={headerDefaultConfig(Colors.white, "Bildirimlerim", false)}>
            <NotificationStack.Screen name="NotificationPage" component={NotificationPage}/>
        </NotificationStack.Navigator>
    );
}

function StackHowDesign() {
    return (
        <HowDesignStack.Navigator
            screenOptions={headerDefaultConfig(Colors.white, "", false)}>
            <HowDesignStack.Screen name="HowDesign" component={HowDesign}/>
        </HowDesignStack.Navigator>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    animatedView: {
        flex: 1,
        overflow: 'hidden'
    },
})
