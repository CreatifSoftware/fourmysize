import React from "react";
import {View, Text} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import {navigationRef} from "./navigation/RootNavigation";
import StackLogin from "./navigation/stackNavigation";
import DrawerNavigation from "./navigation/drawerNavigation";
import Loading from "./components/Loading";
import {useSelector} from "react-redux";
import connect from "react-redux/lib/connect/connect";
import {initialAction} from "./redux/action/masterAction";
import allActions from "./redux/action";


const Route = () => {

    const {signOut} = useSelector(state => state.master)

    const [isloading, setLoading] = React.useState(false)
    const {loading} = useSelector(state => state.master)


    React.useEffect(() => {
        setLoading(loading)
    }, [loading])

    return <NavigationContainer ref={navigationRef}>
        {isloading && <Loading/>}
        {(signOut === undefined ? false : !signOut) ? <DrawerNavigation/> : <StackLogin/>}
    </NavigationContainer>;

};

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = {
    initial: allActions.initialAction
}

export default Route;
