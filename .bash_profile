# shellcheck shell=sh
export ANDROID_HOME=/Users/macbookpro/Library/Android/sdk
export PATH=${PATH}:/Users/macbookpro/Library/Android/sdk/tools
export PATH=${PATH}:/Users/macbookpro/Library/Android/sdk/platform-tools
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
