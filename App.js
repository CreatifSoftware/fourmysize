/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React from 'react';
import Route from "./src/Route";
import {StatusBar, Platform, StyleSheet, ActivityIndicator} from 'react-native';
import KeyboardManager from 'react-native-keyboard-manager'
import {Provider, useDispatch, useSelector} from "react-redux";
import {store, persistor} from './src/redux/store/index'
import {PersistGate} from 'redux-persist/integration/react'
import Loading from "./src/components/Loading";
import {NavigationContainer} from "@react-navigation/native";
import connect from "react-redux/lib/connect/connect";
import {initialAction} from "./src/redux/action/masterAction";


if (Platform.OS === 'ios') {
    KeyboardManager.setEnable(true);
    KeyboardManager.setEnableDebugging(false);
    KeyboardManager.setKeyboardDistanceFromTextField(10);
    KeyboardManager.setEnableAutoToolbar(true);
    KeyboardManager.setToolbarDoneBarButtonItemText("Bitti");
    KeyboardManager.setToolbarPreviousNextButtonEnable(false);
    KeyboardManager.setShouldShowToolbarPlaceholder(false);
    KeyboardManager.setOverrideKeyboardAppearance(false);
    KeyboardManager.setShouldResignOnTouchOutside(true);
    KeyboardManager.resignFirstResponder();
}

const App = () => {


    if (Platform.OS === "android") {
        StatusBar.setBackgroundColor("rgba(0,0,0,0)");
        StatusBar.setTranslucent(true);
        StatusBar.setBarStyle("dark-content")
    }

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <Route>
                    <StatusBar barStyle="dark-content" translucent backgroundColor="transparent"/>
                </Route>
            </PersistGate>
        </Provider>
    );
}

export default App;
